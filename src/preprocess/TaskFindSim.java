package preprocess;

import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import LMForTeKnowbase.LanguageModelEntity;

public class TaskFindSim implements Runnable
{
	double lambda1;
	double lambda2;
	LanguageModelEntity a;
	LanguageModelEntity b;
	LanguageModelEntity c;
	LanguageModelEntity d;
	HashSet<String> words;
	ConcurrentHashMap<String, ConcurrentHashMap<String, Double>> simScore;
	
	public TaskFindSim(LanguageModelEntity a, LanguageModelEntity b, LanguageModelEntity c, LanguageModelEntity d, HashSet<String> words, ConcurrentHashMap<String, ConcurrentHashMap<String, Double>> simScore, double lambda1, double lambda2)
	{
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.words = words;
		this.simScore = simScore;
		this.lambda1=lambda1;
		this.lambda2=lambda2;
	}
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		double mod_a=0.0;
		double mod_b=0.0;
			double score=0.0;
			for(String w:a.getMixture().keySet())
			{
				mod_a = mod_a + a.getMixture().get(w)*a.getMixture().get(w);
				if(b.getMixture().get(w)!=null)
				{
					score = score + a.getMixture().get(w)*b.getMixture().get(w);
				}
			}
			for(String w:b.getMixture().keySet())
			{
				mod_b = mod_b + b.getMixture().get(w)*b.getMixture().get(w);
			}
			score = score/(Math.sqrt(mod_a)*Math.sqrt(mod_b));
			double score1=0.0;
			if(c!=null && d!=null)
			{
				score1 = c.cosineSimilarity(d);
			}
			double tot_score = lambda1*score + lambda2*score1;
			if(tot_score!=0)
			{
				if(simScore.get(a.getName())==null)
				{
					ConcurrentHashMap<String, Double> cc = new ConcurrentHashMap<String, Double>();
					cc.put(b.getName(), tot_score);
					simScore.put(a.getName(), cc);
				}
				else
				{
					ConcurrentHashMap<String, Double> cc = simScore.get(a.getName());
					cc.put(b.getName(), tot_score);
					simScore.put(a.getName(), cc);
				}
			}
				
	}

}

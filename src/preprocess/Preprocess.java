package preprocess;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.w3c.dom.*;

import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;
import com.google.common.graph.*;
import LMForTeKnowbase.LanguageModelEntity;
import LMForTeKnowbase.UtilityFunctions;
import aspect.AdjListCompact;
import aspect.Aspect;
import buildAndProcessTrees.TaskFindRefD;
import javatools.parsers.PlingStemmer;
import main.java.org.ahocorasick.trie.Emit;
import main.java.org.ahocorasick.trie.Trie;
import tomcat.GetPropertyValues;

import preprocess.AdjList;
//import preprocess.Edge;

import javax.xml.parsers.*;
import java.io.*;

public class Preprocess 
{
	public static class Edge
	{
		long document;
		double rel;
		
		public Edge(long d, double rel)
		{
			this.document=d;
			this.rel=rel;
		}
		public long getDoc()
		{
			return this.document;
		}
		public double getRelevance()
		{
			return this.rel;
		}
	}
	
	public static class Edge1 
	{
		float value;
		String name;
		String relName;
		public Edge1(float value, String name)
		{
			this.value = value;
			this.name = name;
		}
		
		public Edge1(float value, String name, String rel)
		{
			this.value = value;
			this.name = name;
			this.relName = rel;
		}
		
		public Edge1()
		{
			//nothing
		}
		public String getName()
		{
			return this.name;
		}
		
		public void setName(String name)
		{
			this.name = name;
		}
		public float getValue()
		{
			return this.value;
		}
		public void setValue(float value)
		{
			this.value = value;
		}
		public String getRelationaName()
		{
			return relName;
		}
		public void setRelationName(String relName)
		{
			this.relName = relName;
		}
		
	}
	
	public static Comparator<Entry<String, Integer>> valueComparator = new Comparator<Entry<String, Integer>>() 
	{
		 public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2)
		 {
			 Integer v1 = e1.getValue();
			 Integer v2 = e2.getValue();
			 return v2.compareTo(v1);
		 }
	};
	
	public static void parse(String taxonomy, String survey) throws Exception
	{
		System.out.println("in parse");
		System.out.println("in parse");
		HashMap<String, String> topic_id_to_name = new HashMap<String, String>();
		BufferedReader br = new BufferedReader(new FileReader("/home/prajna/workspace/TutorialBank/data/taxonomy.csv"));
		String line;
		
		while((line=br.readLine())!=null)
		{
			System.out.println(line);
			StringTokenizer tok = new StringTokenizer(line,",");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			String topic_id = tokens.get(3).replace("\"", "");
			String topic_name = tokens.get(1).replace("\"", "");
			
			topic_id_to_name.put(topic_id, topic_name);
		}
		
		BufferedReader br1 = new BufferedReader(new FileReader("/home/prajna/workspace/TutorialBank/data/survey.csv"));
		HashMap<String, ArrayList<String>> topic_to_survey = new HashMap<String, ArrayList<String>>();
		while((line=br1.readLine())!=null)
		{
			System.out.println(line);
			StringTokenizer tok = new StringTokenizer(line,",");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			String topic_id = tokens.get(3).replace("\"", "");
			String survey_name = tokens.get(0).replace("\"", "");
			if(topic_to_survey.get(topic_id)!=null)
			{
				ArrayList<String> ss = topic_to_survey.get(topic_id);
				ss.add(survey_name);
				topic_to_survey.put(topic_id, ss);
			}
			else
			{
				ArrayList<String> ss = new ArrayList<String>();
				ss.add(survey_name);
				topic_to_survey.put(topic_id, ss);
			}
			
		}
		System.out.println(topic_to_survey.size());
		
		Runtime rt = Runtime.getRuntime();
		BufferedWriter bw = new BufferedWriter(new FileWriter("/home/prajna/workspace/TutorialBank/data/citations_extracted_script.sh"));
		
		
		for(String t:topic_to_survey.keySet())
		{	
			for(String ss:topic_to_survey.get(t))
			{
				System.out.println(t+"\t"+ss);
				String ff ="/home/prajna/workspace/TutorialBank/data/files_text/"+ss+".txt";
				String ff1 = "/home/prajna/workspace/TutorialBank/data/url/"+ss+".txt";
				
				File ff_file = new File(ff);
				File ff1_file = new File(ff1);
				String command="";
				String fff = ss+".xml";
				if(ff_file.exists())
				{
					command = "perl /home/prajna/workspace/ParsCit/bin/citeExtract.pl -m extract_all "+ff+" > /home/prajna/workspace/TutorialBank/data/citations_extracted/"+fff;
				}
				else if(ff1_file.exists())
				{
					System.out.println("url exists");
					command = "perl /home/prajna/workspace/ParsCit/bin/citeExtract.pl -m extract_all "+ff1+" > /home/prajna/workspace/TutorialBank/data/citations_extracted/"+fff;
				}
				
				bw.write(command+"\n");
			}
		}
		bw.close();
	}
	
	public static void parseXMLForAll(String taxonomy, String survey) throws Exception
	{
		System.out.println("in parse");
		
		HashMap<String, String> topic_id_to_name = new HashMap<String, String>();
		BufferedReader br = new BufferedReader(new FileReader("/home/pearl/workspace/TutorialBank/data/taxonomy.csv"));
		String line;
		
		while((line=br.readLine())!=null)
		{
			System.out.println(line);
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			String topic_id = tokens.get(3).replace("\"", "");
			String topic_name = tokens.get(1).replace("\"", "");
			
			topic_id_to_name.put(topic_id, topic_name);
		}
		
		BufferedReader br1 = new BufferedReader(new FileReader("/home/pearl/workspace/TutorialBank/data/survey.csv"));
		HashMap<String, ArrayList<String>> topic_to_survey = new HashMap<String, ArrayList<String>>();
		while((line=br1.readLine())!=null)
		{
			System.out.println(line);
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			String topic_id = tokens.get(3).replace("\"", "");
			System.out.println("topic_id: "+topic_id);
			String survey_name = tokens.get(0).replace("\"", "");
			if(topic_to_survey.get(topic_id)!=null)
			{
				ArrayList<String> ss = topic_to_survey.get(topic_id);
				ss.add(survey_name);
				topic_to_survey.put(topic_id, ss);
			}
			else
			{
				ArrayList<String> ss = new ArrayList<String>();
				ss.add(survey_name);
				topic_to_survey.put(topic_id, ss);
			}
			
		}
		System.out.println(topic_to_survey.size());
		
		String outfile = "/home/pearl/workspace/TutorialBank/data/queries1/all.tsv";
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		for(String t:topic_to_survey.keySet())
		{	
			String topic_name = topic_id_to_name.get(t);
			bw.write(topic_name+"\n\t");
			
			for(String ss:topic_to_survey.get(t))
			{
				System.out.println(topic_name+"\t"+ss);
				String fff = ss+".xml";
				String file = "/home/pearl/workspace/TutorialBank/data/citations_extracted/"+fff;
				 File newFile = new File(file);
				 if(newFile.length() > 0)
				 {
					 parseXML(file,bw);
				 }

			}
		}
		bw.close();
		
	}
	
	
	
	public static void parseXML(String file, BufferedWriter bw) throws Exception
	{
		System.out.println("in parse XML");
		File inputFile = new File(file);
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		try
		{
		org.w3c.dom.Document doc = builder.parse(inputFile);
		
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		
		NodeList nList = doc.getElementsByTagName("variant");
		
		System.out.println("----------------------------");
		for (int i = 0; i < nList.getLength(); i++) 
		{
				NodeList childList = nList.item(i).getChildNodes();
				System.out.println(childList.getLength());
				for (int j = 0; j < childList.getLength(); j++) 
				{
					Node childNode = childList.item(j);
					if(childNode instanceof Element)
					{
						Element childElement = (Element) childNode;
						if(childElement.tagName().contains("section"))
						{
							System.out.println( ((Node) childElement).getTextContent());
							bw.write( ((Node) childElement).getTextContent());
						}
						
					}
		            
		        }
			
		}
		}
		catch(Exception e)
		{
			return;
		}
		//bw.close();
		
	}
	
	public static void init() throws Exception
	{
		System.out.println("working");
		parseXMLForAll("/home/pearl/workspace/TutorialBank/data/taxonomy.csv","/home/pearl/workspace/TutorialBank/data/survey.csv");
		/*
		 * File f = new File(
		 * "sftp://prajna@10.237.26.41/home/prajna/workspace/TutorialBank/data/url/827.txt"
		 * ); if(f.exists()) { System.out.println("exists"); } else {
		 * System.out.println("does not exist"); }
		 */
	}
	
	public static void clean() throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		TagEntities te = new TagEntities();
		  String wikipedia = "/mnt/dell/prajna/neo4j/input/allLinks.tsv";
	    Aspect a = new Aspect();
	    
	    String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	   
  		AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(String entity : a1.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        HashMap<String, String> descriptions_old = new HashMap<String, String>();
        HashMap<String, ArrayList<String>> address = te.getAddress("/home/prajna/workspace/TutorialBank/data/topics_to_resources.csv");
  		HashMap<String, String> descriptions = te.readFile1("/mnt/dell/prajna/allwikipedia/all_text_1_tkb");
  		BufferedReader br1 = new BufferedReader(new FileReader(hm.get("types")));
		String line;
		HashMap<String, ArrayList<String>> supertypes = new HashMap<String, ArrayList<String>>();
		HashMap<String, ArrayList<Edge1>> supertypes_edge = new HashMap<String, ArrayList<Edge1>>();
		while((line=br1.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			String aa = tok.nextToken();
			String bb = tok.nextToken();
			if(aa.length()<=2 || bb.length()<=2) continue;
			if(supertypes.get(aa)==null)
			{
				ArrayList<String> superl = new ArrayList<String>();
				ArrayList<Edge1> superl_edge = new ArrayList<Edge1>();
				superl.add(bb);
				superl_edge.add(new Edge1(0,bb));
				supertypes.put(aa, superl);
				supertypes_edge.put(aa, superl_edge);
			}
			else
			{
				ArrayList<String> superl = supertypes.get(aa);
				ArrayList<Edge1> superl_edge = supertypes_edge.get(aa);
				superl.add(bb);
				superl_edge.add(new Edge1(0,bb));
				supertypes.put(aa, superl);
				supertypes_edge.put(aa, superl_edge);
			}
		}
		
		//AdjList aa = new AdjList(supertypes_edge);
		//HashMap<String, ArrayList<String>> supertypes_inc = aa.getIncidentList();
		 
  		ReadSubgraph rr = new ReadSubgraph();
  		//AdjList wiki = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
  		MutableValueGraph<String, Integer> wiki1 = a.readGraphEfficientWiki(wikipedia);
  		MutableValueGraph<String, Integer> wiki = a.addGraph(wiki1, hm.get("new-graph"));
  		ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki1);
  		
  		//System.out.println("read graph and it worked");
  		LanguageModelEntity lm = new LanguageModelEntity();
  		ArrayList<String> queries = new ArrayList<String>();
  		HashSet<String> facets = new HashSet<String>();
  		
  		File ff1 = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/pairwise_scores");
  		for(File fff:ff1.listFiles())
  		{
  		String query = fff.getName();
  		System.out.println(query);
  		if(address.get(query)!=null)
		{
			String tot_line="";
			for(String ad:address.get(query))
			{
    			BufferedReader br = new BufferedReader(new FileReader(ad));
    			//String line;
    			while((line=br.readLine())!=null)
    			{
    				tot_line = tot_line + " . "+line;
    			}
    			if(descriptions.get(query)!=null)
    			{
    				descriptions.put(query, descriptions.get(query)+" . "+tot_line);
    			}
    			br.close();
			}
		}
		
    HashMap<String, Double> app_hm = new HashMap<String, Double>();
    HashMap<String, Double> alg_hm = new HashMap<String, Double>();
   HashMap<String, Double> imp_hm = new HashMap<String, Double>();
   
   
    if(!(wiki_i.nodes().contains(query))) continue;
   if(wiki_i.successors(query)==null)
   {
	   System.out.println("Query is: "+query+", it is not a valid wikipedia page name. Sorry :(");
        
	   //System.out.println("</body></html>");
   }
   else
   {
	   HashMap<String, Double> scores1 = new HashMap<String, Double>();
	   HashMap<String, HashSet<String>> support_sentences = new HashMap<String, HashSet<String>>();
		if(descriptions.get(query)!=null)
		{
			scores1 = te.computeScore(a1.nodeMap1, descriptions.get(query), query, support_sentences, trie);
		}
		
		File ff = new File("/mnt/delcleanl/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/pairwise_scores/"+query);
		HashMap<String, Double> co_occ = new HashMap<String, Double>(); //stores co-occurence statistics from semantic scholar corpus
		if(ff.exists())
		{
			System.out.println(query+" exists.");
			co_occ = te.computeScore1(ff);
		}
		
		System.out.println("parse_tree: "+ co_occ.get("parse_tree"));
		
	   	ArrayList<String> successors = new ArrayList<String>(wiki_i.successors(query));
		
	   	//successors.addAll(co_occ.keySet());
	   	
	   	HashMap<String, Double> scores = new HashMap<String, Double>();
	   	HashMap<String, ArrayList<String>> subtypes = new HashMap<String, ArrayList<String>>();
		for(String name:successors)
		{
			if(a1.nodeMap1.get(query)!=null && a1.nodeMap1.get(name)!=null)
			{
				//System.out.println(name+" is mapped");
				Optional c1 =  a1.labeledGraph.edgeValue(a1.nodeMap1.get(query), a1.nodeMap1.get(name));
				Optional c11 =  a1.labeledGraph.edgeValue(a1.nodeMap1.get(name), a1.nodeMap1.get(query));
				if(c1.isPresent() || c11.isPresent())
				{
					ArrayList<Integer> c2 = (ArrayList<Integer>) c1.get();
					for(int cc:c2)
					{
						//System.out.println("Relations are: "+a1.relmap1.get(cc));
						if(subtypes.get(a1.relmap1.get(cc))==null)
						{
							ArrayList<String> types = new ArrayList<String>();
							types.add(name);
							subtypes.put(a1.relmap1.get(cc), types);
						}
						else
						{
							ArrayList<String> types = subtypes.get(a1.relmap1.get(cc));
							types.add(name);
							subtypes.put(a1.relmap1.get(cc), types);
						}
					}
				}
				
				if(c11.isPresent())
				{
					//System.out.println("relation exists");
					ArrayList<Integer> c2 = (ArrayList<Integer>) c11.get();
					for(int cc:c2)
					{
						if(subtypes.get(a1.relmap1.get(cc))==null)
						{
							ArrayList<String> types = new ArrayList<String>();
							types.add(name);
							subtypes.put(a1.relmap1.get(cc), types);
						}
						else
						{
							ArrayList<String> types = subtypes.get(a1.relmap1.get(cc));
							types.add(name);
							subtypes.put(a1.relmap1.get(cc), types);
						}
						if(scores.get(a1.relmap1.get(cc))!=null)
        	   			{
        	   				scores.put(a1.relmap1.get(cc), scores.get(a1.relmap1.get(cc))+1);
        	   			}
        	   			else
        	   			{
        	   				scores.put(a1.relmap1.get(cc), 1.0);
        	   			}
					}
				}
			}
			
	   		if(supertypes.get(name)!=null)
	   		{
	   			ArrayList<String> superl = supertypes.get(name);
	   			ArrayList<String> subtypes_names;
	   			for(String ss:superl)
	   			{
    	   			if(subtypes.get(ss)!=null)
    	   			{
    	   				subtypes_names = subtypes.get(ss);
    	   				subtypes_names.add(name);
    	   				subtypes.put(ss,subtypes_names);
    	   			}
    	   			else
    	   			{
    	   				subtypes_names = new ArrayList<String>();
    	   				subtypes_names.add(name);
    	   				subtypes.put(ss,subtypes_names);
    	   			}
    	   			if(scores.get(ss)!=null)
    	   			{
    	   				scores.put(ss, scores.get(ss)+1);
    	   			}
    	   			else
    	   			{
    	   				scores.put(ss, 1.0);
    	   			}
    	   			
	   			}
	   		}
		
		}
		
		ArrayList<Entry<String, Double>> app_ordered = new ArrayList(app_hm.entrySet());
		ArrayList<Entry<String, Double>> alg_ordered = new ArrayList(alg_hm.entrySet());
		ArrayList<Entry<String, Double>> imp_ordered = new ArrayList(imp_hm.entrySet());
		
		ArrayList<Entry<String, Double>> scores_ordered = new ArrayList(scores.entrySet());
		ArrayList<Entry<String, Double>> scores1_ordered = new ArrayList<Entry<String, Double>>();
		HashMap<String, Double> scores2 = new HashMap<String, Double>(); 
		for(Entry<String, Double> e:scores_ordered)
        {
		 	String e1 = e.getKey();
		 	double cooccurence_score=0;
		 	if(scores1.get(e1)!=null)
		 	{
		 		cooccurence_score = cooccurence_score + scores1.get(e1);
		 	}
		 	if(co_occ.get(e1)!=null)
		 	{
		 		cooccurence_score = cooccurence_score + co_occ.get(e1);
		 	}
		 	for(String ss:subtypes.get(e1))
		 	{
		 		if(scores1.get(ss)!=null)
			 	{
			 		//cooccurence_score = cooccurence_score + scores1.get(ss);
			 	}
		 		if(co_occ.get(ss)!=null)
			 	{
			 		//cooccurence_score = cooccurence_score + co_occ.get(ss);
			 	}
		 	}
		 	//if(cooccurence_score!=0)
		 	//{
		 		//cooccurence_score = cooccurence_score + scores.get(e1);
		 	//}
		 	Entry<String, Double> e2 = new AbstractMap.SimpleEntry<String, Double>(e1,cooccurence_score);
		 	scores1_ordered.add(e2);
		 	scores2.put(e1, cooccurence_score);
        }
		 
		
		Collections.sort(app_ordered, LanguageModelEntity.valueComparator);
		Collections.sort(alg_ordered, LanguageModelEntity.valueComparator);
		Collections.sort(imp_ordered, LanguageModelEntity.valueComparator);
		
		Collections.sort(scores1_ordered, LanguageModelEntity.valueComparator);
		
		HashMap<String, Double> word_wise = new HashMap<String, Double>();
		
		
		HashMap<String, HashSet<String>> word_types = new HashMap<String, HashSet<String>>();
		HashMap<String, HashSet<String>> word_types_up = new HashMap<String, HashSet<String>>();
		
		
		ArrayList<Entry<String, Double>> word_wise_ordered = new ArrayList<>(word_wise.entrySet());
		Collections.sort(word_wise_ordered,LanguageModelEntity.valueComparator);
		
		 HashMap<String, String> new_parent = new HashMap<String, String>();
         int cc=0;
         HashSet<String> dont_display = new HashSet<String>();
         for(Entry<String, Double> e:word_wise_ordered)
         {
         	dont_display.add(e.getKey());
         	cc++;
         	System.out.println("<h3>"+e.getKey()+", "+e.getValue()+"</h3>");
         	for(String wt:word_types.get(e.getKey()))
         	{
         		dont_display.add(wt);
         		System.out.println("<p>"+wt+"</p>");
         		
         	}
         	if(cc==5) break;
         }
         
         for(Entry<String, Double> e:scores1_ordered)
         {
         	if(scores1.get(e.getKey())==null && co_occ.get(e.getKey())==null) continue;
         	if(dont_display.contains(e.getKey())) continue;
         	facets.add(e.getKey());
         	for(String ss:subtypes.get(e.getKey()))
          	{
          		facets.add(ss);
          	}
         }
        
   		}
  	}
	
  	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/output/facets"));
	for(String f:facets)
	{
		bw.write(f+"\n");
	}
	bw.close();
		
}
	
	public static void read(String file, String docs, String entities, String prefix) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		HashMap<Long, ArrayList<Integer>> h = new HashMap<Long, ArrayList<Integer>>();
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> aa = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				aa.add(tok.nextToken());
			}
			if(aa.size()!=2) continue;
			if(h.get(Long.parseLong(aa.get(1)))==null)
			{
				ArrayList<Integer> bb = new ArrayList<Integer>();
				bb.add(Integer.parseInt(aa.get(0)));
				h.put(Long.parseLong(aa.get(1)), bb);
			}
			else
			{
				ArrayList<Integer> bb = h.get(Long.parseLong(aa.get(1)));
				bb.add(Integer.parseInt(aa.get(0)));
				h.put(Long.parseLong(aa.get(1)), bb);
			}
		}
		 br = new BufferedReader(new FileReader(entities));
		 HashMap<Integer, String> entitymap = new HashMap<Integer, String>();
		 while((line=br.readLine())!=null)
		 {
			 StringTokenizer tok = new StringTokenizer(line,"\t");
			 ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			if(tokens.size()!=2) continue;
			int a = Integer.parseInt(tokens.get(0));
			String b = tokens.get(1);
			entitymap.put(a, b);
		 }
		 
		 HashMap<String, ArrayList<Edge>> doclist = new HashMap<String, ArrayList<Edge>>();
		 br = new BufferedReader(new FileReader(docs));
		 while((line=br.readLine())!=null)
		 {
				StringTokenizer tok = new StringTokenizer(line,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				if(tokens.size()!=3) continue;
				int a = Integer.parseInt(tokens.get(0));
				String b = tokens.get(1);
				double c = Double.parseDouble(tokens.get(2));
				if(doclist.containsKey(b))
				{
					ArrayList<Edge> bb = doclist.get(b);
					bb.add(new Edge(a,c));
					doclist.put(b, bb);
				}
				else
				{
					ArrayList<Edge> bb = new ArrayList<Edge>();
					bb.add(new Edge(a,c));
					doclist.put(b, bb);
				}
				
		 }
		 
		 for(String a:doclist.keySet())
		 {
			 ArrayList<Edge> list = doclist.get(a);
			 BufferedWriter bw = new BufferedWriter(new FileWriter(prefix+a.replace(" ", "_").replace("/", "")));
			 HashMap<Integer, Double> efreq = new HashMap<Integer, Double>();
			 for(Edge l:list)
			 {
				 System.out.println(l.getDoc());
				 ArrayList<Integer> entity_list = h.get(l.getDoc());
				 System.out.println(entity_list);
				 if(entity_list==null) 
				 {
					 continue;
				 }
				 for(int el:entity_list)
				 {
					 if(efreq.get(el)!=null)
					 {
						 efreq.put(el, efreq.get(el)+1);
					 }
					 else
					 {
						 efreq.put(el, 1.0);
					 }
				 }
			 }
			 
			 for(int el:efreq.keySet())
			 {
				 bw.write(entitymap.get(el)+"\t"+efreq.get(el)+"\n");
			 }
			 
			 bw.close(); 
			 
		 }
	}
	
	public static void generateQueries() throws Exception
	{
		System.out.println("updated working");
    	GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		TagEntities te = new TagEntities();
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    String wikipedia = "/mnt/dell/prajna/neo4j/input/allLinks.tsv";
	    Aspect a = new Aspect();
	    int port = Integer.parseInt(hm.get("port"));
	      
  		AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(String entity : a1.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        HashMap<String, String> descriptions_old = new HashMap<String, String>();
        HashMap<String, ArrayList<String>> address = te.getAddress("/home/prajna/workspace/TutorialBank/data/topics_to_resources.csv");
  		HashMap<String, String> descriptions = te.readFile1("/mnt/dell/prajna/allwikipedia/all_text_1_tkb");
  		
  		String app_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/application.tsv";
  		String alg_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/algorithm.tsv";
  		String imp_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/implementation.tsv";
  		
  		
  		
  		BufferedReader br1 = new BufferedReader(new FileReader(hm.get("types")));
		String line;
		HashMap<String, HashSet<String>> supertypes = new HashMap<String, HashSet<String>>();
		HashMap<String, ArrayList<Edge>> supertypes_edge = new HashMap<String, ArrayList<Edge>>();
		while((line=br1.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			String aa = tok.nextToken();
			String bb = tok.nextToken();
			if(aa.length()<=3 || bb.length()<=3) continue;
			if(supertypes.get(aa)==null)
			{
				HashSet<String> superl = new HashSet<String>();
				///ArrayList<Edge> superl_edge = new ArrayList<Edge>();
				superl.add(bb);
				//superl_edge.add(new Edge(0,bb));
				supertypes.put(aa, superl);
				//supertypes_edge.put(aa, superl_edge);
			}
			else
			{
				HashSet<String> superl = supertypes.get(aa);
				//ArrayList<Edge> superl_edge = supertypes_edge.get(aa);
				superl.add(bb);
				//superl_edge.add(new Edge(0,bb));
				supertypes.put(aa, superl);
				//supertypes_edge.put(aa, superl_edge);
			}
		}
		System.out.println("read types");
		
		//AdjList aa = new AdjList(supertypes_edge);
	//	HashMap<String, ArrayList<String>> supertypes_inc = aa.getIncidentList();
		 
  		ReadSubgraph rr = new ReadSubgraph();
  		//AdjList wiki = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
  		MutableValueGraph<String, Integer> wiki1 = a.readGraphEfficientWiki(wikipedia);
  		MutableValueGraph<String, Integer> wiki = a.addGraph(wiki1, hm.get("new-graph"));
  		ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki1);
  		//System.out.println("read graph and it worked");
  		LanguageModelEntity lm = new LanguageModelEntity();
        
  		System.out.println("read wikipedia and tutorial bank files");
  		
  		File ff1 = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/pairwise_scores");
  		for(File fff:ff1.listFiles())
  		{
  		String query = fff.getName();
  		System.out.println(query);
  		
		if(address.get(query)!=null)
		{
			String tot_line="";
			for(String ad:address.get(query))
			{
    			BufferedReader br = new BufferedReader(new FileReader(ad));
    			//String line;
    			while((line=br.readLine())!=null)
    			{
    				tot_line = tot_line + " . "+line;
    			}
    			if(descriptions.get(query)!=null)
    			{
    				descriptions.put(query, descriptions.get(query)+" . "+tot_line);
    			}
    			br.close();
			}
		}
		
    HashMap<String, Double> app_hm = new HashMap<String, Double>();
    HashMap<String, Double> alg_hm = new HashMap<String, Double>();
   HashMap<String, Double> imp_hm = new HashMap<String, Double>();
   if(!(wiki_i.nodes().contains(query))) continue;
   if(wiki_i.successors(query)==null)
   {
	   System.out.println("query does not exist int the knowledge base");
   }
   else
   {
	   HashMap<String, Double> scores1 = new HashMap<String, Double>();
	   HashMap<String, HashSet<String>> support_sentences = new HashMap<String, HashSet<String>>();
		if(descriptions.get(query)!=null)
		{
			scores1 = te.computeScore(a1.nodeMap1, descriptions.get(query), query, support_sentences, trie);
		}
		
		File ff = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/pairwise_scores/"+query);
		HashMap<String, Double> co_occ = new HashMap<String, Double>(); //stores co-occurence statistics from semantic scholar corpus
		if(ff.exists())
		{
			System.out.println(query+" exists.");
			co_occ = te.computeScore1(ff);
		}
		
		System.out.println("parse_tree: "+ co_occ.get("parse_tree"));
		
	   	ArrayList<String> successors = new ArrayList<String>(wiki_i.successors(query));
	   	HashSet<String> succset = new HashSet<String>(successors);
		
	   	//successors.addAll(co_occ.keySet());
	   	
	   	HashMap<String, Double> scores = new HashMap<String, Double>();
	   	HashMap<String, HashSet<String>> subtypes = new HashMap<String, HashSet<String>>();
		HashMap<String, HashSet<String>> subtypes2 = new HashMap<String, HashSet<String>>();

	   	for(String name:successors)
		{
			if(a1.nodeMap1.get(query)!=null && a1.nodeMap1.get(name)!=null)
			{
				//System.out.println(name+" is mapped");
				Optional c1 =  a1.labeledGraph.edgeValue(a1.nodeMap1.get(query), a1.nodeMap1.get(name));
				Optional c11 =  a1.labeledGraph.edgeValue(a1.nodeMap1.get(name), a1.nodeMap1.get(query));
				if(c1.isPresent() || c11.isPresent())
				{
					ArrayList<Integer> c2 = (ArrayList<Integer>) c1.get();
					for(int cc:c2)
					{
						//System.out.println("Relations are: "+a1.relmap1.get(cc));
						if(subtypes2.get(a1.relmap1.get(cc))==null)
						{
							HashSet<String> types = new HashSet<String>();
							types.add(name);
							subtypes2.put(a1.relmap1.get(cc), types);
						}
						else
						{
							HashSet<String> types = subtypes2.get(a1.relmap1.get(cc));
							types.add(name);
							subtypes2.put(a1.relmap1.get(cc), types);
						}
					}
				}
				
				if(c11.isPresent())
				{
					//System.out.println("relation exists");
					ArrayList<Integer> c2 = (ArrayList<Integer>) c11.get();
					for(int cc:c2)
					{
						if(subtypes2.get(a1.relmap1.get(cc))==null)
						{
							HashSet<String> types = new HashSet<String>();
							types.add(name);
							subtypes2.put(a1.relmap1.get(cc), types);
						}
						else
						{
							HashSet<String> types = subtypes2.get(a1.relmap1.get(cc));
							types.add(name);
							subtypes2.put(a1.relmap1.get(cc), types);
						}
						if(scores.get(a1.relmap1.get(cc))!=null)
        	   			{
        	   				scores.put(a1.relmap1.get(cc), scores.get(a1.relmap1.get(cc))+1);
        	   			}
        	   			else
        	   			{
        	   				scores.put(a1.relmap1.get(cc), 1.0);
        	   			}
					}
				}
			}
			
	   		if(supertypes.get(name)!=null)
	   		{
	   			HashSet<String> superl = supertypes.get(name);
	   			HashSet<String> subtypes_names;
	   			for(String ss:superl)
	   			{
    	   			if(subtypes.get(ss)!=null)
    	   			{
    	   				subtypes_names = subtypes.get(ss);
    	   				subtypes_names.add(name);
    	   				subtypes.put(ss,subtypes_names);
    	   			}
    	   			else
    	   			{
    	   				subtypes_names = new HashSet<String>();
    	   				subtypes_names.add(name);
    	   				subtypes.put(ss,subtypes_names);
    	   			}
    	   			
    	   			
	   			}
	   		}
		
		}
		
		for(String ss:subtypes.keySet())
		{
			boolean flag = false;
			//String ss_stemmed = PlingStemmer.stem(ss);
			for(String ss1:subtypes.keySet())
			{
				if(ss.equals(ss1)) continue;
				//////String ss1_stemmed = PlingStemmer.stem(ss1);
				if(supertypes.get(ss)!=null )
				{
					
					if(supertypes.get(ss).contains(ss1) )
					{
						flag=true;
						HashSet<String> hs = subtypes.get(ss1);
						hs.add(ss);
						hs.addAll(subtypes.get(ss));
						if(subtypes2.get(ss1)==null)
						{
							subtypes2.put(ss1, hs);
						}
						else
						{
							HashSet<String> hs1 = subtypes2.get(ss1);
							hs1.addAll(hs);
							subtypes2.put(ss1, hs1);
						}
						
					}
					
					
				}
				
				
			}
			if(!flag)
			{
				subtypes2.put(ss, subtypes.get(ss));
			}
		}
		for(String ss:subtypes2.keySet())
		{
			if(scores.get(ss)==null)
			{
				scores.put(ss, (double) subtypes2.get(ss).size());
			}
		}
		 
		subtypes = subtypes2;
		
		ArrayList<Entry<String, Double>> app_ordered = new ArrayList(app_hm.entrySet());
		ArrayList<Entry<String, Double>> alg_ordered = new ArrayList(alg_hm.entrySet());
		ArrayList<Entry<String, Double>> imp_ordered = new ArrayList(imp_hm.entrySet());
		
		ArrayList<Entry<String, Double>> scores_ordered = new ArrayList(scores.entrySet());
		ArrayList<Entry<String, Double>> scores1_ordered = new ArrayList<Entry<String, Double>>();
		HashMap<String, Double> scores2 = new HashMap<String, Double>(); 
		
		for(Entry<String, Double> e:scores_ordered)
        {
		 	String e1 = e.getKey();
		 	double cooccurence_score=0;
		 	if(scores1.get(e1)!=null)
		 	{
		 		cooccurence_score = cooccurence_score + scores1.get(e1);
		 	}
		 	if(co_occ.get(e1)!=null)
		 	{
		 		cooccurence_score = cooccurence_score + co_occ.get(e1);
		 	}
		 	for(String ss:subtypes.get(e1))
		 	{
		 		if(scores1.get(ss)!=null)
			 	{
			 		//cooccurence_score = cooccurence_score + scores1.get(ss);
			 	}
		 		if(co_occ.get(ss)!=null)
			 	{
			 		//cooccurence_score = cooccurence_score + co_occ.get(ss);
			 	}
		 	}
		 	//if(cooccurence_score!=0)
		 	//{
		 		//cooccurence_score = cooccurence_score + scores.get(e1);
		 	//}
		 	Entry<String, Double> e2 = new AbstractMap.SimpleEntry<String, Double>(e1,cooccurence_score);
		 	scores1_ordered.add(e2);
		 	scores2.put(e1, cooccurence_score);
        }
		
		
		
		Collections.sort(app_ordered, LanguageModelEntity.valueComparator);
		Collections.sort(alg_ordered, LanguageModelEntity.valueComparator);
		Collections.sort(imp_ordered, LanguageModelEntity.valueComparator);
		
		Collections.sort(scores1_ordered, LanguageModelEntity.valueComparator);
		
		HashMap<String, Double> word_wise = new HashMap<String, Double>();
		
		
		HashMap<String, HashSet<String>> word_types = new HashMap<String, HashSet<String>>();
		HashMap<String, HashSet<String>> word_types_up = new HashMap<String, HashSet<String>>();
		
		
		
		ArrayList<Entry<String, Double>> word_wise_ordered = new ArrayList<>(word_wise.entrySet());
		Collections.sort(word_wise_ordered,LanguageModelEntity.valueComparator);
		
		
       // writer.println("<html><title>Welcome</title><body>");
        //writer.println("<h1>Query is: "+query+"</h1>");
		
		
      
        HashMap<String, String> new_parent = new HashMap<String, String>();
        int cc=0;
        HashSet<String> dont_display = new HashSet<String>();
      
        
        
       HashMap<String, Double> scores1_ordered1 = new HashMap<String, Double>();
       HashMap<String, HashSet<String>> support_sentences1 = new HashMap<String, HashSet<String>>();
       HashMap<String, HashSet<String>> subtypes1 = new HashMap<String, HashSet<String>>();
       HashSet<String> succset1 = new HashSet<String>();
       for(Entry<String, Double> e:scores1_ordered)
        {
        	String stemmed_en = PlingStemmer.stem(e.getKey());
        	if(succset.contains(e.getKey())) succset1.add(stemmed_en);
        	if(scores1_ordered1.get(stemmed_en)==null) scores1_ordered1.put(stemmed_en, e.getValue());
        	else scores1_ordered1.put(stemmed_en, e.getValue() + scores1_ordered1.get(stemmed_en));
        	
        	if(support_sentences1.get(stemmed_en)!=null)
        	{
        		if(support_sentences.get(e.getKey())!=null)
        		{
        			HashSet<String> sent1 = support_sentences.get(e.getKey());
        			sent1.addAll(support_sentences1.get(stemmed_en));
        			support_sentences1.put(stemmed_en, sent1);
        		}
        		
        	}
        	else
    		{
        		if(support_sentences.get(e.getKey())!=null)
        		{
        			HashSet<String> sent1 = support_sentences.get(e.getKey());
        			//sent1.addAll(support_sentences1.get(stemmed_en));
        			support_sentences1.put(stemmed_en, sent1);
        		}
    		}
        	
        	if(subtypes1.get(stemmed_en)!=null)
        	{
        		if(subtypes.get(e.getKey())!=null)
        		{
        			HashSet<String> sent1 = subtypes.get(e.getKey());
        			sent1.addAll(subtypes1.get(stemmed_en));
        			subtypes1.put(stemmed_en, sent1);
        		}
        		
        	}
        	else
    		{
        		if(subtypes.get(e.getKey())!=null)
        		{
        			HashSet<String> sent1 = subtypes.get(e.getKey());
        			//sent1.addAll(support_sentences1.get(stemmed_en));
        			subtypes1.put(stemmed_en, sent1);
        		}
    		}
        	
        }
        
        scores1_ordered = new ArrayList<>(scores1_ordered1.entrySet());
        Collections.sort(scores1_ordered, LanguageModelEntity.valueComparator);
        subtypes = subtypes1;
        support_sentences = support_sentences1;
        
        BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/aspect_queries/"+query));
        int count=0;
        
        for(Entry<String, Double> e:scores1_ordered)
        {
        	if(scores1.get(e.getKey())==null && co_occ.get(e.getKey())==null && !succset1.contains(e.getKey())) continue;
        	if(dont_display.contains(e.getKey())) continue;
        	count++;
        	int c=1;
        	
        	bw.write(query.replace("_", " ")+"\t"+e.getKey().replace("_", " "));
        	
        	//writer.println("<h3>"+e.getKey()+", "+e.getValue()+"</h3>");
        	for(String ss:subtypes.get(e.getKey()))
        	{
        		int c1=1;
        		bw.write("\t"+ss);
        		
        	}
        	bw.write("\n");
        }
        bw.close();
        
	}
  		}
	}
	
	public static void generateScript(String kb, String queries, String aspect) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(queries));
		String line;
		ArrayList<String> querieslist = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			querieslist.add(line);
		}
		
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    
	    Aspect a = new Aspect();
  		AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);

		
	}
	
	public static double computeEdgeScore(LanguageModelEntity a, LanguageModelEntity b, HashSet<String> words)
	{
		for(String w1:a.getMixture().keySet())
		{
			if(b.getMixture().get(w1)!=null)
			{
				for(String w:words)
				{
					if(a.getMixture().get(w)==null)
					{
						a.getMixture().put(w, 0.0);
					}
					if(b.getMixture().get(w)==null)
					{
						b.getMixture().put(w, 0.0);
					}
				}
				return a.cosineSimilarity(b);
			}
		}
		
		return 0.0;
	}
	
	public static void computeAllPairShortestPaths(String entities) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(entities));
		String line;
		
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    
	    HashSet<Integer> entities1 = new HashSet<Integer>();
		while((line=br.readLine())!=null)
		{
			if(ac.nodeMap1.get(line.replace(" ", "_"))!=null)
				entities1.add(ac.nodeMap1.get(line.replace(" ", "_")));
		}	
			
			int num_threads = (Runtime.getRuntime().availableProcessors()+1)*7/8; 
			try
			{
				int nn=entities1.size();
				if(nn<num_threads) nn=num_threads+1;
					
				ThreadPoolExecutor executor = new ThreadPoolExecutor(num_threads,
						nn, Long.MAX_VALUE, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(nn));
				
				for(int node1:entities1)
				{
					
					//TaskFindRefDModified t1 = new TaskFindRefDModified(node1, refdScore, a, nodesAtTWoHops, adjList);
					TaskShortestPath t1 = new TaskShortestPath(node1, ac.undirectedGraph);
					executor.execute(t1);
					
				}
				executor.shutdown();
				executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
			}
			catch(IllegalArgumentException e)
			{
				System.out.println("Oops, but lets continue");
				
			}
	
	}
	
	public static void computePhraseScore(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	    for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();	
	    
		BufferedReader br1 = new BufferedReader(new FileReader(queries));
		String line1;
		while((line1=br1.readLine())!=null)
	    {
			System.out.println(line1);
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
			BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/phraseScores/"+line1));
	    	BufferedReader br = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/"+line1));
	    	String line;
	    	HashMap<String, Double> scores = new HashMap<String, Double>();
	    	while((line=br.readLine())!=null)
	    	{
	    		String[] arr = line.split("\t");
	    		scores.put(arr[0], Double.parseDouble(arr[1]));
	    	}
	    	br.close();
	    	br = new BufferedReader(new FileReader(phrasefile));
	   
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			String phrase = arr[0].substring(3);
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			double phraseScore=0.0;
	 			for(Emit e:named_entity_occurences)
	 			{
	 				String en1 = e.getKeyword().replace(" ", "_");
	 				if(scores.get(en1)!=null)
	 				{
	 					phraseScore = phraseScore + scores.get(en1);
	 				}
	 			}
	 			bw.write(phrase+"\t"+phraseScore+"\n");;
	 		}
	 	    br.close();
	 	    bw.close();
	    }
		br1.close();
	}
	
	public static void computeEntityScore(String queries, String wikipedia, String new_graph, String entities) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);

	    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		
		date = new Date();
		System.out.println(dateFormat.format(date)); 
		System.out.println("read teknowbase");
		
        AdjList adj = new AdjList();
        
        
        MutableValueGraph<String, Integer> wiki1 = a11.readGraphEfficientWiki(wikipedia);
  		//MutableValueGraph<String, Integer> wiki = a11.addGraph(wiki1, new_graph);
  		ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki1);
  		
		date = new Date();
		System.out.println(dateFormat.format(date)); 
		System.out.println("read wikipedia");
		HashMap<String, HashMap<String, Double>> queryToEntity = new HashMap<String, HashMap<String, Double>>();
		
		BufferedReader br = new BufferedReader(new FileReader(entities));
		String line;
		int count=0;
		while((line=br.readLine())!=null)
		{
			count=count+1;
			date = new Date();
    		if(count%100==0)
    				{
    			System.out.println(count); 
    		}
			String entity=line.replace(" ", "_");
			int e_code = ac.nodeMap1.get(entity);
			File ff = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/shortest_paths/"+e_code);
			if(!ff.exists()) continue;
			BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/shortest_paths/"+e_code));
			String line1;
			HashMap<Integer, Integer> sp = new HashMap<Integer, Integer>();
			while((line1=br1.readLine())!=null)
			{
				String[] arr = line1.split("\t");
				sp.put(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
			}
			br1 = new BufferedReader(new FileReader(queries));
		    while((line1=br1.readLine())!=null)
		    {
		    	
		    	double score=0.0;
		    	if(!wiki_i.nodes().contains(line1)) continue;
	        	for(String n:wiki_i.adjacentNodes(line1))
 				{
 					if(ac.nodeMap1.get(n)!=null)
 					{
 						int n_code= ac.nodeMap1.get(n);
 						if(sp.get(n_code)!=null)
 						{
 							score = score + (1.0/(double)sp.get(n_code));
 						}
 						else
 						{
 							if(e_code==n_code)
 							{
 								score=1.0;
 							}
 						}
 					}
 				}
	        	if(queryToEntity.get(line1)==null)
	        	{
	        		HashMap<String, Double> ent_score = new HashMap<String, Double>();
	        		ent_score.put(entity, score);
	        		queryToEntity.put(line1, ent_score);
	        	}
	        	else
	        	{
	        		HashMap<String, Double> ent_score = queryToEntity.get(line1);
	        		ent_score.put(entity, score);
	        		queryToEntity.put(line1, ent_score);
	        	}
    		
	 		}
	 			
		    //System.out.println("done for query "+entity);
	 			
	 		}
		for(String q:queryToEntity.keySet())
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/"+q));
			for(String ent:queryToEntity.get(q).keySet())
			{
				bw.write(ent+"\t"+queryToEntity.get(q).get(ent)+"\n");
			}
			bw.close();
		}
	 	   
	}
	
	public static void tagEntities(String outputfile, String tagged_entities) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    String nodemap = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    
	    BufferedWriter bw = new BufferedWriter(new FileWriter(outputfile));
	    BufferedReader br = new BufferedReader(new FileReader(tagged_entities));
	    String line;
	    while((line=br.readLine())!=null)
 		{
	    	//line = line.replace(" ", "_");
	    	bw.write(line.replace(" ", "_")+"\t");
	    	System.out.println(line.replace(" ", "_"));
	    	if(ac.nodeMap1.get(line.replace(" ", "_"))==null) continue;
	    	int node = ac.nodeMap1.get(line.replace(" ", "_"));
	    	Set<Integer> ss = ac.undirectedGraph.adjacentNodes(node);
			
			HashSet<Integer> ss1 = new HashSet<Integer>(ss);
			ss1.add(node);
			for(int s1:ss)
			{
				if(s1==node) continue;
				//ss1.addAll(ac.undirectedGraph.adjacentNodes(s1));
			}
			for(int s1:ss1)
			{
				if(ac.nodeMap.get(s1).startsWith("<http://")) continue;
				bw.write(ac.nodeMap.get(s1)+" ");
				//words.add(ac.nodeMap.get(s1));
			}
			bw.write("\n");
				
 		}
	    bw.close();
	    br.close();
	}
	
	public static void computePhraseBOW(String queries, String entities_1_hop) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    String nodemap = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    
	    BufferedReader br = new BufferedReader(new FileReader(entities_1_hop));
	    HashMap<String, ArrayList<String>> hh = new HashMap<String, ArrayList<String>>();
	    String line2;
	    
	    while((line2=br.readLine())!=null)
	    {
	    	String[] arr = line2.split("\t");
	    	String ents[] = arr[1].split(" ");
	    	ArrayList<String> ents_list = new ArrayList<String>();
	    	for(String ee:ents)
	    	{
	    		ents_list.add(ee);
	    	}
	    	hh.put(arr[0], ents_list);
	    }
	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    HashSet<String> entities = new HashSet<String>();
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	    for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        
        AdjList adj = new AdjList();
        HashMap<String, HashMap<String, HashMap<String, Integer>>> qToPhrase = new HashMap<String, HashMap<String, HashMap<String, Integer>>>();
        
        while((line1=br1.readLine())!=null)
	    {
        	System.out.println(line1);
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	//String phraseToEnt = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/phraseToEnt/"+line1;
	    	//BufferedWriter bww = new BufferedWriter(new FileWriter(phraseToEnt));
	    	br = new BufferedReader(new FileReader(phrasefile));
	 	    String line;
	 	    HashSet<String> words = new HashSet<String>();
	 	    HashMap<String, HashMap<String, Integer>> phraseToWords = new HashMap<String, HashMap<String, Integer>>();
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			String phrase = arr[0].substring(3).toLowerCase();
	 			String[] words1 = phrase.split(" ");
	 			HashMap<String, Integer> phraseWordList = new HashMap<String, Integer>();
				for(String ss:words1)
				{
					//System.out.println(ss);
					words.add(ss);
					if(phraseWordList.get(ss)==null) phraseWordList.put(ss, 1);
					else phraseWordList.put(ss, phraseWordList.get(ss)+1);
					//phraseWordList.put(ss);
				}
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase);
	 			if(named_entity_occurences.size()==0) continue;
	 			int num=0;
	 			for(Emit e:named_entity_occurences)
	 			{
	 				String ent = e.getKeyword().replace(" ", "_");
	 				if(hh.get(ent)==null) continue;
	 				ArrayList<String> n_ents = hh.get(ent);
	 				for(String nn:n_ents)
	 				{
	 					String[] arr_ent = nn.split(" ");
	 					for(String ae:arr_ent)
	 					{
	 						words.add(ae);
	 						if(phraseWordList.get(ae)==null) phraseWordList.put(ae, 1);
	 						else phraseWordList.put(ae, phraseWordList.get(ae)+1);
	 					}
	 				}
	 			}
	 			phraseToWords.put(phrase, phraseWordList);
	 		}
	 	    
	 	    ArrayList<String> words_list = new ArrayList<String>(words);
	        System.out.println(words_list.size());
	        HashMap<String, Integer> words_to_index = new HashMap<String, Integer>();
	        for(int i=0;i<words_list.size();i++)
	        {
	        	words_to_index.put(words_list.get(i), i);
	        }
	        ArrayList<Integer> myList= new ArrayList<>(Arrays.asList(new Integer[words_list.size()]));
	        Collections.fill(myList, 0);//fills all 40 entries with 0"
	        
	        BufferedWriter bww = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/bow/"+line1));
        	bww.write(phraseToWords.keySet().size()+"\t"+myList.size()+"\n");
        	for(String p:phraseToWords.keySet())
        	{
        		bww.write(p.replace(" ", "_"));
        		ArrayList<Integer> dd = new ArrayList<Integer>(myList);
        		HashMap<String, Integer> p1 = phraseToWords.get(p);
        		for(String pp:p1.keySet())
        		{
        			dd.set(words_to_index.get(pp), p1.get(pp));
        		}
        		
        		for(double d:dd)
        		{
        			bww.write(" "+d);
        		}
        		bww.write("\n");
        	}
        	bww.close();
	    }
        System.out.println("done reading all words");
        
	}
	
	public static void computePhraseEmbedding(String outputfile, String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    String tkb_embeddings = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/tkb.emd";
	    String nodemap = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    
	    BufferedReader br = new BufferedReader(new FileReader(nodemap));
	    HashMap<String, String> hh = new HashMap<String, String>();
	    String line2;
	    
	    while((line2=br.readLine())!=null)
	    {
	    	String[] arr = line2.split("\t");
	    	hh.put(arr[1], arr[0]);
	    }
	    
	    HashMap<String, LanguageModelEntity> lm_tkb = LanguageModelEntity.readEmbeddingsFromFile(tkb_embeddings, hh);
	    
	    HashMap<String, LanguageModelEntity> phraseEmbeddings = new HashMap<String, LanguageModelEntity>();
	   
	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    HashSet<String> entities = new HashSet<String>();
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	    for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        AdjList adj = new AdjList();
        
        while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	String phraseToEnt = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/phraseToEnt/"+line1;
	    	BufferedWriter bww = new BufferedWriter(new FileWriter(phraseToEnt));
	    	br = new BufferedReader(new FileReader(phrasefile));
	 	    String line;
	 	    
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			if(phraseEmbeddings.get(phrase)!=null) continue;
	 			
	 			bww.write(phrase.toLowerCase());
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			LanguageModelEntity lm1 = new LanguageModelEntity("",adj,new HashMap<String, Double>(),new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
	 			
	 			if(named_entity_occurences.size()==0) continue;
	 			int num=0;
	 			for(Emit e:named_entity_occurences)
	 			{
	 				if(lm_tkb.get(e.getKeyword().replace(" ", "_"))!=null)
	 				{
	 					num++;
	 					bww.write("\t"+num+") "+e.getKeyword());
	 					LanguageModelEntity lm2 = lm1.add(lm_tkb.get(e.getKeyword().replace(" ", "_")));
	 					lm1 = lm2;
	 				}
	 				else
	 				{
	 					continue;
	 				}
	 				//entities.add(e.getKeyword());
	 			}
	 			bww.write("\n");
	 			if(lm1.getMixture().size()>0) 
	 			{
	 				LanguageModelEntity lm2 = lm1.scale(num);
	 				phraseEmbeddings.put(phrase, lm2);
	 			}
	 			
	 		}
	 	    bww.close();
	 	    
	    }
	    
	    BufferedWriter bw = new BufferedWriter(new FileWriter(outputfile));
	    bw.write(phraseEmbeddings.keySet().size()+"\t128");
	    for(String en:phraseEmbeddings.keySet())
	    {
	    	bw.write("\n"+en.replace(" ", "_"));
			for(String s:phraseEmbeddings.get(en).getMixture().keySet())
			{
				LanguageModelEntity lm = phraseEmbeddings.get(en);
				if(!s.equals("e127"))
					bw.write(" "+lm.getMixture().get(s));
				else
					bw.write(" "+lm.getMixture().get(s));
			}
	    }
	    bw.close();
	}
	
	public static void computePhraseBOE(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
 	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);

	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
	 	   for(String entity : ac.nodeMap1.keySet())
	        {
	            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
	        }
	        Trie trie = trie_builder.build();
	    
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/uniquePhrases/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	   String line;
	 	   System.out.println(line1);
	 	   
	        HashSet<String> allEntities = new HashSet<String>();
	        HashMap<String, HashSet<String>> hh = new HashMap<String, HashSet<String>>();
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			HashSet<String> entities = new HashSet<String>();
	 			Set<String> e1 = new HashSet<String>();
	 			
	 			for(Emit e:named_entity_occurences)
	 			{
	 				if(!ac.undirectedGraph.nodes().contains(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")))) continue;

	 				entities.add(e.getKeyword().replace(" ", "_"));
	 				allEntities.add(e.getKeyword().replace(" ", "_"));
	 				
	 				Set<Integer> neigh = ac.undirectedGraph.adjacentNodes(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")));
	 				for(int n:neigh)
	 				{
	 					entities.add(ac.nodeMap.get(n));
	 					allEntities.add(ac.nodeMap.get(n));
	 				}
	 			}
	 			if(entities.size()==0) continue;
	 			hh.put(phrase, entities);
	 		}
	 	    ArrayList<String> allEntityList = new ArrayList<String>(allEntities);
	 	    System.out.println(allEntityList.size());
	 	    
	 	    ArrayList<Integer> vector = new ArrayList<Integer>();
	 	    HashMap<String, Integer> indexedEntities = new HashMap<String, Integer>();
	 	    for(int i=0;i<allEntityList.size();i++)
	 	    {
	 	    	indexedEntities.put(allEntityList.get(i), i);
	 	    	vector.add(0);
	 	    }
	 	    System.out.println(vector.size());

	 	    
	 	    BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/boe/"+line1));
	 	    bw.write(hh.size()+"\t"+allEntityList.size()+"\n");
	 	    for(String e:hh.keySet())
	 	    {
	 	    	ArrayList<Integer> vector1 = new ArrayList<Integer>(vector);
	 	    	System.out.println(vector1.size());
	 	    	for(String en:hh.get(e))
	 	    	{
	 	    		int index = indexedEntities.get(en);
	 	    		vector1.set(index, 1);
	 	    	}
	 	    	bw.write(e.replace(" ", "_"));
	 	    	for(int i:vector1)
	 	    	{
	 	    		bw.write(" "+i);
	 	    	}
	 	    	bw.write("\n");
	 	    }
	 	   bw.close();
	 	   
	    }
	    
	}
	
	public static void generateClustersNearestNeighbor(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
 	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);

	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	   	for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
	    
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/uniquePhrases/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	   String line;
	 	   System.out.println(line1);
	 	   
	        HashSet<String> allEntities = new HashSet<String>();
	        HashMap<String, HashSet<String>> hh = new HashMap<String, HashSet<String>>();
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			HashSet<String> entities = new HashSet<String>();
	 			Set<String> e1 = new HashSet<String>();
	 			
	 			for(Emit e:named_entity_occurences)
	 			{
	 				if(!ac.undirectedGraph.nodes().contains(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")))) continue;

	 				entities.add(e.getKeyword().replace(" ", "_"));
	 				allEntities.add(e.getKeyword().replace(" ", "_"));
	 				
	 				Set<Integer> neigh = ac.undirectedGraph.adjacentNodes(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")));
	 				for(int n:neigh)
	 				{
	 					entities.add(ac.nodeMap.get(n));
	 					allEntities.add(ac.nodeMap.get(n));
	 				}
	 			}
	 			if(entities.size()==0) continue;
	 			hh.put(phrase, entities);
	 		}
	 	    ArrayList<String> allEntityList = new ArrayList<String>(allEntities);
	 	    System.out.println(allEntityList.size());
	 	    
	 	    ArrayList<Integer> vector = new ArrayList<Integer>();
	 	    HashMap<String, Integer> indexedEntities = new HashMap<String, Integer>();
	 	    for(int i=0;i<allEntityList.size();i++)
	 	    {
	 	    	indexedEntities.put(allEntityList.get(i), i);
	 	    	vector.add(0);
	 	    }
	 	    System.out.println(vector.size());

	 	    
	 	   
	 	   
	    }
	}
	
	public static void computePhraseBOETypes(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
 	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);

	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
	 	   for(String entity : ac.nodeMap1.keySet())
	        {
	            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
	        }
	        Trie trie = trie_builder.build();
	    
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	   String line;
	 	   System.out.println(line1);
	 	   
	        HashSet<String> allEntities = new HashSet<String>();
	        HashMap<String, HashSet<String>> hh = new HashMap<String, HashSet<String>>();
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			HashSet<String> entities = new HashSet<String>();
	 			Set<String> e1 = new HashSet<String>();
	 			
	 			for(Emit e:named_entity_occurences)
	 			{
	 				if(!ac.undirectedGraph.nodes().contains(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")))) continue;

	 				entities.add(e.getKeyword().replace(" ", "_"));
	 				allEntities.add(e.getKeyword().replace(" ", "_"));
	 				
	 				Set<Integer> neigh = ac.undirectedGraph.adjacentNodes(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")));
	 				for(int n:neigh)
	 				{
	 					entities.add(ac.nodeMap.get(n));
	 					allEntities.add(ac.nodeMap.get(n));
	 				}
	 			}
	 			if(entities.size()==0) continue;
	 			hh.put(phrase, entities);
	 		}
	 	    ArrayList<String> allEntityList = new ArrayList<String>(allEntities);
	 	    System.out.println(allEntityList.size());
	 	    
	 	    ArrayList<Integer> vector = new ArrayList<Integer>();
	 	    HashMap<String, Integer> indexedEntities = new HashMap<String, Integer>();
	 	    for(int i=0;i<allEntityList.size();i++)
	 	    {
	 	    	indexedEntities.put(allEntityList.get(i), i);
	 	    	vector.add(0);
	 	    }
	 	    System.out.println(vector.size());

	 	    
	 	    BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/boe/"+line1));
	 	    bw.write(hh.size()+"\t"+allEntityList.size()+"\n");
	 	    for(String e:hh.keySet())
	 	    {
	 	    	ArrayList<Integer> vector1 = new ArrayList<Integer>(vector);
	 	    	System.out.println(vector1.size());
	 	    	for(String en:hh.get(e))
	 	    	{
	 	    		int index = indexedEntities.get(en);
	 	    		vector1.set(index, 1);
	 	    	}
	 	    	bw.write(e.replace(" ", "_"));
	 	    	for(int i:vector1)
	 	    	{
	 	    		bw.write(" "+i);
	 	    	}
	 	    	bw.write("\n");
	 	    }
	 	   bw.close();
	 	   
	    }
	    
	}
	

	
	public static void tagEntities(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
 	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);

	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
	 	   for(String entity : ac.nodeMap1.keySet())
	        {
	            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
	        }
	        Trie trie = trie_builder.build();
	 	   
	   
	    while((line1=br1.readLine())!=null)
	    {
	    	System.out.println(line1);
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	   String line;
	 	   HashSet<String> entities = new HashSet<String>();
	 	    
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			
	 			for(Emit e:named_entity_occurences)
	 			{
	 				entities.add(e.getKeyword().replace(" ", "_"));
	 			}
	 		}
	 	    BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_entities/"+line1));
	 	    for(String e:entities)
	 	    {
	 	    	bw.write(e+"\n");
	 	    }
	 	    bw.close();
	    }
	    
	}
	
	public static void computeSPEntities(String entities) throws Exception
	{
	    BufferedReader br2 = new BufferedReader(new FileReader(entities));
	    String line;
	    HashSet<String> tagged_entities = new HashSet<String>();
	    while((line=br2.readLine())!=null)
	    {
	    	tagged_entities.add(line.replace(" ", "_"));
	    }
	    ArrayList<String> te = new ArrayList<String>(tagged_entities);
	    Collections.sort(te);
	    
	    String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);

	    BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/pairwise_entity_scores/scores"));
	    for(String s:te)
	    {
	    	System.out.println(s);
	    	HashMap<String, Double> sp_scores = new HashMap<String, Double>();
	    	if(ac.nodeMap1.get(s)==null) continue;
	    		int s_node = ac.nodeMap1.get(s);
	    		File f = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/shortest_paths/"+s_node);
	    		if(f.exists()) 
	    		{
	    			//continue;
	    			BufferedReader br1 = new BufferedReader(new FileReader(f.getAbsolutePath()));
	    			String line1;
	    			while((line1=br1.readLine())!=null)
	    			{
	    				String arr[] = line1.split("\t");
	    				int node = Integer.parseInt(arr[0]);
	    				String node_s = ac.nodeMap.get(node);
	    				double sp = Double.parseDouble(arr[1]);
	    				if(sp_scores.get(node_s)==null)
	    				{
	    					sp_scores.put(node_s, (double)1.0/sp);
	    				}
	    				else
	    				{
	    					sp_scores.put(node_s, sp_scores.get(node_s)+ (double)1.0/sp);
	    				}
	    			}
	    			
	    		}
	    	for(String s1: te)
	    	{
	    		
	    			
	    			if(sp_scores.get(s1)!=null)
	    			{
	    				bw.write(s+"\t"+s1+"\t"+sp_scores.get(s1)+"\n");
	    			}
	    		
	    	}
	    }
	    bw.close();
	}

	public static void starCluster(String file, double thresh, String output, int count, String wikipedia, String new_graph, String entities, String phraseEmbeddings, double lambda1, double lambda2) throws Exception
	{
		String query=file.replace("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/", "");
		//BufferedReader br =  new BufferedReader(new FileReader(file));
		String line;
		MutableValueGraph<String, Double> doc_graph = ValueGraphBuilder.undirected().build();
		HashSet<String> h = new HashSet<String>();
		HashSet<String> words = new HashSet<String>();
		int c1=0;
		
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    
	    String tkb_embeddings = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_embeddings.txt";
	    String tkb_mappings = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mappings_org.txt";
	    
	    BufferedReader br = new BufferedReader(new FileReader(tkb_mappings));
	    HashMap<String, String> hh = new HashMap<String, String>();
	    while((line=br.readLine())!=null)
	    {
	    	String[] arr = line.split("\t");
	    	hh.put(arr[0], arr[1]);
	    }
	    
	    HashMap<String, LanguageModelEntity> lm_tkb = LanguageModelEntity.readEmbeddingsFromFile(tkb_embeddings, hh);
	   
	    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43

	    System.out.println("read tkb embeddings");
	    
	    BufferedReader br2 = new BufferedReader(new FileReader(entities));
	    HashSet<String> tagged_entities = new HashSet<String>();
	    while((line=br2.readLine())!=null)
	    {
	    	tagged_entities.add(line.replace(" ", "_"));
	    }
	    
	    Aspect a11 = new Aspect();
	    
	    MutableValueGraph<String, Integer> wiki1 = a11.readGraphEfficientWiki(wikipedia);
  		MutableValueGraph<String, Integer> wiki = a11.addGraph(wiki1, new_graph);
  		ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki1);
	    br2 = new BufferedReader(new FileReader(wikipedia));
	    /*
	    ArrayList<Edge> wiki_nei = new ArrayList<Edge>();
	    int ccc=0;
	    while((line=br2.readLine())!=null)
	    {
	    	StringTokenizer tok = new StringTokenizer(line,"\t");
	    	while(tok.hasMoreTokens())
	    	{
	    		
	    	}
	    }*/
	    
  		date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
  		
  		System.out.println("read wikipedia");

	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
  		
	    date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
	    
	    System.out.println("read teknowbase");

	    HashMap<String, Double> frame_score = new HashMap<String, Double>();
	    
	    for(String en:tagged_entities)
	    {
	    	double tot_cosine = 0.0;
	    	if(lm_tkb.get(en.replace(" ", "_"))!=null)
	    	{
		    	for(String neigh:wiki_i.adjacentNodes(query))
		    	{
		    		if(lm_tkb.get(neigh)!=null)
		    		{
		    			double cosine_sim = lm_tkb.get(en.replace(" ", "_")).cosineSimilarity(lm_tkb.get(neigh));
		    			tot_cosine = tot_cosine + cosine_sim;
		    		}
		    	}
	    	}
	    	frame_score.put(en, tot_cosine);
	    }
	    
	    date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
	    
	    System.out.println("computed frame score for entities");

	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
  		
		for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        
        HashMap<String, Double> phraseScoreMap = new HashMap<String, Double>();
        AdjList a = new AdjList();
        br =  new BufferedReader(new FileReader(file));

	    while((line=br.readLine())!=null)
		{
			String[] arr = line.split("', ");
			
			String phrase = arr[0].substring(3);
			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
			
			double phraseScore=0.0;
			for(Emit em:named_entity_occurences)
			{
				phraseScore = phraseScore + frame_score.get(em.getKeyword().replace(" ", "_"));
			}
			
			phraseScoreMap.put(phrase , phraseScore);
			String[] words1 = phrase.split(" ");
			for(String ss:words1)
			{
				//System.out.println(ss);
				words.add(ss);
			}
			//System.out.println(phrase);
			h.add(phrase);
			
		}
	    
	    ArrayList<Entry<String, Double>> phraseScoreList = new ArrayList<Entry<String, Double>>(phraseScoreMap.entrySet());
	    Collections.sort(phraseScoreList, LanguageModelEntity.valueComparator);
	    
	    date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
	    
	    System.out.println("sorted phrases based on scores");
	     
	    System.out.println("words size: "+words.size());
		
		HashMap<String, LanguageModelEntity> lm = new HashMap<String, LanguageModelEntity>();
		for(Entry<String, Double> e:phraseScoreList)
		{
			if(c1>=count) break;
			String ss = e.getKey();
			StringTokenizer tok = new StringTokenizer(ss);
			HashMap<String, Double> mixture = new HashMap<String, Double>();
			HashSet<String> tokhash = new HashSet<String>();
			while(tok.hasMoreTokens())
			{
				String token = tok.nextToken();
				tokhash.add(token);
				if(mixture.get(token)==null) mixture.put(token, 1.0);
				else
					mixture.put(token, mixture.get(token)+1);
			}
			
			LanguageModelEntity lm1 = new LanguageModelEntity(ss,a,new HashMap<String, Double>(),new HashMap<String, HashMap<String, Double>>(), mixture);
			lm.put(ss, lm1);
			c1++;
		}
		date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
		
		System.out.println("generated word vectors for phrases");
		
		HashMap<String, LanguageModelEntity> ph_emb = LanguageModelEntity.readEmbeddingsFromFileAlternate(phraseEmbeddings);
		
		date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
		
		System.out.println("read phrase vector for phrases");
		
		//HashMap<String, HashMap<String, Double>> graph = new HashMap<String, HashMap<String, Double>>();
		
		ConcurrentHashMap<String, ConcurrentHashMap<String, Double>> simScore = new ConcurrentHashMap<String, ConcurrentHashMap<String, Double>>();
		for(String h1:lm.keySet())
		{
			//System.out.println(h1);
			/*
			 * for(String w:words) { if(lm.get(h1).getMixture().get(w)==null) {
			 * lm.get(h1).getMixture().put(w, 0.0); } }
			 */
			
			int num_threads = (Runtime.getRuntime().availableProcessors()+1)*7/8; 
			try
			{
				int nn=lm.keySet().size();
				if(nn<num_threads) nn=num_threads+1;
					
				ThreadPoolExecutor executor = new ThreadPoolExecutor(num_threads,
						nn, Long.MAX_VALUE, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(nn));
				
				for(String node1:lm.keySet())
				{
					if(h1.equals(node1)) continue;
					//TaskFindRefDModified t1 = new TaskFindRefDModified(node1, refdScore, a, nodesAtTWoHops, adjList);
					TaskFindSim t1 = new TaskFindSim(lm.get(h1), lm.get(node1), ph_emb.get(h1.replace(" ", "_")), ph_emb.get(node1.replace(" ", "_")), words, simScore, lambda1, lambda2);
					executor.execute(t1);
					
				}
				executor.shutdown();
				executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
			}
			catch(IllegalArgumentException e)
			{
				System.out.println("Oops, but lets continue");
				continue;
			}
			
		}
		
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(output+"_graph_thresh_"+thresh));
		
		for(String s:simScore.keySet())
		{
			for(String s1:simScore.get(s).keySet())
			{
				if(simScore.get(s).get(s1)>=thresh)
				{
					bw1.write(s+"\t"+s1+"\t"+thresh+"\n");
					Optional cc = doc_graph.edgeValue(s, s1);
					if (cc.isPresent()) {
						continue;
					} else {
						
						doc_graph.putEdgeValue(s, s1, simScore.get(s).get(s1));
					}
				}
			}
		}
		bw1.close();
		HashMap<String, Integer> degrees = new HashMap<String, Integer>();
		
		for(String n:doc_graph.nodes())
		{
			degrees.put(n, doc_graph.degree(n));
		}
		
		ArrayList<Entry<String, Integer>> degree_list = new ArrayList<Entry<String, Integer>>(degrees.entrySet());
		Collections.sort(degree_list, valueComparator);
		//System.out.println(degree_list);
		ArrayList<ArrayList<String>> clusters = new ArrayList<ArrayList<String>>();
		HashSet<String> marked = new HashSet<String>();
		for(Entry<String, Integer> e:degree_list)
		{
			String a1 = e.getKey();
			if(marked.contains(a1)) continue;
			marked.add(a1);
			ArrayList<String> list1 = new ArrayList<String>();
			list1.add(a1);
			for(String adj:doc_graph.adjacentNodes(a1))
			{
				if(marked.contains(adj)) continue;
				list1.add(adj);
				marked.add(adj);
			}
			clusters.add(list1);
		}
		
		date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
		
		System.out.println("generated clusters");
		BufferedWriter bw = new BufferedWriter(new FileWriter(output));
		for(ArrayList<String> c:clusters)
		{
			bw.write(c+"\n");
		}
		bw.close();
		
	}
	/**
	 * tags entities from teknowbase in the list of files
	 * @param outputfile
	 * @param phrasefile
	 * @throws Exception
	 */
	
	public static void tagEntitiesPhrases(String outputfile, String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    Aspect a11 = new Aspect();
 	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
 	   System.out.println("read knowledge graph");
 	   
 	   Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	   for(String entity : ac.nodeMap1.keySet())
       {
           trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
       }
       Trie trie = trie_builder.build();
	    
       System.out.println("read trie");
 	    HashSet<String> entities = new HashSet<String>();
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	 BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	    String line;
	 	    
	 	   System.out.println(line1);
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			
	 			for(Emit e:named_entity_occurences)
	 			{
	 				entities.add(e.getKeyword());
	 			}
	 		}
	 	    
	    }
	    
	    BufferedWriter bw = new BufferedWriter(new FileWriter(outputfile));
	    for(String en:entities)
	    {
	    	bw.write(en+"\n");
	    }
	    bw.close();
	}
	
	public static void indexSet()
	{
		HashSet<String> hh = new HashSet<>();
		HashSet<String> hh1 = new HashSet<>();
		HashSet<String> hh2 = new HashSet<>();

		HashMap<HashSet<String>, String> hm = new HashMap<HashSet<String>, String>();
		hh.add("a");
		hh.add("b");
		hh1.add("a");
		hh1.add("b");
		hh2.add("a");
		hh2.add("b");
		hm.put(hh, "yeah");
		hm.put(hh1, "noooo!!! :(");
		hm.put(hh1, "3rd one!!! :(");
		int c=1;
		for(HashSet<String> hhh:hm.keySet())
		{
			System.out.println("set no. "+c+"\n");
			c++;
			for(String s:hhh)
			{
				System.out.println(s+" ");
			}
			System.out.println(hm.get(hhh));
		}
	}
	
	public static void keepUniquePhrases(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	    String line;
	 	    Aspect a11 = new Aspect();
	 	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	   		
	 	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
	 	    for(String entity : ac.nodeMap1.keySet())
	        {
	            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
	        }
	        Trie trie = trie_builder.build();
	        
	        HashMap<HashSet<String>, ArrayList<String>> entsToPhrase = new HashMap<HashSet<String>, ArrayList<String>>();
	 	    HashMap<String, Double> phraseScore = new HashMap<String, Double>();
	        while((line=br.readLine())!=null)
	 		{
	 	    	HashSet<String> entities = new HashSet<String>();
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			double value = Double.parseDouble(arr[1].replace(")", ""));
	 			phraseScore.put(phrase, value);
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			
	 			for(Emit e:named_entity_occurences)
	 			{
	 				entities.add(e.getKeyword());
	 			}
	 			if(entsToPhrase.get(entities)==null)
	 			{
	 				ArrayList<String> phraseList = new ArrayList<String>();
	 				phraseList.add(phrase);
	 				entsToPhrase.put(entities, phraseList);
	 			}
	 			else
	 			{
	 				ArrayList<String> phraseList = entsToPhrase.get(entities);
	 				phraseList.add(phrase);
	 				entsToPhrase.put(entities, phraseList);
	 			}
	 		}
	        BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/uniquePhrases/"+line1));
	        for(HashSet<String> ent:entsToPhrase.keySet())
	        {
	        	double sum=0.0;
	        	double max=0.0;
	        	
	        	String final_phrase="";
	        	ArrayList<String> ph = entsToPhrase.get(ent);
	        	for(String p:ph)
	        	{
	        		if(phraseScore.get(p)>max) 
	        		{
	        			max = phraseScore.get(p);
	        			final_phrase = p;
	        		}
	        		sum = sum + phraseScore.get(p);	
	        	}
	        	bw.write("(u'"+final_phrase+"', "+max+")\n");
	        }
	        bw.close();
	 	    
	 	    
	    }
	}
	
	public static void readWikipedia(String wikipedia, String new_graph) throws Exception
	{
		Aspect a11 = new Aspect();
		 MutableValueGraph<String, Integer> wiki1 = a11.readGraphEfficientWiki(wikipedia);
	  	MutableValueGraph<String, Integer> wiki = a11.addGraph(wiki1, new_graph);
	  	ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki1);
	  		
	  	while(true)
	  	{
	  		Scanner sc= new Scanner(System.in);
	  		System.out.print("Enter a concept: ");  
	  		String str= sc.nextLine();  
	  		//reads string  
	  		if(!wiki_i.nodes().contains(str))
	  			{
	  				System.out.println("does not contain concept");
	  				continue;
	  			}
        	for(String n:wiki_i.adjacentNodes(str))
			{
        		System.out.println(n);
			}
	  	}
	  		
	}
	
	public static void assignSubtopicScore(String queries) throws Exception
	{
			BufferedReader br = new BufferedReader(new FileReader(queries));
		    String line;
		    while((line=br.readLine())!=null)
		    {
		    	HashMap<String, Double> pair_scores = new HashMap<String, Double>();
		    	BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/pairwise_scores/"+line));
		    	String line1;
		    	double tot=0.0;
		    	while((line1=br1.readLine())!=null)
		    	{
		    		String[] arr = line1.split("\t");
			    	tot = tot+Double.parseDouble(arr[1]);
			    	pair_scores.put(arr[0], Double.parseDouble(arr[1]));
		    	}
		    	for(String p:pair_scores.keySet())
		    	{
		    		pair_scores.put(p, pair_scores.get(p)/tot);
		    	}
		    	
		    	HashMap<String, Double> refd_scores = new HashMap<String, Double>();
		    	BufferedReader br2 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/refdScores/"+line));
		    	String line2;
		    	double tot2=0.0;
		    	while((line2=br2.readLine())!=null)
		    	{
		    		String[] arr = line2.split("\t");
		    		if(pair_scores.get(arr[0])!=null)
		    		{
			    	//tot = tot+Double.parseDouble(arr[1]);
		    			refd_scores.put(arr[0], Double.parseDouble(arr[1])*pair_scores.get(arr[0]));
		    		}
		    	}
		    	ArrayList<Entry<String, Double>> scores_sorted = new ArrayList<Entry<String, Double>>(refd_scores.entrySet());
		    	Collections.sort(scores_sorted, LanguageModelEntity.valueComparator);
		    	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopic_scores/"+line));
		    	
		    	for(Entry e:scores_sorted)
		    	{
		    		bw.write(e.getKey()+"\t"+e.getValue()+"\n");
		    	}
		    		
		    	bw.close();
		    	
		    }
	}
	
	public static void computeRefDScores(String queries, String wikipedia) throws Exception
	{
			ReadSubgraph rr = new ReadSubgraph();
			AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
		    
		    MutableValueGraph<String, Integer> weightedGraph = ValueGraphBuilder.directed().allowsSelfLoops(true).build();
		    
		    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		    Date  date = new Date();
		    System.out.println("read wikipedia "+dateFormat.format(date));
		    
		    BufferedReader br = new BufferedReader(new FileReader(queries));
		    String line;
		    while((line=br.readLine())!=null)
		    {
		    	System.out.println(line);
			    String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_entities/"+line;
		    	BufferedReader br1 = new BufferedReader(new FileReader(phrasefile));
		 	    String line1;
			    HashMap<String, Double> h = new HashMap<String, Double>();
			    
			    while((line1=br1.readLine())!=null)
			    {
			    	String[] arr = line1.split("\t");
			    	if(arr[0].length()<5) continue;
			    	h.put(arr[0],0.0);
			    }
			    
			    HashMap<String, Double> refdScores = new HashMap<String, Double>();
			    for(String s:h.keySet())
			    {
			    	double refd = a.referenceDistance(line, s);
			    	//if(refd>0.02)
			    		refdScores.put(s, refd);
			    }
			    ArrayList<Entry<String, Double>> refdList = new ArrayList<Entry<String, Double>>(refdScores.entrySet());
			    Collections.sort(refdList, LanguageModelEntity.valueComparator);
			    BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/refdScores/"+line));
			    for(Entry e:refdList)
			    {
			    	bw.write(e.getKey()+"\t"+e.getValue()+"\n");
			    }
			    bw.close();
		    }
	}
	    
	
	public static void chooseCenters(int k, String wikipedia, String new_graph, String query) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		Aspect a11 = new Aspect();
		MutableValueGraph<String, Integer> wiki1 = a11.readGraphEfficientWiki(wikipedia);
	  	MutableValueGraph<String, Integer> wiki = a11.addGraph(wiki1, new_graph);
	  	ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki1);
	  	
	  	date = new Date();
		System.out.println("read wikipedia "+dateFormat.format(date));
		
	  	String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    
	    HashSet<String> missing_entities = new HashSet<String>();
	    
 	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
 	    
 	    date = new Date();
		System.out.println("read teknowbase "+dateFormat.format(date));
 	    
 	    BufferedReader br = new BufferedReader(new FileReader(query));
 	    String line;
 	    
 	    while((line=br.readLine())!=null)
 	    {
 	    	System.out.println(line);
 	    	date = new Date();
 			System.out.println(dateFormat.format(date));
 	    	if(!wiki_i.nodes().contains(line)) continue;
 	    	HashMap<String, Double> sp_scores = new HashMap<String, Double>();
 	    	for(String s:wiki_i.adjacentNodes(line))
 	    	{
 	    		if(ac.nodeMap1.get(s)==null) continue;
 	    		int s_node = ac.nodeMap1.get(s);
 	    		File f = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/shortest_paths/"+s_node);
 	    		if(f.exists()) 
 	    		{
 	    			//continue;
 	    			BufferedReader br1 = new BufferedReader(new FileReader(f.getAbsolutePath()));
 	    			String line1;
 	    			while((line1=br1.readLine())!=null)
 	    			{
 	    				String arr[] = line1.split("\t");
 	    				int node = Integer.parseInt(arr[0]);
 	    				String node_s = ac.nodeMap.get(node);
 	    				double sp = Double.parseDouble(arr[1]);
 	    				if(sp_scores.get(node_s)==null)
 	    				{
 	    					sp_scores.put(node_s, (double)1.0/sp);
 	    				}
 	    				else
 	    				{
 	    					sp_scores.put(node_s, sp_scores.get(node_s)+ (double)1.0/sp);
 	    				}
 	    			}
 	    			
 	    		}
 	    		else
 	    		{
 	    			missing_entities.add(s);
 	    			//System.out.println("sp missing for "+s);
 	    		}
 	    		
 	    	}
 	    	ArrayList<Entry<String, Double>> sp_scores_sorted = new ArrayList<Entry<String, Double>>(sp_scores.entrySet());
 	  	   Collections.sort(sp_scores_sorted, LanguageModelEntity.valueComparator);
 	  	   BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/k-means/tkb/"+line));
 	  	   
 	  	   
 	  	   for(Entry e:sp_scores_sorted)
 	  	   {
 	  		   int node = ac.nodeMap1.get(e.getKey());
 	  		   bw.write(e.getKey()+"\t"+e.getValue()+"\t"+ac.undirectedGraph.adjacentNodes(node).size()+"\n");
 	  	   }
 	  	   bw.close();
 	    	
 	    }
 	    BufferedWriter bw1 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/missing_sp_entities/entities"));
 	    for(String ss:missing_entities)
 	    {
 	    	bw1.write(ss+"\n");
 	    }
 	    bw1.close();
	}
	
	public static void entityDistribution(String queries) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(queries));
		String line;
		while((line=br.readLine())!=null)
		{
			File f = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopics");
			
		}
	}
	
	public static void starClusterRefD(String queries, String wikipedia) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    Aspect a11 = new Aspect();
	    HashSet<String> missing_entities = new HashSet<String>();
	    
 	    AdjListCompact ac = a11.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
 	    
 	   ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
 	    
 	    MutableValueGraph<String, Integer> weightedGraph = ValueGraphBuilder.directed().allowsSelfLoops(true).build();
 	    
 	    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
 	    Date  date = new Date();
		System.out.println("read teknowbase "+dateFormat.format(date));
 	    
 	    BufferedReader br = new BufferedReader(new FileReader(queries));
 	    String line;
 	    while((line=br.readLine())!=null)
 	    {
 	    	System.out.println(line);
 	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_entities/"+line;
	    	BufferedReader br1 = new BufferedReader(new FileReader(phrasefile));
	 	    String line1;
	 	   
	 	    
	 	    HashMap<String, Double> refdScores = new HashMap<String, Double>();
	    	br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/refdScores/"+line));
	    	 
	    	while((line1=br1.readLine())!=null)
	 	    {
	 	    	String[] arr = line1.split("\t");
	 	    	if(arr[0].length()<5) continue;
	 	    	double score = Double.parseDouble(arr[1]);
	 	    	//if(score>0.0)
	 	    		refdScores.put(arr[0],score);
	 	    }
	    	
	    	 for(String n:refdScores.keySet())
		 	    {
		 	    	if(ac.nodeMap1.get(n)==null) continue;
		 	    	int e_code = ac.nodeMap1.get(n);
		 	    	if(!ac.labeledGraph.nodes().contains(e_code)) continue;
		 	    	for(int neigh:ac.labeledGraph.adjacentNodes(e_code))
		 	    	{
		 	    		String neigh_string = ac.nodeMap.get(neigh);
		 	    		Optional c1 = ac.labeledGraph.edgeValue(e_code, neigh);
		 	    		if(refdScores.get(neigh_string)!=null)
		 	    		{
		 	    			if(c1.isPresent())
		 	    			{
		 	    				ArrayList<Integer> c = (ArrayList<Integer>) c1.get();
		 	    				for(int cc:(ArrayList<Integer>) c)
		 	    				{
		 	    					if(ac.relmap1.get(cc)!=null)
		 	    					{
		 	    						String rel = ac.relmap1.get(cc);
		 	    						if(rel.contains("_inverse"))
		 	    						{
		 	    							continue;
		 	    						}
		 	    						else
		 	    						{
		 	    							weightedGraph.putEdgeValue(n, neigh_string, 1);
		 	    						}
		 	    					}
		 	    				}
		 	    			}
		 	    		}
		 	    		
		 	    	}
		 	    	
		 	    }
		 	    HashMap<String, Double> h1 = new HashMap<String, Double>();
		 	    for(String n:weightedGraph.nodes())
		 	    {
		 	    	h1.put(n, (double) weightedGraph.adjacentNodes(n).size());
		 	    }
		 	   ArrayList<Entry<String, Double>> en = new ArrayList<Entry<String, Double>>(h1.entrySet());
			   Collections.sort(en, LanguageModelEntity.valueComparator);
			  
			   ArrayList<ArrayList<String>> clusters = new ArrayList<ArrayList<String>>();
				HashSet<String> marked = new HashSet<String>();
				for(Entry<String, Double> e:en)
				{
					String a1 = e.getKey();
					int a1_code = ac.nodeMap1.get(a1);
					if(marked.contains(a1)) continue;
					if(!ac.labeledGraph.nodes().contains(a1_code)) continue;
					if(!weightedGraph.nodes().contains(a1)) continue;
					ArrayList<String> list1 = new ArrayList<String>();
					marked.add(a1);
					list1.add(a1);
					for(String adj:weightedGraph.adjacentNodes(a1))
					{
						if(marked.contains(adj)) continue;
						list1.add(adj);
						marked.add(adj);
					}
					clusters.add(list1);
					
				}
				
				HashMap<ArrayList<String>, Double> refd_sorted = new HashMap<ArrayList<String>, Double>();
			
				for(ArrayList<String> c:clusters)
				{
					double sum=0.0;
					double sum1=0.0;
					for(String s:c)
					{
						if(refdScores.get(s)==null) continue;
						double refd = refdScores.get(s);
						if(refd>0)
							sum = sum + refd;
					}
					refd_sorted.put(c, sum);
				}
				
				ArrayList<Entry<ArrayList<String>, Double>> ec_refd = new ArrayList<Entry<ArrayList<String>, Double>>(refd_sorted.entrySet());
				Collections.sort(ec_refd, LanguageModelEntity.valueComparatorList);
				
				
				BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/refd_clustered_1/"+line));
				for(Entry<ArrayList<String>, Double> e:ec_refd)
				{
					HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
					ArrayList<String> c = e.getKey();
					for(String cc:c)
					{
						//if(refdScores.get(cc)==null) continue;
						ent_ordered.put(cc, h1.get(cc));
					}
					ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
					Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
					int count=0;
					for(Entry<String, Double> cc:c_ordered)
					{
						bw.write(cc+"\t");
						count++;
						//if(count==15) break;
					}
					bw.write("\n");
					//bw1.write(c+"\t"+e.getValue()+"\n");
				}
				bw.close();
 	    }
	 	    
	}
	
	
	
	public static void starClusterEntities(String queries, String wikipedia) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    Aspect a11 = new Aspect();
	    HashSet<String> missing_entities = new HashSet<String>();
	    
 	    AdjListCompact ac = a11.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
 	    
 	   ReadSubgraph rr = new ReadSubgraph();
		//AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
 	    
 	    MutableValueGraph<String, Integer> weightedGraph = ValueGraphBuilder.directed().allowsSelfLoops(true).build();
 	    
 	    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
 	    Date  date = new Date();
		System.out.println("read teknowbase "+dateFormat.format(date));
 	    
 	    BufferedReader br = new BufferedReader(new FileReader(queries));
 	    String line;
 	    while((line=br.readLine())!=null)
 	    {
 	    	System.out.println(line);
 	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_entities/"+line;
	    	BufferedReader br1 = new BufferedReader(new FileReader(phrasefile));
	 	    String line1;
	 	    HashMap<String, Double> h = new HashMap<String, Double>();
 	    
	 	    while((line1=br1.readLine())!=null)
	 	    {
	 	    	String[] arr = line1.split("\t");
	 	    	if(arr[0].length()<5) continue;
	 	    	h.put(arr[0],0.0);
	 	    }
	 	    
	 	    HashMap<String, Double> refdScores = new HashMap<String, Double>();
	    	br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/refdScores/"+line));
	    	 
	    	while((line1=br1.readLine())!=null)
	 	    {
	 	    	String[] arr = line1.split("\t");
	 	    	if(arr[0].length()<5) continue;
	 	    	refdScores.put(arr[0],Double.parseDouble(arr[1]));
	 	    }
	 	    
	 	    
	 	    HashMap<String, Double> subtopicScores = new HashMap<String, Double>();
	 	    
	 	    br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopic_scores/"+line));
	 	    while((line1=br1.readLine())!=null)
	 	    {
	 	    	String[] arr = line1.split("\t");
	 	    	subtopicScores.put(arr[0],Double.parseDouble(arr[1]));
	 	    }
	 	    
	 	    for(String n:refdScores.keySet())
	 	    {
	 	    	if(ac.nodeMap1.get(n)==null) continue;
	 	    	int e_code = ac.nodeMap1.get(n);
	 	    	if(!ac.labeledGraph.nodes().contains(e_code)) continue;
	 	    	for(int neigh:ac.labeledGraph.adjacentNodes(e_code))
	 	    	{
	 	    		String neigh_string = ac.nodeMap.get(neigh);
	 	    		Optional c1 = ac.labeledGraph.edgeValue(e_code, neigh);
	 	    		if(h.get(neigh_string)!=null && refdScores.get(neigh_string)!=null)
	 	    		{
	 	    			if(c1.isPresent())
	 	    			{
	 	    				ArrayList<Integer> c = (ArrayList<Integer>) c1.get();
	 	    				for(int cc:(ArrayList<Integer>) c)
	 	    				{
	 	    					if(ac.relmap1.get(cc)!=null)
	 	    					{
	 	    						String rel = ac.relmap1.get(cc);
	 	    						if(rel.contains("_inverse"))
	 	    						{
	 	    							continue;
	 	    						}
	 	    						else
	 	    						{
	 	    							weightedGraph.putEdgeValue(n, neigh_string, 1);
	 	    						}
	 	    					}
	 	    				}
	 	    			}
	 	    		}
	 	    		
	 	    	}
	 	    	
	 	    }
	 	    HashMap<String, Double> h1 = new HashMap<String, Double>();
	 	    for(String n:weightedGraph.nodes())
	 	    {
	 	    	h1.put(n, (double) weightedGraph.adjacentNodes(n).size());
	 	    }
	 	   ArrayList<Entry<String, Double>> en = new ArrayList<Entry<String, Double>>(h1.entrySet());
		   Collections.sort(en, LanguageModelEntity.valueComparator);
		  
	 	    
	 	   ArrayList<ArrayList<String>> clusters = new ArrayList<ArrayList<String>>();
			HashSet<String> marked = new HashSet<String>();
			for(Entry<String, Double> e:en)
			{
				String a1 = e.getKey();
				int a1_code = ac.nodeMap1.get(a1);
				if(marked.contains(a1)) continue;
				marked.add(a1);
				ArrayList<String> list1 = new ArrayList<String>();
				list1.add(a1);
				if(!ac.labeledGraph.nodes().contains(a1_code)) continue;
				if(!weightedGraph.nodes().contains(a1)) continue;
					for(String adj:weightedGraph.adjacentNodes(a1))
					{
						
						if(marked.contains(adj)) continue;
						list1.add(adj);
						marked.add(adj);
					}
					clusters.add(list1);
				
			}
			
			HashMap<ArrayList<String>, Double> clusters_sorted = new HashMap<ArrayList<String>, Double>();
			
			for(ArrayList<String> c:clusters)
			{
				double sum=0.0;
				for(String s:c)
				{
					if(h.get(s)==null) continue;
					sum = sum + h.get(s);
				}
				clusters_sorted.put(c, sum);
			}
			
			HashMap<ArrayList<String>, Double> refd_sorted = new HashMap<ArrayList<String>, Double>();
			HashMap<String, Double> refdscores = new HashMap<String, Double>();
			
			HashMap<ArrayList<String>, Double> both_sorted = new HashMap<ArrayList<String>, Double>();
			
			/*for(ArrayList<String> c:clusters)
			{
				double sum=0.0;
				double sum1=0.0;
				for(String s:c)
				{
					if(h.get(s)==null) continue;
					double refd = a.referenceDistance(line, s);
					refdscores.put(s, refd);
					if(refd>0)
						sum = sum + refd;
					sum1 = sum1 + h.get(s);
				}
				refd_sorted.put(c, sum);
				both_sorted.put(c, sum*sum1);
			}*/
			
			HashMap<ArrayList<String>, Double> subtopic_sorted = new HashMap<ArrayList<String>, Double>();
			
			for(ArrayList<String> c:clusters)
			{
				double sum=0.0;
				double sum1=0.0;
				for(String s:c)
				{
					if(subtopicScores.get(s)==null) continue;
					double sscore = subtopicScores.get(s);
					sum = sum + sscore;
					sum1 = sum1 + h.get(s);
				}
				subtopic_sorted.put(c, 1.0*c.size());
			}
			
			ArrayList<Entry<ArrayList<String>, Double>> ec = new ArrayList<Entry<ArrayList<String>, Double>>(clusters_sorted.entrySet());
			Collections.sort(ec, LanguageModelEntity.valueComparatorList);
			
			ArrayList<Entry<ArrayList<String>, Double>> ec_refd = new ArrayList<Entry<ArrayList<String>, Double>>(refd_sorted.entrySet());
			Collections.sort(ec_refd, LanguageModelEntity.valueComparatorList);
			
			ArrayList<Entry<ArrayList<String>, Double>> ec_both = new ArrayList<Entry<ArrayList<String>, Double>>(both_sorted.entrySet());
			Collections.sort(ec_both, LanguageModelEntity.valueComparatorList);
			
			ArrayList<Entry<ArrayList<String>, Double>> ec_subtopic = new ArrayList<Entry<ArrayList<String>, Double>>(subtopic_sorted.entrySet());
			Collections.sort(ec_subtopic, LanguageModelEntity.valueComparatorList);
			
			BufferedWriter bw1 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_unordered"));
	
			BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line));
			
			BufferedWriter bw2 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_refd_ordered"));
	
			BufferedWriter bw3 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_both_ordered"));
			
			BufferedWriter bw4 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_subtopics_ordered"));
	
			for(Entry<ArrayList<String>, Double> e:ec)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(h.get(cc)==null) continue;
					ent_ordered.put(cc,h.get(cc));
				}
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw.write(c_ordered+"\t"+e.getValue()+"\n");
				bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			for(Entry<ArrayList<String>, Double> e:ec_refd)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(refdScores.get(cc)==null) continue;
					ent_ordered.put(cc,refdscores.get(cc));
				}
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw2.write(c_ordered+"\t"+e.getValue()+"\n");
				//bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			for(Entry<ArrayList<String>, Double> e:ec_both)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(refdScores.get(cc)==null) continue;
					ent_ordered.put(cc,refdscores.get(cc));
				}
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw3.write(c_ordered+"\t"+e.getValue()+"\n");
				//bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			for(Entry<ArrayList<String>, Double> e:ec_subtopic)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(subtopicScores.get(cc)==null) continue;
					ent_ordered.put(cc,subtopicScores.get(cc));
				}
				
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw4.write(c_ordered+"\t"+e.getValue()+"\n");
				//bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			bw.close();
			bw1.close();
			bw2.close();
			bw3.close();
			bw4.close();
 	    }
 	    
	}
	
	public static void extractEntScore(String file) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
 	    Aspect a11 = new Aspect();
	    
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	   	for(String entity : ac.nodeMap1.keySet())
        {
 	   		int e_code= ac.nodeMap1.get(entity);
 	   		if(ac.undirectedGraph.nodes().contains(e_code) && entity.length()>4)
 	   		{
 	   			trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
 	   		}
        }
        Trie trie = trie_builder.build();
	   while((line=br.readLine())!=null)
	   {
		   	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/lda/clusters_phrases/"+line));

		    String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/"+line;
		    BufferedReader br1 = new BufferedReader(new FileReader(phrasefile));
		    String line1;
		    HashMap<String, Double> h = new HashMap<String, Double>();
	    
		    while((line1=br1.readLine())!=null)
		    {
		    	String[] arr = line1.split("\t");
		    	if(arr[0].length()<5) continue;
		    	h.put(arr[0],Double.parseDouble(arr[1]));
		    }
			   
		    String topicfile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/lda/results_with_scores_phrases/"+line;
			System.out.println(topicfile);
		    br1 = new BufferedReader(new FileReader(topicfile));

		    HashMap<String, ArrayList<String>> topicToEntities = new HashMap<String, ArrayList<String>>();
			
		    HashMap<String, ArrayList<Entry<String, Double>>> topicToEntitiesFreq = new HashMap<String, ArrayList<Entry<String, Double>>>();
		    
		    HashMap<String, Double> topicScore = new HashMap<String, Double>();
		    
		    HashMap<String, Edge> hh = new HashMap<String, Edge>();
		    
		    while((line1=br1.readLine())!=null)
			{
		    	HashMap<String, Double> entfreq = new HashMap<String, Double>();
				String[] arr = line1.split("\t");
				if(arr.length<2) continue;
				//double prob = Double.parseDouble(arr[2]);
				//long topic = Long.parseLong(arr[0]);
				Collection<Emit> ce = trie.parseText(arr[1].toLowerCase());
				System.out.println(ce.size());
				ArrayList<String> ent_list = new ArrayList<String>();
				//Edge ee1 = new Edge(topic, prob);
				for(Emit e1:ce)
				{
					String ent = e1.getKeyword().replace(" ", "_");
					if(h.get(ent)==null) continue;
					if(entfreq.get(e1.getKeyword().replace(" ", "_"))==null)
					{
						entfreq.put(e1.getKeyword().replace(" ", "_"), 1.0);
					}
					else
					{
						entfreq.put(e1.getKeyword().replace(" ", "_"), entfreq.get(e1.getKeyword().replace(" ", "_"))+1);
					}
					//ent_list.add(e1.getKeyword().replace(" ", "_"));
					/*if(hh.get(ent)==null)
					{
						hh.put(ent, ee1);
					}
					else
					{
						Edge eee = hh.get(ent);
						if(eee.rel<prob)
						{
							eee.rel=prob;
							eee.document=topic;
						}
						hh.put(ent, eee);
					}*/
				}
				ArrayList<Entry<String, Double>> entries_ent = new ArrayList<Entry<String, Double>>(entfreq.entrySet());
				Collections.sort(entries_ent, LanguageModelEntity.valueComparator);
				
				System.out.println(entries_ent.size());
				bw.write("[");
				for(Entry<String, Double> ee:entries_ent)
				{
					
			    	int count=0;
			    		
			    		if(count==entries_ent.size()-1)
			    		{
			    			bw.write(ee.getKey());
			    		}
			    		else
			    		{
			    			bw.write(ee.getKey()+", ");
			    		}
			    		count++;
			    	
			    	
			    	
					//ent_list.add(ee.getKey());
				}
				bw.write("]\t"+entries_ent.size()+"\n");
				
				//topicToEntities.put(arr[0], ent_list);
				
			}
		    bw.close();
		    
		   /* for(String s:hh.keySet())
		    {
		    	long topic = hh.get(s).getDoc();
		    	String tstring = Long.toString(topic);
		    	if(topicToEntities.get(tstring)==null)
		    	{
		    		HashSet<String> ee = new HashSet<String>();
		    		ee.add(s);
		    		topicToEntities.put(tstring, ee);
		    	}
		    	else
		    	{
		    		HashSet<String> ee = topicToEntities.get(tstring);
		    		ee.add(s);
		    		topicToEntities.put(tstring, ee);
		    	}
		    }*/
			
		   /* for(String t:topicToEntities.keySet())
		    {
		    	double score = 0.0;
		    	
		    	for(String ee:topicToEntities.get(t))
		    	{
		    		if(h.get(ee)==null) continue;
		    		score = score + h.get(ee);
		    	}
		    	topicScore.put(t, score);
		    }
		    ArrayList<Entry<String, Double>> topicScoresSorted = new ArrayList<Entry<String, Double>>(topicScore.entrySet());
		    Collections.sort(topicScoresSorted, LanguageModelEntity.valueComparator);
		    for(String t:topicToEntities.keySet())
		    {
		    	
		    	bw.write("[");
		    	int count=0;
		    	for(String ee:topicToEntities.get(t))
		    	{
		    		
		    		if(count==topicToEntities.get(t).size()-1)
		    		{
		    			bw.write(ee);
		    		}
		    		else
		    		{
		    			bw.write(ee+", ");
		    		}
		    		count++;
		    	}
		    	
		    	bw.write("]\t0\n");
		    }
		    bw.close();*/
	   }
	}
	
	public static void extractEnt(String file) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
 	    Aspect a11 = new Aspect();
	    
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	   	for(String entity : ac.nodeMap1.keySet())
        {
 	   		int e_code= ac.nodeMap1.get(entity);
 	   		if(ac.undirectedGraph.nodes().contains(e_code) && entity.length()>4)
 	   		{
 	   			trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
 	   		}
        }
        Trie trie = trie_builder.build();
	   while((line=br.readLine())!=null)
	   {
		   	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/lda/clusters/"+line));

		    String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/"+line;
		    BufferedReader br1 = new BufferedReader(new FileReader(phrasefile));
		    String line1;
		    HashMap<String, Double> h = new HashMap<String, Double>();
	    
		    while((line1=br1.readLine())!=null)
		    {
		    	String[] arr = line1.split("\t");
		    	if(arr[0].length()<5) continue;
		    	h.put(arr[0],Double.parseDouble(arr[1]));
		    }
			   
		    String topicfile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/lda/results_with_scores/"+line;
			System.out.println(topicfile);
		    br1 = new BufferedReader(new FileReader(topicfile));

		    HashMap<String, HashSet<String>> topicToEntities = new HashMap<String, HashSet<String>>();
			
		    HashMap<String, ArrayList<Entry<String, Double>>> topicToEntitiesFreq = new HashMap<String, ArrayList<Entry<String, Double>>>();
		    
		    HashMap<String, Double> topicScore = new HashMap<String, Double>();
		    
		    HashMap<String, Edge> hh = new HashMap<String, Edge>();
		    
		    while((line1=br1.readLine())!=null)
			{
		    	HashMap<String, Double> entfreq = new HashMap<String, Double>();
				String[] arr = line1.split("\t");
				if(arr.length<3) continue;
				double prob = Double.parseDouble(arr[2]);
				long topic = Long.parseLong(arr[0]);
				Collection<Emit> ce = trie.parseText(arr[1].toLowerCase());
				HashSet<String> ent_list = new HashSet<String>();
				Edge ee1 = new Edge(topic, prob);
				for(Emit e1:ce)
				{
					String ent = e1.getKeyword().replace(" ", "_");
					if(h.get(ent)==null) continue;
					/*if(entfreq.get(e1.getKeyword().replace(" ", "_"))==null)
					{
						entfreq.put(e1.getKeyword().replace(" ", "_"), 1.0);
					}
					else
					{
						entfreq.put(e1.getKeyword().replace(" ", "_"), entfreq.get(e1.getKeyword().replace(" ", "_"))+1);
					}*/
					//ent_list.add(e1.getKeyword().replace(" ", "_"));
					if(hh.get(ent)==null)
					{
						hh.put(ent, ee1);
					}
					else
					{
						Edge eee = hh.get(ent);
						if(eee.rel<prob)
						{
							eee.rel=prob;
							eee.document=topic;
						}
						hh.put(ent, eee);
					}
				}
				/*ArrayList<Entry<String, Double>> entries_ent = new ArrayList<Entry<String, Double>>(entfreq.entrySet());
				Collections.sort(entries_ent, LanguageModelEntity.valueComparator);
				
				for(Entry<String, Double> ee:entries_ent)
				{
					ent_list.add(ee.getKey());
				}
				topicToEntities.put(arr[0], ent_list);*/
			}
		    for(String s:hh.keySet())
		    {
		    	long topic = hh.get(s).getDoc();
		    	String tstring = Long.toString(topic);
		    	if(topicToEntities.get(tstring)==null)
		    	{
		    		HashSet<String> ee = new HashSet<String>();
		    		ee.add(s);
		    		topicToEntities.put(tstring, ee);
		    	}
		    	else
		    	{
		    		HashSet<String> ee = topicToEntities.get(tstring);
		    		ee.add(s);
		    		topicToEntities.put(tstring, ee);
		    	}
		    }
			
		   /* for(String t:topicToEntities.keySet())
		    {
		    	double score = 0.0;
		    	
		    	for(String ee:topicToEntities.get(t))
		    	{
		    		if(h.get(ee)==null) continue;
		    		score = score + h.get(ee);
		    	}
		    	topicScore.put(t, score);
		    }
		    ArrayList<Entry<String, Double>> topicScoresSorted = new ArrayList<Entry<String, Double>>(topicScore.entrySet());
		    Collections.sort(topicScoresSorted, LanguageModelEntity.valueComparator);*/
		    for(String t:topicToEntities.keySet())
		    {
		    	
		    	bw.write("[");
		    	int count=0;
		    	for(String ee:topicToEntities.get(t))
		    	{
		    		
		    		if(count==topicToEntities.get(t).size()-1)
		    		{
		    			bw.write(ee);
		    		}
		    		else
		    		{
		    			bw.write(ee+", ");
		    		}
		    		count++;
		    	}
		    	
		    	bw.write("]\t0\n");
		    }
		    bw.close();
	   }
		
	}
	
	public static void convertPhraseToVectorUsingTKB(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    
	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    Aspect a11 = new Aspect();
 	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
 	    System.out.println("read teknowbase");
 	    ReadSubgraph rr = new ReadSubgraph();
 	    AdjList a1 = new AdjList();
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	    String line;
	 	    System.out.println(line1);
	 	    //HashMap<String, LanguageModelEntity> lm = new HashMap<String, LanguageModelEntity>();
	 	    Set<LanguageModelEntity> lm = new HashSet<LanguageModelEntity>();
	 	    HashSet<String> words = new HashSet<String>();
	    }
	}
	
	/**
	 * main function to read phrases and cluster them in hierarchical fashion
	 * @param tkb
	 * @param queries
	 * @throws Exception
	 */
	
	public static void clusterPhrases(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    
	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    Aspect a11 = new Aspect();
 	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
 	    System.out.println("read teknowbase");
 	    ReadSubgraph rr = new ReadSubgraph();
 	    AdjList a1 = new AdjList();
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	    String line;
	 	    System.out.println(line1);
	 	    //HashMap<String, LanguageModelEntity> lm = new HashMap<String, LanguageModelEntity>();
	 	    Set<LanguageModelEntity> lm = new HashSet<LanguageModelEntity>();
	 	    HashSet<String> words = new HashSet<String>();
	 	    BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/hierarchical_cluster/"+line1));
	 	    HashSet<String> phrase_list = new HashSet<String>();
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			String phrase = arr[0].substring(3);
	 			String[] words1 = phrase.split(" ");
				for(String ss:words1)
				{
					//System.out.println(ss);
					words.add(ss);
				}
				phrase_list.add(phrase);
				
	 		}
	 	    for(String phrase:phrase_list)
	 	    {
		 	   HashMap<String, Double> mixture = new HashMap<String, Double>();
				HashSet<String> tokhash = new HashSet<String>();
				String[] words1 = phrase.split(" ");
				for(String token:words1)
				{
					if(mixture.get(token)==null) mixture.put(token, 1.0);
					else
						mixture.put(token, mixture.get(token)+1);
				}
				for(String token:words)
				{
					if(mixture.get(token)==null)
					{
						mixture.put(token, 0.0);
					}
				}
				LanguageModelEntity lm1 = new LanguageModelEntity(phrase,a1,new HashMap<String, Double>(),new HashMap<String, HashMap<String, Double>>(), mixture);
			lm.add(lm1);
	 	    }
	 	    System.out.println("read phrase representations");
	 	    UtilityFunctions u = new UtilityFunctions();
	 	    u.clusterHierarchicalPhrases(lm, bw);
	 	    bw.close();
	 	     
	    }
	    
	}
	
	public static void useQDMKB(String tkb_mapped, String nodemap, String relmap, String outfile, String queries) throws Exception
	{
		BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    QDMKB qdmkb = new QDMKB(tkb_mapped, nodemap, relmap);
		System.out.println("read graph");
	    while((line1=br1.readLine())!=null)
	    {
			String query =line1;
			qdmkb.getFirstHopProperties(query, outfile);
	    }
	}
	
	public static void useQDMKBInt(String tkb_mapped, String nodemap, String relmap, String outfile, String queries) throws Exception
	{
		BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    QDMKB qdmkb = new QDMKB(tkb_mapped, nodemap, relmap);
		System.out.println("read graph");
	    while((line1=br1.readLine())!=null)
	    {
			String query =line1;
			qdmkb.getFirstHopProperties(query, outfile);
	    }
	}
	
	public static void tagEntitiesInDocs(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
 	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
	    for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        System.out.println("read graph and built trie");
        BufferedReader br = new BufferedReader(new FileReader(queries));
        String line;
        while((line=br.readLine())!=null)
        {
        	System.out.println(line);
        	BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/uniquePhrases/"+line));
        	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/uniquePhrasesMapped/"+line));
        	String line1;
        	int count=0;
        	while((line1=br1.readLine())!=null)
        	{
        		//String[] arr = line1.split("\t");
        		String[] arr = line1.split("', ");
	 			String phrase = arr[0].substring(3);
	 			System.out.println(phrase);
        		//String text = (line1.replace("\t", " ")).toLowerCase().replace("http://"," ").replace("<strong>", "").replace("</strong>", "").replace("[","").replace("(","").replace(")","").replace("]", "");
        		Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			HashSet<String> entities = new HashSet<String>();
        		for(Emit e:named_entity_occurences)
	 			{
	 				entities.add(e.getKeyword().replace(" ", "_"));
	 			}
        		for(String e:entities)
        		{
        			//bw.write(count+"\t"+e+"\n");
        			bw.write(phrase+"\t"+e+"\n");
        		}
        		//bw.write("\n");
        		count = count+1;
        	}
        	bw.close();
        }
	 	   
	}
	

	public static void tagEntitiesInClusters(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
 	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	    
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
	    for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        System.out.println("read graph and built trie");
        BufferedReader br = new BufferedReader(new FileReader(queries));
        String line;
        while((line=br.readLine())!=null)
        {
        	System.out.println(line);
        	File ff = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/clusters_ranked_ratio/complete/"+line+"_updated_11");
        	if(!ff.exists()) continue;
        	
        	BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/clusters_ranked_ratio/complete/"+line+"_updated_11"));
        	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/clusters_ranked_ratio/tagged/"+line));
        	
        	String line1;
        	while((line1=br1.readLine())!=null)
        	{
        		String[] arr = line1.split("\t");
        		Collection<Emit> named_entity_occurences = trie.parseText(arr[0].toLowerCase());
	 			
	 			ArrayList<String> entities = new ArrayList<String>();
        		for(Emit e:named_entity_occurences)
	 			{
	 				entities.add(e.getKeyword().replace(" ", "_"));
	 			}
        		bw.write(arr[0]);
        		for(String e:entities)
        		{
        			//bw.write(count+"\t"+e+"\n");
        			bw.write("\t"+e);
        		}
        		bw.write("\n");
        	}
        	bw.close();
        	
        }
	 	   
	}
	
	public static void createDocToEntities(String queries, String docfolder) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
 	    
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);

	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    while((line1=br1.readLine())!=null)
	    {
	    	
	    }
	    
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
	    for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
	}
	
	public static void rankItems(String queries, String entityMap, String embedding) throws Exception
	{
		UtilityFunctions uf = new UtilityFunctions();
		HashMap<String, String> stringToId = uf.createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> emb = uf.readEmbeddingsFromFile(embedding,stringToId,new AdjList());
		BufferedReader br = new BufferedReader(new FileReader(queries));
		String line;
		ArrayList<String> queries1 = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			queries1.add(line);
		}
		
		for(String q:queries1)
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/clusters_ranked_ratio/items_ranked_sim/"+q));
			BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/clusters_ranked_ratio/tagged/"+q));
			String line1;
			while((line1=br1.readLine())!=null)
			{
				HashMap<String, Double> entities = new HashMap<String, Double>();
				String[] arr = line1.split("\t");
				
				for(int i=1;i<arr.length;i++)
				{
					if (arr[i].equals("network")) continue;
					if(entities.get(arr[i])==null)
						//entities.put(arr[i], 1.0/(arr.length-1));
						entities.put(arr[i], 1.0);
					else
					{
						//entities.put(arr[i], entities.get(arr[i])+(1.0/(arr.length-1)));
						entities.put(arr[i], entities.get(arr[i])+1.0);

					}
				}
				ArrayList<Entry<String, Double>> ent_ordered = new ArrayList<Entry<String, Double>>(entities.entrySet());
				Collections.sort(ent_ordered, LanguageModelEntity.valueComparator);
				bw.write(ent_ordered.get(0).getKey());
				//System.out.println(q+"\t"+ent_ordered.get(ent_ordered.size()-1).getKey()+"\t"+ent_ordered.get(ent_ordered.size()-1).getValue());
				//System.out.println(q+"\t"+ent_ordered.get(0).getKey()+"\t"+ent_ordered.get(0).getValue());
				
				HashMap<String, Double> distance = new HashMap<String, Double>();
				String inp = uf.extractEmbedding(emb, ent_ordered.get(0).getKey());
				
				LanguageModelEntity l1 = emb.get(inp);
				if(l1==null) continue;
				System.out.println(emb.size());
				for(Entry<String, Double> e:ent_ordered)
				{
					//System.out.println(e);
					String e1 = uf.extractEmbedding(emb, e.getKey());
					if(e1==null) continue;
					if(e1.equals(inp)) continue;
					LanguageModelEntity l = emb.get(e1);
					if(l==null) continue;
					Double d = l1.cosineSimilarity(l);
					distance.put(e1,d*e.getValue());
				}
				ArrayList<Entry<String, Double>> distances = new ArrayList<Entry<String, Double>>(distance.entrySet());
				Collections.sort(distances, LanguageModelEntity.valueComparator);
				for(Entry<String, Double> entry:distances)
				{
					bw.write("\t"+entry.getKey());
				}
				bw.write("\n");
			}
			bw.close();
			
		}
	}
	
	public static void retrieveResults()
	{
		
	}
	
	public static void main(String args[]) throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		rankItems(hm.get("queries"), hm.get("mappings"), hm.get("emb"));
		//tagEntitiesInClusters(hm.get("queries"));
		assignSubtopicScore(hm.get("queries"));
		//starClusterEntities(hm.get("queries"),hm.get("wikipedia"));
		//useQDMKB(hm.get("teknowbase-mapped"), hm.get("nodemap"), hm.get("relmap"), hm.get("outputfile"), hm.get("queries"));
		//keepUniquePhrases(hm.get("queries"));
		//computeRefDScores(hm.get("queries"), hm.get("wikipedia"));
		computePhraseBOE(hm.get("queries"));
		//tagEntities(hm.get("queries"));
		computePhraseBOW(hm.get("queries"), hm.get("entities_1_hop"));
		//tagEntities(hm.get("outputfile"), hm.get("entities"));
		//clusterPhrases(hm.get("queries"));
		//computePhraseEmbedding(hm.get("outputfile"), hm.get("queries"));
		//computeSPEntities(hm.get("entities"));
		//extractEntScore(hm.get("queries"));
		starClusterEntities(hm.get("queries"), hm.get("wikipedia"));
		//starClusterRefD(hm.get("queries"), hm.get("wikipedia"));
		//chooseCenters(10, hm.get("wikipedia"),hm.get("new-graph"),hm.get("queries"));
		//readWikipedia(hm.get("wikipedia"), hm.get("new-graph"));
		//computePhraseScore(hm.get("queries"));
		//tagEntitiesPhrases(hm.get("outputfile"), hm.get("queries"));
		//computeEntityScore(hm.get("queries"), hm.get("wikipedia"), hm.get("new-graph"), hm.get("entities"));
		//computeAllPairShortestPaths(hm.get("outputfile"));
		//starCluster(hm.get("phrases"), Double.parseDouble(hm.get("sim_thresh")), hm.get("output"), Integer.parseInt(hm.get("count")), hm.get("wikipedia"), hm.get("new-graph"), hm.get("entities"), hm.get("phraseEmbeddings"), Double.parseDouble(hm.get("lambda1")), Double.parseDouble(hm.get("lambda2")));
		//generateQueries();
		//clean();
		//read(hm.get("entity-index"), hm.get("query-to-doc"), hm.get("entity-map"), hm.get("prefix"));
			//init();
	}

}

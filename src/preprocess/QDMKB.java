package preprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import aspect.AdjListCompact;
import aspect.Aspect;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import java.util.*;

public class QDMKB 
{
	AdjListCompact tkb;
	public QDMKB(String tkb, String nodemap, String relmap) throws Exception
	{
		 Aspect a11 = new Aspect();
	 	  AdjListCompact ac = a11.readGraphEfficientAlternate(tkb, relmap, nodemap);
	 	  System.out.println("read knowledge graph");
	 	  this.tkb = ac;
	 	  ac.getListOfNeighborsForEdgeLabel(new HashSet<Integer>(ac.nodeMap.keySet()));
	 	  
	}
	
	public HashMap<String, ArrayList<String>> getSecondHopProperties(String query, HashMap<String, ArrayList<String>> props1)
	{
		HashMap<String, ArrayList<String>> props2 = new HashMap<String, ArrayList<String>>();
		for(String r:props1.keySet())
		{
			ArrayList<String> list1 = props1.get(r);
			HashMap<String, ArrayList<String>> props2_1 = new HashMap<String, ArrayList<String>>();
			HashMap<String, ArrayList<String>> e_rel = new HashMap<String, ArrayList<String>>();
			for(String nn:list1)
			{
				Integer node = tkb.nodeMap1.get(nn);
				Set<Integer> neigh = tkb.labeledGraph.adjacentNodes(node);
				
				for(Integer n:neigh)
				{
					String nodename = tkb.nodeMap.get(n);
					if(e_rel.get(nodename)==null)
					{
						ArrayList<String> e_rel_list = new ArrayList<String>();
						e_rel_list.add(nn);
						e_rel.put(nodename, e_rel_list);
					}
					else
					{
						ArrayList<String> e_rel_list = e_rel.get(nodename);
						e_rel_list.add(nn);
						e_rel.put(nodename, e_rel_list);
					}
					Optional c1 =  tkb.labeledGraph.edgeValue(node, n);
					ArrayList<Integer> c = new ArrayList<Integer>();
					if(c1.isPresent())
					{
						c = (ArrayList<Integer>) c1.get();
						for(Integer cc:c)
						{
							String relname = tkb.relmap1.get(cc);
							
							//if(relname.equals("typeof")) continue;
							long l = (((long)node.intValue()) << 32) | (cc.intValue() & 0xffffffffL);
							if(tkb.pairAdjList.get(l)==null) continue;
							if(props2_1.get(relname)!=null)
							{
								ArrayList<String> props = props2_1.get(relname);
								props.add(nodename);
								props2_1.put(relname, props);
							}
							else
							{
								ArrayList<String> props = new ArrayList<String>();
								props.add(nodename);
								props2_1.put(relname, props);
							}
							//Set<Integer> s = tkb.pairAdjList.get(l);
							
						}
					}
				}
			}
			
			for(String pp:props2_1.keySet())
			{
				ArrayList<String> pp_list = props2_1.get(pp);
				HashSet<String> h_list = new HashSet<String>(pp_list);
				double diversity = h_list.size()/pp_list.size();
				if(diversity>=0.5)
				{
					String new_r = pp+" of ";
					for(String ee:pp_list)
					{
						ArrayList<String> e_rel_list = e_rel.get(ee);
						for(String eee:e_rel_list)
						{
							String new_r1 = new_r + " "+eee;
							if(props2.get(new_r1)==null)
							{
								ArrayList<String> final_list = new ArrayList<String>();	
								final_list.add(ee);
								props2.put(new_r1, final_list);
							}
							else
							{
								ArrayList<String> final_list = props2.get(new_r1);	
								final_list.add(ee);
								props2.put(new_r1, final_list);
							}
						}
					}
					
				}
				else
				{
					String new_r = "all "+pp+" of "+r;
					if(props2.get(new_r)==null)
					{
						props2.put(new_r, new ArrayList<String>(h_list));
					}
					else
					{
						HashSet<String> list11 = new HashSet<String>(props2.get(new_r));
						list11.addAll(h_list);
						props2.put(new_r, new ArrayList<String>(list11));
						
					}
					
				}
			}
		}
		return props2;
	}
	
	
	public void getFirstHopProperties(String query, String outfile) throws Exception
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile+"/"+query));
		if (tkb.nodeMap1.get(query)==null) 
		{
			System.out.println("query does not exists in kb");
			return;
		}
		HashMap<String, ArrayList<String>> props1 = new HashMap<String, ArrayList<String>>();
		Integer node = tkb.nodeMap1.get(query);
		
		Integer type_node=0;
		if(tkb.nodeMap1.get("typeof")!=null)
		{
			type_node = tkb.nodeMap1.get("typeof");
		}
		long l1 = (((long)node.intValue()) << 32) | (type_node.intValue() & 0xffffffffL);
		
		if(tkb.pairAdjList.get(l1)!=null) 
		{
			Set<Integer> s = tkb.pairAdjList.get(l1);
			for(int n1:s)
			{
				Set<Integer> neigh = tkb.labeledGraph.adjacentNodes(n1);
				for(Integer n:neigh)
				{
					String nodename = tkb.nodeMap.get(n);
					Optional c1 =  tkb.labeledGraph.edgeValue(node, n);
					ArrayList<Integer> c = new ArrayList<Integer>();
					if(c1.isPresent())
					{
						c = (ArrayList<Integer>) c1.get();
						for(Integer cc:c)
						{
							String relname = tkb.relmap1.get(cc);
							
							if(relname.equals("typeof")) continue;
							long l = (((long)node.intValue()) << 32) | (cc.intValue() & 0xffffffffL);
							if(tkb.pairAdjList.get(l)==null) continue;
							if(props1.get(relname)!=null)
							{
								ArrayList<String> props = props1.get(relname);
								HashSet<String> propsSet = new HashSet<String>(props);
								propsSet.add(nodename);
								//props.add(nodename);
								props1.put(relname, new ArrayList<String>(propsSet));
							}
							else
							{
								ArrayList<String> props = new ArrayList<String>();
								props.add(nodename);
								props1.put(relname, props);
							}
							//Set<Integer> s = tkb.pairAdjList.get(l);
							
						}
					}
				}
			}
		}
		Set<Integer> neigh = tkb.labeledGraph.adjacentNodes(node);
		for(int n:neigh)
		{
			String nodename = tkb.nodeMap.get(n);
			Optional c1 =  tkb.labeledGraph.edgeValue(node, n);
			ArrayList<Integer> c = new ArrayList<Integer>();
			if(c1.isPresent())
			{
				c = (ArrayList<Integer>) c1.get();
				for(Integer cc:c)
				{
					String relname = tkb.relmap1.get(cc);
					if(relname.equals("typeof")) continue;
					long l = (((long)node.intValue()) << 32) | (cc.intValue() & 0xffffffffL);
					if(tkb.pairAdjList.get(l)==null) continue;
					if(props1.get(relname)!=null)
					{
						ArrayList<String> props = props1.get(relname);
						HashSet<String> propsSet = new HashSet<String>(props);
						propsSet.add(nodename);
						//props.add(nodename);
						props1.put(relname, new ArrayList<String>(propsSet));
					}
					else
					{
						ArrayList<String> props = new ArrayList<String>();
						props.add(nodename);
						props1.put(relname, props);
					}
					
				}
			}
		}
		
		HashMap<String, ArrayList<String>> props2 = this.getSecondHopProperties(query, props1);
		
		for(String s:props2.keySet())
		{
			props2.get(s).remove(query);
			HashSet<String> list2 = new HashSet<String>(props2.get(s));
			if(list2.size()<=1) continue;
			
			bw.write(s+":\t");
			for(String ss:list2)
			{
				bw.write(ss+"\t");
			}
			bw.write("\n");
		}

		for(String s:props1.keySet())
		{
			props1.get(s).remove(query);
			HashSet<String> list2 = new HashSet<String>(props1.get(s));
			if(list2.size()<=1) continue;
			
			bw.write(s+":\t");
			for(String ss:list2)
			{
				bw.write(ss+"\t");
			}
			bw.write("\n");
		}
		bw.close();
		
		
	}
	
	
}

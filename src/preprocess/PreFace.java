package preprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;

import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import LMForTeKnowbase.LanguageModelEntity;
import aspect.AdjListCompact;
import aspect.Aspect;
import main.java.org.ahocorasick.trie.Emit;
import main.java.org.ahocorasick.trie.Trie;
import preprocess.AdjList;
import preprocess.ReadSubgraph;

public class PreFace 
{
	public static void computePhraseBOE(String queries, String teknowbase_mapped, String nodemap, String relmap, String phraseFolder, String outputFolder) throws Exception
	{
	    Aspect a11 = new Aspect();
	    AdjListCompact ac = a11.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);

	    BufferedReader br1 = new BufferedReader(new FileReader(queries));
	    String line1;
	    Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
 	   	for(String entity : ac.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
    
	    while((line1=br1.readLine())!=null)
	    {
	    	String phrasefile = phraseFolder+"/"+line1;
	    	BufferedReader br = new BufferedReader(new FileReader(phrasefile));
	 	   String line;
	 	   System.out.println(line1);
	 	   
	        HashSet<String> allEntities = new HashSet<String>();
	        HashMap<String, HashSet<String>> hh = new HashMap<String, HashSet<String>>();
	 	    while((line=br.readLine())!=null)
	 		{
	 			String[] arr = line.split("', ");
	 			
	 			String phrase = arr[0].substring(3);
	 			
	 			Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
	 			HashSet<String> entities = new HashSet<String>();
	 			Set<String> e1 = new HashSet<String>();
	 			
	 			for(Emit e:named_entity_occurences)
	 			{
	 				if(!ac.undirectedGraph.nodes().contains(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")))) continue;

	 				entities.add(e.getKeyword().replace(" ", "_"));
	 				allEntities.add(e.getKeyword().replace(" ", "_"));
	 				
	 				Set<Integer> neigh = ac.undirectedGraph.adjacentNodes(ac.nodeMap1.get(e.getKeyword().replace(" ", "_")));
	 				for(int n:neigh)
	 				{
	 					entities.add(ac.nodeMap.get(n));
	 					allEntities.add(ac.nodeMap.get(n));
	 				}
	 			}
	 			if(entities.size()==0) continue;
	 			hh.put(phrase, entities);
	 		}
	 	    ArrayList<String> allEntityList = new ArrayList<String>(allEntities);
	 	    System.out.println(allEntityList.size());
	 	    
	 	    ArrayList<Integer> vector = new ArrayList<Integer>();
	 	    HashMap<String, Integer> indexedEntities = new HashMap<String, Integer>();
	 	    for(int i=0;i<allEntityList.size();i++)
	 	    {
	 	    	indexedEntities.put(allEntityList.get(i), i);
	 	    	vector.add(0);
	 	    }
	 	    System.out.println(vector.size());

	 	    
	 	    BufferedWriter bw = new BufferedWriter(new FileWriter(outputFolder+"/"+line1));
	 	    bw.write(hh.size()+"\t"+allEntityList.size()+"\n");
	 	    for(String e:hh.keySet())
	 	    {
	 	    	ArrayList<Integer> vector1 = new ArrayList<Integer>(vector);
	 	    	System.out.println(vector1.size());
	 	    	for(String en:hh.get(e))
	 	    	{
	 	    		int index = indexedEntities.get(en);
	 	    		vector1.set(index, 1);
	 	    	}
	 	    	bw.write(e.replace(" ", "_"));
	 	    	for(int i:vector1)
	 	    	{
	 	    		bw.write(" "+i);
	 	    	}
	 	    	bw.write("\n");
	 	    }
	 	   bw.close();
	 	   
	    }
	    
	}
	
	public static void computeRefDScores(String queries, String wikipedia) throws Exception
	{
			ReadSubgraph rr = new ReadSubgraph();
			AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
		    		    
		    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		    Date  date = new Date();
		    System.out.println("read wikipedia "+dateFormat.format(date));
		    
		    BufferedReader br = new BufferedReader(new FileReader(queries));
		    String line;
		    while((line=br.readLine())!=null)
		    {
		    	System.out.println(line);
			    String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_entities/"+line;
		    	BufferedReader br1 = new BufferedReader(new FileReader(phrasefile));
		 	    String line1;
			    HashMap<String, Double> h = new HashMap<String, Double>();
			    
			    while((line1=br1.readLine())!=null)
			    {
			    	String[] arr = line1.split("\t");
			    	if(arr[0].length()<5) continue;
			    	h.put(arr[0],0.0);
			    }
			    
			    HashMap<String, Double> refdScores = new HashMap<String, Double>();
			    for(String s:h.keySet())
			    {
			    	double refd = a.referenceDistance(line, s);
			    	//if(refd>0.02)
			    		refdScores.put(s, refd);
			    }
			    ArrayList<Entry<String, Double>> refdList = new ArrayList<Entry<String, Double>>(refdScores.entrySet());
			    Collections.sort(refdList, LanguageModelEntity.valueComparator);
			    BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/refdScores/"+line));
			    for(Entry e:refdList)
			    {
			    	bw.write(e.getKey()+"\t"+e.getValue()+"\n");
			    }
			    bw.close();
		    }
	}
	
	public static void assignSubtopicScore(String queries) throws Exception
	{
			BufferedReader br = new BufferedReader(new FileReader(queries));
		    String line;
		    while((line=br.readLine())!=null)
		    {
		    	HashMap<String, Double> pair_scores = new HashMap<String, Double>();
		    	BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/pairwise_scores/"+line));
		    	String line1;
		    	double tot=0.0;
		    	while((line1=br1.readLine())!=null)
		    	{
		    		String[] arr = line1.split("\t");
			    	tot = tot+Double.parseDouble(arr[1]);
			    	pair_scores.put(arr[0], Double.parseDouble(arr[1]));
		    	}
		    	for(String p:pair_scores.keySet())
		    	{
		    		pair_scores.put(p, pair_scores.get(p)/tot);
		    	}
		    	
		    	HashMap<String, Double> refd_scores = new HashMap<String, Double>();
		    	BufferedReader br2 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/refdScores/"+line));
		    	String line2;
		    	double tot2=0.0;
		    	while((line2=br2.readLine())!=null)
		    	{
		    		String[] arr = line2.split("\t");
		    		if(pair_scores.get(arr[0])!=null)
		    		{
			    	//tot = tot+Double.parseDouble(arr[1]);
		    			refd_scores.put(arr[0], Double.parseDouble(arr[1])*pair_scores.get(arr[0]));
		    		}
		    	}
		    	ArrayList<Entry<String, Double>> scores_sorted = new ArrayList<Entry<String, Double>>(refd_scores.entrySet());
		    	Collections.sort(scores_sorted, LanguageModelEntity.valueComparator);
		    	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopic_scores/"+line));
		    	
		    	for(Entry e:scores_sorted)
		    	{
		    		bw.write(e.getKey()+"\t"+e.getValue()+"\n");
		    	}
		    		
		    	bw.close();
		    	
		    }
	}
	
	public static void starClusterEntities(String queries, String wikipedia) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/latest/mappings_edges";
	    
	    Aspect a11 = new Aspect();
	    HashSet<String> missing_entities = new HashSet<String>();
	    
 	    AdjListCompact ac = a11.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
 	    
 	   ReadSubgraph rr = new ReadSubgraph();
		//AdjList a = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
 	    
 	    MutableValueGraph<String, Integer> weightedGraph = ValueGraphBuilder.directed().allowsSelfLoops(true).build();
 	    
 	    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
 	    Date  date = new Date();
		System.out.println("read teknowbase "+dateFormat.format(date));
 	    
 	    BufferedReader br = new BufferedReader(new FileReader(queries));
 	    String line;
 	    while((line=br.readLine())!=null)
 	    {
 	    	System.out.println(line);
 	    	String phrasefile = "/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_entities/"+line;
	    	BufferedReader br1 = new BufferedReader(new FileReader(phrasefile));
	 	    String line1;
	 	    HashMap<String, Double> h = new HashMap<String, Double>();
 	    
	 	    while((line1=br1.readLine())!=null)
	 	    {
	 	    	String[] arr = line1.split("\t");
	 	    	if(arr[0].length()<5) continue;
	 	    	h.put(arr[0],0.0);
	 	    }
	 	    
	 	    HashMap<String, Double> refdScores = new HashMap<String, Double>();
	    	br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/tagged_entities/query_wise_scores/refdScores/"+line));
	    	 
	    	while((line1=br1.readLine())!=null)
	 	    {
	 	    	String[] arr = line1.split("\t");
	 	    	if(arr[0].length()<5) continue;
	 	    	refdScores.put(arr[0],Double.parseDouble(arr[1]));
	 	    }
	 	    
	 	    
	 	    HashMap<String, Double> subtopicScores = new HashMap<String, Double>();
	 	    
	 	    br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopic_scores/"+line));
	 	    while((line1=br1.readLine())!=null)
	 	    {
	 	    	String[] arr = line1.split("\t");
	 	    	subtopicScores.put(arr[0],Double.parseDouble(arr[1]));
	 	    }
	 	    
	 	    for(String n:refdScores.keySet())
	 	    {
	 	    	if(ac.nodeMap1.get(n)==null) continue;
	 	    	int e_code = ac.nodeMap1.get(n);
	 	    	if(!ac.labeledGraph.nodes().contains(e_code)) continue;
	 	    	for(int neigh:ac.labeledGraph.adjacentNodes(e_code))
	 	    	{
	 	    		String neigh_string = ac.nodeMap.get(neigh);
	 	    		Optional c1 = ac.labeledGraph.edgeValue(e_code, neigh);
	 	    		if(h.get(neigh_string)!=null && refdScores.get(neigh_string)!=null)
	 	    		{
	 	    			if(c1.isPresent())
	 	    			{
	 	    				ArrayList<Integer> c = (ArrayList<Integer>) c1.get();
	 	    				for(int cc:(ArrayList<Integer>) c)
	 	    				{
	 	    					if(ac.relmap1.get(cc)!=null)
	 	    					{
	 	    						String rel = ac.relmap1.get(cc);
	 	    						if(rel.contains("_inverse"))
	 	    						{
	 	    							continue;
	 	    						}
	 	    						else
	 	    						{
	 	    							weightedGraph.putEdgeValue(n, neigh_string, 1);
	 	    						}
	 	    					}
	 	    				}
	 	    			}
	 	    		}
	 	    		
	 	    	}
	 	    	
	 	    }
	 	    HashMap<String, Double> h1 = new HashMap<String, Double>();
	 	    for(String n:weightedGraph.nodes())
	 	    {
	 	    	h1.put(n, (double) weightedGraph.adjacentNodes(n).size());
	 	    }
	 	   ArrayList<Entry<String, Double>> en = new ArrayList<Entry<String, Double>>(h1.entrySet());
		   Collections.sort(en, LanguageModelEntity.valueComparator);
		  
	 	    
	 	   ArrayList<ArrayList<String>> clusters = new ArrayList<ArrayList<String>>();
			HashSet<String> marked = new HashSet<String>();
			for(Entry<String, Double> e:en)
			{
				String a1 = e.getKey();
				int a1_code = ac.nodeMap1.get(a1);
				if(marked.contains(a1)) continue;
				marked.add(a1);
				ArrayList<String> list1 = new ArrayList<String>();
				list1.add(a1);
				if(!ac.labeledGraph.nodes().contains(a1_code)) continue;
				if(!weightedGraph.nodes().contains(a1)) continue;
					for(String adj:weightedGraph.adjacentNodes(a1))
					{
						
						if(marked.contains(adj)) continue;
						list1.add(adj);
						marked.add(adj);
					}
					clusters.add(list1);
				
			}
			
			HashMap<ArrayList<String>, Double> clusters_sorted = new HashMap<ArrayList<String>, Double>();
			
			for(ArrayList<String> c:clusters)
			{
				double sum=0.0;
				for(String s:c)
				{
					if(h.get(s)==null) continue;
					sum = sum + h.get(s);
				}
				clusters_sorted.put(c, sum);
			}
			
			HashMap<ArrayList<String>, Double> refd_sorted = new HashMap<ArrayList<String>, Double>();
			HashMap<String, Double> refdscores = new HashMap<String, Double>();
			
			HashMap<ArrayList<String>, Double> both_sorted = new HashMap<ArrayList<String>, Double>();
			
			/*for(ArrayList<String> c:clusters)
			{
				double sum=0.0;
				double sum1=0.0;
				for(String s:c)
				{
					if(h.get(s)==null) continue;
					double refd = a.referenceDistance(line, s);
					refdscores.put(s, refd);
					if(refd>0)
						sum = sum + refd;
					sum1 = sum1 + h.get(s);
				}
				refd_sorted.put(c, sum);
				both_sorted.put(c, sum*sum1);
			}*/
			
			HashMap<ArrayList<String>, Double> subtopic_sorted = new HashMap<ArrayList<String>, Double>();
			
			for(ArrayList<String> c:clusters)
			{
				double sum=0.0;
				double sum1=0.0;
				for(String s:c)
				{
					if(subtopicScores.get(s)==null) continue;
					double sscore = subtopicScores.get(s);
					sum = sum + sscore;
					sum1 = sum1 + h.get(s);
				}
				subtopic_sorted.put(c, 1.0*c.size());
			}
			
			ArrayList<Entry<ArrayList<String>, Double>> ec = new ArrayList<Entry<ArrayList<String>, Double>>(clusters_sorted.entrySet());
			Collections.sort(ec, LanguageModelEntity.valueComparatorList);
			
			ArrayList<Entry<ArrayList<String>, Double>> ec_refd = new ArrayList<Entry<ArrayList<String>, Double>>(refd_sorted.entrySet());
			Collections.sort(ec_refd, LanguageModelEntity.valueComparatorList);
			
			ArrayList<Entry<ArrayList<String>, Double>> ec_both = new ArrayList<Entry<ArrayList<String>, Double>>(both_sorted.entrySet());
			Collections.sort(ec_both, LanguageModelEntity.valueComparatorList);
			
			ArrayList<Entry<ArrayList<String>, Double>> ec_subtopic = new ArrayList<Entry<ArrayList<String>, Double>>(subtopic_sorted.entrySet());
			Collections.sort(ec_subtopic, LanguageModelEntity.valueComparatorList);
			
			BufferedWriter bw1 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_unordered"));
	
			BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line));
			
			BufferedWriter bw2 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_refd_ordered"));
	
			BufferedWriter bw3 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_both_ordered"));
			
			BufferedWriter bw4 = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/star_cluster_entities/only_refd/"+line+"_subtopics_ordered"));
	
			for(Entry<ArrayList<String>, Double> e:ec)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(h.get(cc)==null) continue;
					ent_ordered.put(cc,h.get(cc));
				}
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw.write(c_ordered+"\t"+e.getValue()+"\n");
				bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			for(Entry<ArrayList<String>, Double> e:ec_refd)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(refdScores.get(cc)==null) continue;
					ent_ordered.put(cc,refdscores.get(cc));
				}
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw2.write(c_ordered+"\t"+e.getValue()+"\n");
				//bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			for(Entry<ArrayList<String>, Double> e:ec_both)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(refdScores.get(cc)==null) continue;
					ent_ordered.put(cc,refdscores.get(cc));
				}
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw3.write(c_ordered+"\t"+e.getValue()+"\n");
				//bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			for(Entry<ArrayList<String>, Double> e:ec_subtopic)
			{
				HashMap<String, Double> ent_ordered = new HashMap<String, Double>();
				ArrayList<String> c = e.getKey();
				for(String cc:c)
				{
					if(subtopicScores.get(cc)==null) continue;
					ent_ordered.put(cc,subtopicScores.get(cc));
				}
				
				ArrayList<Entry<String, Double>> c_ordered = new ArrayList<Entry<String, Double>>(ent_ordered.entrySet());
				Collections.sort(c_ordered, LanguageModelEntity.valueComparator);
				bw4.write(c_ordered+"\t"+e.getValue()+"\n");
				//bw1.write(c+"\t"+e.getValue()+"\n");
			}
			
			bw.close();
			bw1.close();
			bw2.close();
			bw3.close();
			bw4.close();
 	    }
 	    
	}
	

}

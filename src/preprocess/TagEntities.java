package preprocess;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import main.java.org.ahocorasick.trie.Emit;
import main.java.org.ahocorasick.trie.Trie;
import tomcat.GetPropertyValues;

/**
 * class to preprocess all the tutorial files and create a graph of related concepts among them
 * @author root
 *
 */

public class TagEntities 
{
	public static HashMap<String, ArrayList<String>> getAddress(String file) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		HashMap<String, ArrayList<String>> address = new HashMap<String, ArrayList<String>>();
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,",");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			File f1 = new File("/home/prajna/workspace/TutorialBank/data/url/"+tokens.get(2)+".txt");
			File f2 = new File("/home/prajna/workspace/TutorialBank/data/files_text/"+tokens.get(2)+".txt");
			
			//System.out.println(f1.getAbsolutePath());
			//System.out.println(f2.getAbsolutePath());
			
			
			
			if(f1.exists())
			{
				if(address.get(tokens.get(2))==null)
				{
					ArrayList<String> addresses = new ArrayList<String>();
					addresses.add(f1.getAbsolutePath());
					address.put(tokens.get(2), addresses);
				}
				else
				{
					ArrayList<String> addresses = address.get(tokens.get(2));
					addresses.add(f1.getAbsolutePath());
					address.put(tokens.get(2), addresses);
				}
			}
			else if(f2.exists())
			{
				if(address.get(tokens.get(2))==null)
				{
					ArrayList<String> addresses = new ArrayList<String>();
					addresses.add(f2.getAbsolutePath());
					address.put(tokens.get(2), addresses);
				}
				else
				{
					ArrayList<String> addresses = address.get(tokens.get(2));
					addresses.add(f2.getAbsolutePath());
					address.put(tokens.get(2), addresses);
				}
			}
			else
			{
				System.out.println("file does not exist");
				continue;
			}
		}
		return address;
	}
	public static void readFile(Trie trie, String file, String outfile) throws Exception
	{
		HashMap<String, HashSet<String>> graph = new HashMap<String, HashSet<String>>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,",");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			File f1 = new File("/home/pearl/workspace/TutorialBank/data/url/"+tokens.get(2)+".txt");
			File f2 = new File("/home/pearl/workspace/TutorialBank/data/files_text/"+tokens.get(2)+".txt");
			
			//System.out.println(f1.getAbsolutePath());
			//System.out.println(f2.getAbsolutePath());
			
			HashSet<String> neighbors = new HashSet<String>();
			
			if(f1.exists())
			{
				neighbors = tagEntities(f1.getAbsolutePath(), trie);
			}
			else if(f2.exists())
			{
				neighbors=tagEntities(f2.getAbsolutePath(), trie);
			}
			else
			{
				System.out.println("file does not exist");
				continue;
			}
			if(graph.get(tokens.get(1))==null)
			{
				graph.put(tokens.get(1), neighbors);
			}
			else
			{
				HashSet<String> ne = graph.get(tokens.get(1));
				ne.addAll(neighbors);
				graph.put(tokens.get(1), ne);
			}
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		for(String s:graph.keySet())
		{
			for(String ss:graph.get(s))
			{
				bw.write(s+"\t"+ss.replace(" ", "_")+"\n");
			}
		}
		bw.close();
	}
	
	public static HashSet<String> tagEntities(String file, Trie trie) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		String text="";
		while((line=br.readLine())!=null)
		{
                text = text + line.toLowerCase().replaceAll("[(),.!?;:]", " $0 ").replaceAll("[^A-Za-z0-9. ]+", " $0 ")+" ";
		}
		
		Collection<Emit> named_entity_occurences = trie.parseText(text);
		HashMap<String, Double> tagged_entities = new HashMap<String, Double>();
		
		for(Emit e:named_entity_occurences)
		{
			if(tagged_entities.get(e.getKeyword())!=null)
			{
				tagged_entities.put(e.getKeyword(), tagged_entities.get(e.getKeyword())+1.0);
			}
			else
			{
				tagged_entities.put(e.getKeyword(), 1.0);
			}
			//System.out.println(e.getKeyword());
		}
		
		ArrayList<Entry<String, Double>> al = new ArrayList<Entry<String, Double>>(tagged_entities.entrySet());
		Collections.sort(al, LMForTeKnowbase.LanguageModelEntity.valueComparator);
		HashSet<String> neighbors = new HashSet<String>();
		for(Entry<String, Double> e:al)
		{
			neighbors.add(e.getKey());
		}
		return neighbors;
	}
	
	public static void writeDescriptions(String infile, String outfile, AdjList a, Trie trie) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(infile));
		String line;
		HashMap<String, String> descriptions = new HashMap<String, String>();
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			int c=0;
			String entity="";
			String desc = "";
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken().toLowerCase());
			}
			if(tokens.size()!=2) continue;
			if(a.getAdjList().get(tokens.get(0).replace(" ", "_"))!=null)
			{
				Collection<Emit> named_entity_occurences = trie.parseText(tokens.get(1).toLowerCase());
				String text = tokens.get(1).toLowerCase();
				HashSet<String> hh = new HashSet<String>();
				for(Emit e:named_entity_occurences)
				{
					hh.add(e.getKeyword());
					text = text.replace(e.getKeyword(), e.getKeyword().replace(" ", "_"));
				}
				
				descriptions.put(tokens.get(0).replace(" ", "_"), text);
			}
			
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
		for(String d:descriptions.keySet())
		{
			bw.write(d+"\t"+descriptions.get(d)+"\n");
		}
		bw.close();
	}
	
	
	public static HashMap<String, String> readFile1(String file) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		HashMap<String, String> descriptions = new HashMap<String, String>();
		
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			int c=0;
			String entity="";
			String desc = "";
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken().toLowerCase());
			}
			if(tokens.size()!=2) continue;
			
			if(descriptions.get(tokens.get(0).replace(" ", "_"))!=null)
			{
				descriptions.put(tokens.get(0).replace(" ", "_"), descriptions.get(tokens.get(0).replace(" ", "_"))+" "+tokens.get(1));
			}
			else
			{
				descriptions.put(tokens.get(0).replace(" ", "_"),tokens.get(1));
			}
		}
		return descriptions;
		//HashMap<String, Double> scores = computeScore(trie, descriptions, "");
	}
	
	public static HashMap<String, Double> computeScore(HashMap<String, Integer> a, String alllines, String query, HashMap<String, HashSet<String>> support_sentences, Trie trie) throws Exception
	{
		//String alllines = file_text.get(query);
		SentenceBoundaryDemo s1 = new SentenceBoundaryDemo();
		alllines = alllines.replaceAll("[(),.!?;:]", " $0 ");
		ArrayList<String> sentences = s1.splitSentences1(alllines);
		//StringTokenizer tok = new StringTokenizer(alllines,".");
		Collection<Emit> named_entity_occurences = trie.parseText(alllines.toLowerCase());

		HashMap<String, Double> cooccurence_count = new HashMap<String, Double>();
		for(String text:sentences)
		{
			//text = text.replaceAll("[(),.!?;:]", " $0 ");
			//System.out.println("sentence is: "+text);
			if(text.contains(query.replace("_", " ")))
			{
				for(Emit e:named_entity_occurences)
				{
					String ss = e.getKeyword();
					if(ss.equals(query.replace("_", " "))) continue;
					if(text.contains(ss))
					{
						String ss1 = ss.replace(" ", "_");
						
						if(support_sentences.get(ss1)!=null)
						{
							HashSet<String> sent = support_sentences.get(ss1);
							sent.add(text);
							support_sentences.put(ss1, sent);
						}
						else
						{
							HashSet<String> sent = new HashSet<String>();
							sent.add(text);
							support_sentences.put(ss1, sent);
						}
						if(cooccurence_count.get(ss1)!=null)
	            		{
	            			cooccurence_count.put(ss1, (double) support_sentences.get(ss1).size());
	            		}
	            		else
	            		{
	            			cooccurence_count.put(ss1,1.0);
	            		}
					}
				}
			}
			/*if(text.contains(query))
			{
				StringTokenizer tok = new StringTokenizer(text," ");
				while(tok.hasMoreTokens())
				{
					String ss = tok.nextToken();
					if(a.get(ss)!=null)
					{
						if(cooccurence_count.get(ss)!=null)
	            		{
	            			cooccurence_count.put(ss, cooccurence_count.get(ss)+1);
	            		}
	            		else
	            		{
	            			cooccurence_count.put(ss,1.0);
	            		}
	            		support_sentences.put(ss, text);
					}
				}
						
			}*/
		}
		
		return cooccurence_count;
		
	}
	
	public static void main(String args[]) throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		ReadSubgraph rr = new ReadSubgraph();
		AdjList a = rr.readKB(new BufferedReader(new FileReader(hm.get("teknowbase"))));
		
		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(String entity : a.getAdjList().keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        
        //readFile(trie, hm.get("infile"), hm.get("outfile"));
		writeDescriptions(hm.get("infile"),hm.get("outfile"),a,trie);
	}
	public HashMap<String, Double> computeScore1(File ff) throws Exception
	{
		HashMap<String, Double> co_occ = new HashMap<String, Double>();
		BufferedReader br = new BufferedReader(new FileReader(ff.getAbsolutePath()));
		String line;
		while((line=br.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
			}
			if(tokens.size()!=2) continue;
			co_occ.put(tokens.get(0), Double.parseDouble(tokens.get(1)));
		}
		return co_occ;
	}
}

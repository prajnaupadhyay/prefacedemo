package preprocess;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.google.common.graph.ImmutableValueGraph;

public class TaskShortestPath implements Runnable
{
	int n1;
	ImmutableValueGraph<Integer, Double> graph;
	
	
	public TaskShortestPath(int n1, ImmutableValueGraph graph) throws Exception
	{
		this.n1=n1;
		this.graph=graph;
	}
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		HashMap<Integer, Integer> s_paths = new HashMap<Integer, Integer>();
		
		
		ArrayList<Integer> queue = new ArrayList<Integer>();
		HashSet<Integer> marked = new HashSet<Integer>();
		marked.add(n1);
		queue.add(n1);
		int count=0;
		while(queue.size()>0)
		{
			int front = queue.get(0);
			//System.out.println(front);
			queue.remove(0);
			Set<Integer> neigh = this.graph.adjacentNodes(front);
			count++;
			for(int nn:neigh)
			{
				if(!marked.contains(nn)) 
				{
					queue.add(nn);
					marked.add(nn);
					s_paths.put(nn, count);
				}
			}
		}
		try 
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/shortest_paths/"+n1));
			for(int nn:s_paths.keySet())
			{
				bw.write(nn+"\t"+s_paths.get(nn)+"\n");
			}
			bw.close();
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}

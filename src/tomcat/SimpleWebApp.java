package tomcat;

import java.io.*;
import java.net.URI;
import java.net.http.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aliasi.cluster.CompleteLinkClusterer;
import com.aliasi.cluster.Dendrogram;
import com.aliasi.cluster.HierarchicalClusterer;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.*;

import LMForTeKnowbase.LanguageModelEntity;
import LMForTeKnowbase.UtilityFunctions;
import aspect.AdjListCompact;
import aspect.Aspect;
import buildAndProcessTrees.BuildTree;
import cc.mallet.types.NormalizedDotProductMetric;
import cc.mallet.types.SparseVector;
import cc.mallet.util.Maths;
import javatools.parsers.PlingStemmer;
import main.java.org.ahocorasick.trie.Emit;
import main.java.org.ahocorasick.trie.Trie;
import preprocess.AdjList;
import preprocess.Edge;
import preprocess.Preprocess;
import preprocess.ReadSubgraph;
import preprocess.TagEntities;


import io.github.crew102.rapidrake.RakeAlgorithm;
import io.github.crew102.rapidrake.data.SmartWords;
import io.github.crew102.rapidrake.model.RakeParams;
import io.github.crew102.rapidrake.model.Result;

 
public class SimpleWebApp 
{
	public static HashMap<Integer, HashMap<Integer, Double>> computeProbability(String query, HashMap<String, Double> path_scores, AdjListCompact a1) throws IOException
	{
		HashMap<Integer, HashMap<Integer, Double>> prob_list = new HashMap<Integer,HashMap<Integer, Double>>();
				  
		 for(String p:path_scores.keySet()) 
		 { //System.out.println(p); 
			  StringTokenizer  tok = new StringTokenizer(p," "); 
			  ArrayList<String> tokens = new ArrayList<String>();
			  while(tok.hasMoreTokens())
			  {
				  tokens.add(tok.nextToken());
			  }
			  int ccc=0;
			  String pp="";
			  ArrayList<Integer> path1 = new ArrayList<Integer>();
			  for(String t:tokens)
			  {
				  if(ccc<tokens.size()-1)
				  {
					  if(ccc==0)
						  pp = pp+t;
					  else
						  pp = pp + " "+t;
					  path1.add((a1.relmap.get(t)));
				  }
				  ccc++;
			  }
			  
			  int source = a1.nodeMap1.get(query);
			  a1.pathConstrainedRandomWalk(path1, source, path_scores.get(p), p, prob_list); 
			  
		  } 
		 
		 return prob_list;
	}
	
	public static HashMap<String, Double> readPathScores(String file, AdjListCompact a1) throws Exception
	{
		BufferedReader br1 = new BufferedReader(new FileReader(file));
		  HashMap<Integer, HashMap<Integer, Double>> prob_list = new HashMap<Integer, HashMap<Integer, Double>>();
		  
		  HashSet<Integer> paths = new HashSet<Integer>(); 
		  HashMap<String, Double> path_scores = new HashMap<String, Double>();
		  String line;
		  while((line=br1.readLine())!=null) 
		  { 
			  StringTokenizer tok = new StringTokenizer(line," ");
			  ArrayList<String> tokens = new ArrayList<String>(); 
			  while(tok.hasMoreTokens()) 
			  {
				  tokens.add(tok.nextToken()); 
			  }
		  //System.out.println(tokens.get(0)+" "+tokens.get(1));
		  //System.out.println(tokens.get(1));
			  int ccc=0;
			  String pp="";
			  for(String t:tokens)
			  {
				  if(ccc<tokens.size()-1)
				  {
					  if(ccc==0)
						  pp=pp+t;
					  else
						  pp = pp + " "+t;
					  paths.add((a1.relmap.get(t)));
				  }
				  ccc++;
			  }
			  double score = Double.parseDouble(tokens.get(tokens.size()-1));
			  path_scores.put(pp, score);
		  
		  }
		  System.out.println("read all the meta-paths");
		  a1.getListOfNeighborsForEdgeLabel(new HashSet<Integer>(paths));
		  System.out.println("created index on all relations in meta-paths");
		  return path_scores;
	}
	
	public class Feature
	{
		public String name;
		public double[] values;
		
		public Feature(String name, double[] values)
		{
			this.name=name;
			this.values = values;
		}
	}
	
	public Feature getFeature(String name, double[] values)
	{
		return new Feature(name, values);
	}
	
	public MyEntry<String,Double> getEntry(String name, double values)
	{
		return new MyEntry<String,Double>(name, values);
	}
	
	
	
	public static Set<Set<Feature>> clusterHierarchical(HashSet<Feature> s1, BuildTree b, double mindistance)
	{
		
		if(s1.size()==0) 
		{
			System.out.println("not here");
		}
		HierarchicalClusterer<Feature> clClusterer = new CompleteLinkClusterer<Feature>(UtilityFunctions.COSINE_DISTANCE1);
		Dendrogram<Feature> completeLinkDendrogram = clClusterer.hierarchicalCluster(s1);
		//HashMap<String, Double> grandCentroid = UtilityFunctions.calculateCentroid(s1);
		Set<Set<Feature>> candidates = completeLinkDendrogram.partitionDistance(mindistance);
		for(int c=completeLinkDendrogram.size()-1;c>=2;c--)
		{
			Set<Set<Feature>> ss = completeLinkDendrogram.partitionK(c);		
			if(ss==null) continue;
			
			candidates.addAll(ss);
		}
		return candidates;
	}
	
	public static double[] computeLM(HashMap<Set<String>, Double> s, HashMap<String, Integer> ent_index)
	{
		String final_phrase = "";
		for(Set<String> ss:s.keySet())
		{
			for(String sss:ss)
			{
			final_phrase = final_phrase + " "+sss;
			}
		}
		return computeLM(final_phrase, ent_index);
	}
	
	public static double[] computeLM(String phrase, HashMap<String, Integer> ent_index)
	{
		HashMap<String, Double> probdist = new HashMap<String, Double>();
		String arr[] = phrase.split(" ");
		for(String a:arr)
		{
			if(ent_index.get(a)==null) continue;
			if(probdist.get(a)!=null)
			{
				probdist.put(a, probdist.get(a)+1);
			}
			else
			{
				probdist.put(a, 1.0);
			}
		}
		for(String s:ent_index.keySet())
		{
			if(probdist.get(s)==null)
			{
				probdist.put(s, 1.0);
			}
			else
			{
				probdist.put(s, probdist.get(s)+1);
			}
		}
		double[] lm_dist = new double[probdist.size()];
		for(String s:probdist.keySet())
		{
			lm_dist[ent_index.get(s)]=probdist.get(s)/(2*probdist.size());
		}
		return lm_dist;
	}
	
	public static double[] computeLMSet(Set<String> lm, HashMap<String, Integer> ent_index)
	{
		HashMap<String, Double> probdist = new HashMap<String, Double>();
		for(String ll:lm)
		{
			String arr[] = ll.split(" ");
			for(String a:arr)
			{
				if(ent_index.get(a)==null) continue;
				if(probdist.get(a)!=null)
				{
					probdist.put(a, probdist.get(a)+1);
				}
				else
				{
					probdist.put(a, 1.0);
				}
			}
			
		}
		
		for(String s:ent_index.keySet())
		{
			if(probdist.get(s)==null)
			{
				probdist.put(s, 1.0);
			}
			else
			{
				probdist.put(s, probdist.get(s)+1);
			}
		}
		double[] lm_dist = new double[probdist.size()];
		for(String s:probdist.keySet())
		{
			lm_dist[ent_index.get(s)]=probdist.get(s)/(2*probdist.size());
		}
		return lm_dist;
	}
	
	
	public static double[] computeLM(Set<LanguageModelEntity> lm, HashMap<String, Integer> ent_index)
	{
		HashMap<String, Double> probdist = new HashMap<String, Double>();
		for(LanguageModelEntity ll:lm)
		{
			String arr[] = ll.getName().split("_");
			for(String a:arr)
			{
				if(ent_index.get(a)==null) continue;
				if(probdist.get(a)!=null)
				{
					probdist.put(a, probdist.get(a)+1);
				}
				else
				{
					probdist.put(a, 1.0);
				}
			}
			
		}
		
		for(String s:ent_index.keySet())
		{
			if(probdist.get(s)==null)
			{
				probdist.put(s, 1.0);
			}
			else
			{
				probdist.put(s, probdist.get(s)+1);
			}
		}
		double[] lm_dist = new double[probdist.size()];
		for(String s:probdist.keySet())
		{
			lm_dist[ent_index.get(s)]=probdist.get(s)/(2*probdist.size());
		}
		return lm_dist;
		
	}
	
	public static void parseXMLStackOverflow(String file) throws Exception
	{
		System.out.println("in parse XML");
		HashMap<String, ArrayList<String>> indexes = new HashMap<String, ArrayList<String>>();
		HashMap<String,String> postToTags = new HashMap<String,String>();
		File inputFolder = new File(file);
		for(File f1:inputFolder.listFiles())
		{
			if(f1.isDirectory())
			{
				String inputFile = f1.getAbsolutePath()+"/Posts.xml";
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				
					org.w3c.dom.Document doc = builder.parse(inputFile);
					
					doc.getDocumentElement().normalize();
					System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
					
					NodeList nList = doc.getElementsByTagName("row");
					System.out.println("----------------------------");
					for (int i = 0; i < nList.getLength(); i++) 
					{
						Node e = nList.item(i);
						
						//if(e instanceof org.w3c.dom.Element)
						
							org.w3c.dom.Element e1 = (org.w3c.dom.Element) e;
							//if(childElement.tagName().contains("section"))
							
							String tags="";
							String posttype = e1.getAttribute("PostTypeId");
							if(posttype.equals("1")) 
							{
								tags = e1.getAttribute("Tags");
								String id = e1.getAttribute("Id");
								postToTags.put(id,tags);
								
							}
							
					}
					for (int i = 0; i < nList.getLength(); i++) 
					{
						Node e = nList.item(i);
						
						//if(e instanceof org.w3c.dom.Element)
						
						org.w3c.dom.Element e1 = (org.w3c.dom.Element) e;
						//if(childElement.tagName().contains("section"))
						String content = e1.getAttribute("Title")+"\t"+e1.getAttribute("Body");
						String tags="";
						String posttype = e1.getAttribute("PostTypeId");
						if(posttype.equals("2"))
						{
							String id = e1.getAttribute("ParentId");
							tags = postToTags.get(id);
						}
							
						String[] arr = tags.split("><");
						for(String s:arr)
						{
							s = s.replace(">", "").replace("<", "").replace("-", "_");
							ArrayList<String> s1 = indexes.get(s);
							if(s1==null)
							{
								s1 = new ArrayList<String>();
							}
							s1.add(content);
							indexes.put(s, s1);
						}
					  }
										
			}
		}
		System.out.println(indexes.size());
		for(String s:indexes.keySet())
		{
			System.out.println(s);
			File ff = new File("/mnt/dell/prajna/SOindex/"+s);
			if(ff.isDirectory()) continue;
			BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/prajna/SOindex/"+s));
			for(String ss:indexes.get(s))
			{
				String ss1 = html2text(ss);
				bw.write(ss1+"\n");
			}
			bw.close();
		}
	
		
		//bw.close();
		
	}
	/**
	 * unusable
	 * @param s
	 * @return
	 */
	
/*	public static ArrayList<Entry<String, Double>> orderItems(Set<String> s, HashMap<String, LanguageModelEntity> ss, String aspect)
	{
		HashMap<String, Double> hh = new HashMap<String, Double>();
		if (ss.get(aspect)==null)
		{
			return new ArrayList<Entry<String, Double>>()
		}
		for(String e:s)
		{
			hh.put(e, query.cosineSimilarity(e));
		}
		ArrayList<Entry<LanguageModelEntity, Double>> ee = new ArrayList<Entry<LanguageModelEntity, Double>>(hh.entrySet());
		Collections.sort(ee, LanguageModelEntity.valueComparatorEntity);
		return ee;
	}*/
	
	public static String html2text(String html) {
	    return Jsoup.parse(html).text();
	}
	
	public static HashMap<String, Double> extractAbstractAndTagEntities(String query, int k, Trie trie) throws Exception
	{
		String query_req="http://aryabhata.cse.iitd.ac.in:6003/search?q="+query.replace(" ", "+").replace("-", "+").replace("_", "+")+"&start=1&n="+k;
        String html_string = Jsoup.connect(query_req).get().html();
        Document html = Jsoup.parse(html_string);
        //System.out.println("html is: "+html);
       // String h1 = html.body().getElementsByTag("a").text();
        Elements aherf = html.body().getElementsByTag("a");
        HashMap<String, Double> pairwise_scores = new HashMap<String, Double>();
        for(Element el:aherf)
        {
        String tot_abstract="";
     	  String elem_query="http://aryabhata.cse.iitd.ac.in:6003/"+el.attr("href");
     	  String elem_string = Jsoup.connect(elem_query).get().html();
     	 Document elem_doc = Jsoup.parse(elem_string);
     	 Elements eel = elem_doc.getElementsByTag("paperAbstract");
     	 
     	 for(Element e1:eel)
     	 {
     		 tot_abstract = tot_abstract + " "+e1.text() ;
     	 }
     	 Collection<Emit> named_entity_occurences = trie.parseText(tot_abstract.toLowerCase());
		 for(Emit e:named_entity_occurences)
		 {
			 String ent = e.getKeyword().replace(" ", "_");
			 if(pairwise_scores.get(ent)==null)
			 pairwise_scores.put(ent, 1.0);
			 else
			 {
				 pairwise_scores.put(ent, pairwise_scores.get(ent)+1.0);
			 }
		 }
     	 
     	  // System.out.println("url is: "+elem_query);
     	  //System.out.println("element string: "+elem_string);
     	  // System.out.println(el.attr("href"));
     	  
        }
        System.out.println("extracted abstract");
        //System.out.println("extracted abstract: "+tot_abstract);
        return pairwise_scores;
	}
	
	public static String extractAbstractFromDisk(String query, int k, String port) throws Exception
	{
		String query_req="http://localhost:"+port+"/search?q="+query.replace(" ", "+").replace("-", "+").replace("_", "+")+"&start=1&n="+k;
        String html_string = Jsoup.connect(query_req).get().html();
        System.out.println("connected jsoup");
        Document html = Jsoup.parse(html_string);
        System.out.println("parsed html jsoup");
        //System.out.println("html is: "+html);
       // String h1 = html.body().getElementsByTag("a").text();
        Elements aherf = html.body().getElementsByTag("a");
        String tot_abstract="";
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
	      //an instance of builder to parse the specified xml file  
	     DocumentBuilder db = dbf.newDocumentBuilder();  
	       
        for(Element el:aherf)
        {
	       if( el.attr("href").contains("document?identifier="))
	       {
		        String xml_file = el.attr("href").replace("document?identifier=", "");
		        //System.out.println(el.attr("href"));
		        //System.out.println(xml_file); 
		        org.w3c.dom.Document doc = db.parse(xml_file);
		        
		        doc.getDocumentElement().normalize();    
		       // System.out.println("Root element: " + doc.getDocumentElement().getNodeName());   
		        NodeList nodeList = doc.getElementsByTagName("paperAbstract");		
		        for (int itr = 0; itr < nodeList.getLength(); itr++)   
		        {  
		        	Node node = nodeList.item(itr);  
			     	 tot_abstract = tot_abstract + " "+node.getTextContent();
		        	//System.out.println("\nNode value :" + node.getTextContent());  
			     	  
		        }
		        //SSystem.out.println("extracted abstract");
	       }
	       
        //System.out.println("extracted abstract: "+tot_abstract);
       
	}
        return tot_abstract;
	}
	
	
	
	public static String extractAbstract(String query, int k, String port) throws Exception
	{
		String query_req="http://localhost:"+port+"/search?q="+query.replace(" ", "+").replace("-", "+").replace("_", "+")+"&start=1&n="+k;
        String html_string = Jsoup.connect(query_req).get().html();
        System.out.println("connected jsoup");
        Document html = Jsoup.parse(html_string);
        System.out.println("parsed html jsoup");
        //System.out.println("html is: "+html);
       // String h1 = html.body().getElementsByTag("a").text();
        Elements aherf = html.body().getElementsByTag("a");
        String tot_abstract="";
        for(Element el:aherf)
        {
        
     	  String elem_query="http://localhost:"+port+"/"+el.attr("href");
     	  String elem_string = Jsoup.connect(elem_query).get().html();
     	 Document elem_doc = Jsoup.parse(elem_string);
     	 Elements eel = elem_doc.getElementsByTag("paperAbstract");
     	 
     	 for(Element e1:eel)
     	 {
     		 tot_abstract = tot_abstract + " "+e1.text() ;
     	 }
     	
     	  
        }
        System.out.println("extracted abstract");
        //System.out.println("extracted abstract: "+tot_abstract);
        return tot_abstract;
	}
	
	public static String extractAbstractParallel(String query, int k, String port) throws Exception
	{
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate1 = new Date();
		System.out.println("entered abstract extraction "+dateFormat.format(firstDate1));
		
		String query_req="http://localhost:"+port+"/search?q="+query.replace(" ", "+").replace("-", "+").replace("_", "+")+"&start=1&n="+k;
        String html_string = Jsoup.connect(query_req).get().html();
        Date firstDate2 = new Date();
        System.out.println("connected via Jsoup "+dateFormat.format(firstDate2));
        
        Document html = Jsoup.parse(html_string);
        
        Date firstDate3 = new Date();
        System.out.println("parse html string "+dateFormat.format(firstDate3));
        
        //System.out.println("html is: "+html);
       // String h1 = html.body().getElementsByTag("a").text();
        Elements aherf = html.body().getElementsByTag("a");
        
        Date firstDate4 = new Date();
        System.out.println("retrieved ahref tags "+dateFormat.format(firstDate4));
        
        HashSet<String> tagged_entities = new HashSet<String>();
        String tot_abstract="";
        ConcurrentHashMap<Element,String> abstractlist = new ConcurrentHashMap<Element,String>();
        int count=aherf.size();
		 int num_threads = (Runtime.getRuntime().availableProcessors()+1)/2;

	        ThreadPoolExecutor executor = new ThreadPoolExecutor(num_threads, count, Long.MAX_VALUE, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(count));

	        //ConcurrentHashMap<Set<String>, Double> scores = new ConcurrentHashMap<Set<String>, Double>();
		
        for(Element el:aherf)
        {
        	TaskParallelizeExtractAbstract t1 = new TaskParallelizeExtractAbstract(el,abstractlist,port);
            executor.execute(t1);
     	  // System.out.println("url is: "+elem_query);
     	  //System.out.println("element string: "+elem_string);
     	  // System.out.println(el.attr("href"));
     	  
        }
        System.out.println("extracted abstract");
        executor.shutdown();
	    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
	     System.out.println(abstractlist.size());
    for(Element ss:abstractlist.keySet())
    {
    	tot_abstract = tot_abstract+" "+abstractlist.get(ss);
    }
        //System.out.println("extracted abstract: "+tot_abstract);
        return tot_abstract;
	}
	
	public static String extractAbstractParallelFromDisk(String query, int k, String port) throws Exception
	{
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate1 = new Date();
		System.out.println("entered abstract extraction "+dateFormat.format(firstDate1));
		
		String query_req="http://localhost:"+port+"/search?q="+query.replace(" ", "+").replace("-", "+").replace("_", "+")+"&start=1&n="+k;
        String html_string = Jsoup.connect(query_req).get().html();
        Date firstDate2 = new Date();
        System.out.println("connected via Jsoup "+dateFormat.format(firstDate2));
        
        Document html = Jsoup.parse(html_string);
        
        Date firstDate3 = new Date();
        System.out.println("parse html string "+dateFormat.format(firstDate3));
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
	      //an instance of builder to parse the specified xml file  
	     DocumentBuilder db = dbf.newDocumentBuilder();  
	
        //System.out.println("html is: "+html);
       // String h1 = html.body().getElementsByTag("a").text();
        Elements aherf = html.body().getElementsByTag("a");
        
        Date firstDate4 = new Date();
        System.out.println("retrieved ahref tags "+dateFormat.format(firstDate4));
        
          String tot_abstract="";
        ConcurrentHashMap<Element,String> abstractlist = new ConcurrentHashMap<Element,String>();
        int count=aherf.size();
		 int num_threads = (Runtime.getRuntime().availableProcessors()+1)/2;

	        ThreadPoolExecutor executor = new ThreadPoolExecutor(num_threads, count, Long.MAX_VALUE, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(count));

	        //ConcurrentHashMap<Set<String>, Double> scores = new ConcurrentHashMap<Set<String>, Double>();
		
        for(Element el:aherf)
        {
        	TaskParallelizeExtractAbstractFromDisk t1 = new TaskParallelizeExtractAbstractFromDisk(el,abstractlist,port,db);
            executor.execute(t1);
     	  // System.out.println("url is: "+elem_query);
     	  //System.out.println("element string: "+elem_string);
     	  // System.out.println(el.attr("href"));
     	  
        }
        System.out.println("extracted abstract");
        executor.shutdown();
	    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
	     System.out.println(abstractlist.size());
    for(Element ss:abstractlist.keySet())
    {
    	tot_abstract = tot_abstract+" "+abstractlist.get(ss);
    }
        //System.out.println("extracted abstract: "+tot_abstract);
        return tot_abstract;
	}
	
	
	public static Result extractPhrases(String text) throws Exception
	{
		
        
		//text = extractAbstracts();
		String[] stopWords = new SmartWords().getSmartWords(); 
	    String[] stopPOS = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"}; 
	    int minWordChar = 1;
	    boolean shouldStem = true;
	    String phraseDelims = "[-,.?():;\"!/]"; 
	    RakeParams params = new RakeParams(stopWords, stopPOS, minWordChar, shouldStem, phraseDelims);
	    
	    // Create a RakeAlgorithm object
	    String POStaggerURL = "model-bin/en-pos-maxent.bin"; // The path to your POS tagging model
	    String SentDetecURL = "model-bin/en-sent.bin"; // The path to your sentence detection model
	    RakeAlgorithm rakeAlg = new RakeAlgorithm(params, POStaggerURL, SentDetecURL);
	    
	    // Call the rake method
	    //String txt = "dogs are great, don't you agree? I love dogs, especially big dogs";
	    Result result = rakeAlg.rake(text);
	    
	    // Print the result
	    return  result.distinct();
	}
	
	public static List benchmarkStringIndexOf(String longString) {
		List stringSplit = new ArrayList<>();
	    int pos = 0, end;
	    while ((end = longString.indexOf(' ', pos)) >= 0) {
	        stringSplit.add(longString.substring(pos, end));
	        pos = end + 1;
	    }
	    return stringSplit;
	}
	
	public static HashMap<String, double[]> computeFeature(HashMap<String, Collection<Emit>> phrases, AdjListCompact a1)
	{
		HashMap<String, double[]> features = new HashMap<String, double[]>();
		HashMap<String, Integer> wordToInt = new HashMap<String, Integer>();
		
			for(String p:phrases.keySet())
			{
				//System.out.println(p+"\n");
				int wi=0;
				String[] words = p.split(" ");
				for(String w:words)
				{
					//System.out.println(w+"\n");
					if(w.length()==1) continue;
					if(wordToInt.get(w)==null)
					{
						wordToInt.put(w, wi);
						wi++;
						
					}
				}
				for(Emit e:phrases.get(p))
				{
					String ent = e.getKeyword().replace(" ", "_");
					
					if(ent.length()<=4) continue;
					//System.out.println(ent+"\n");
					if(wordToInt.get(ent)==null)
					{
						wordToInt.put(ent, wi);
						wi++;
					}
					Set<Integer> neigh = a1.undirectedGraph.adjacentNodes(a1.nodeMap1.get(ent));
					for(int ent1:neigh)
					{
						String ent1_string=a1.nodeMap.get(ent1);
						//System.out.println(ent1_string+"\n");
						if(ent1_string.length()<=4) continue;
						if(wordToInt.get(ent1_string)==null)
						{
							wordToInt.put(ent1_string, wi);
							wi++;
						}
					}
					
				}
				
			}
			AdjList a = new AdjList();
			for(String p:phrases.keySet())
			{
				//System.out.println(p+"\n");
				ArrayList<Double> myList= new ArrayList<>(Arrays.asList(new Double[wordToInt.size()]));
			    Collections.fill(myList, 0.0);//fills all 40 entries 
			    String[] words = p.split(" ");
			    for(String w:words)
				{
			    	if(w.length()==1) continue;
			    	//System.out.println(w+"\n");
					myList.set(wordToInt.get(w), 1.0);
				}
				for(Emit e:phrases.get(p))
				{
					String ent = e.getKeyword().replace(" ", "_");
					if(ent.length()<=4) continue;
					//System.out.println(ent+"\n");
					myList.set(wordToInt.get(ent), 1.0);
					Set<Integer> neigh = a1.undirectedGraph.adjacentNodes(a1.nodeMap1.get(ent));
					for(int ent1:neigh)
					{
						String ent1_string=a1.nodeMap.get(ent1);
						if(ent1_string.length()<=4) continue;
						//System.out.println(ent1_string+"\n");
						myList.set(wordToInt.get(ent1_string), 1.0);
					}
				}
				double[] lm = new double[myList.size()];
				int i=0;
				for(double m:myList)
				{
					lm[i]=m;
					i++;
				}
				
				features.put(p,lm);
				
			}
			return features;
	}
	
	public static HashMap<String, HashSet<String>> computeFeatureString(HashMap<String, Collection<Emit>> phrases, AdjListCompact a1)
	{
		HashMap<String, HashSet<String>> features = new HashMap<String, HashSet<String>>();
			for(String p:phrases.keySet())
			{
				HashSet<String> feature_string = new HashSet<String>();
				
				//System.out.println(p+"\n");
				
				String[] words = p.split(" ");
				for(String w:words)
				{
					//System.out.println(w+"\n");
					feature_string.add(w);
				}
				for(Emit e:phrases.get(p))
				{
					String ent = e.getKeyword().replace(" ", "_");
					
					if(ent.length()<=4) continue;
					//System.out.println(ent+"\n");
					feature_string.add(ent);
					Set<Integer> neigh = a1.undirectedGraph.adjacentNodes(a1.nodeMap1.get(ent));
					for(int ent1:neigh)
					{
						String ent1_string=a1.nodeMap.get(ent1);
						//System.out.println(ent1_string+"\n");
						if(ent1_string.length()<=4) continue;
						feature_string.add(ent1_string);
					}
					
				}
				features.put(p, feature_string);
				
			}
			
			return features;
	}
	
	public static HashMap<String, HashMap<String, Double>> computePairwiseSim(HashMap<String, LanguageModelEntity> features)
	{
		HashMap<String, HashMap<String, Double>> pairwise_sim = new HashMap<String, HashMap<String, Double>>();
		ArrayList<String> phrases = new ArrayList<String>(features.keySet());
		for(int i=0;i<phrases.size()-1;i++)
		{
			HashMap<String, Double> sim_for_phrase= new HashMap<String, Double>();
			LanguageModelEntity e1 = features.get(phrases.get(i));
			for(int j=i+1;j<phrases.size();j++)
			{
				double cosine_sim = e1.cosineSimilarity(features.get(phrases.get(j)));
				sim_for_phrase.put(phrases.get(j), cosine_sim);
			}
			pairwise_sim.put(phrases.get(i), sim_for_phrase);
		}
		return pairwise_sim;
	}
	
	public static ArrayList<Entry<String,Double>> findPrerequisitesAndRank(Set<String> f, Trie trie, HashSet<String> tagged_entities)
	{
		HashMap<String, Double> ent = new HashMap<String, Double>();
		for(String fac:f)
		{
		   Collection<Emit> named_entity_occurences = trie.parseText(fac.toLowerCase());
		   for(Emit e:named_entity_occurences)
		   {
			   String ee = e.getKeyword().replace(" ", "_");
			   if(tagged_entities.contains(ee)) continue;
			   if(e.getKeyword().length()<=4) continue;
			   
			   if(ent.get(e.getKeyword())==null)
			   {
				   ent.put(e.getKeyword(), 1.0);
			   }
			   else
			   {
				   ent.put(e.getKeyword(), ent.get(e.getKeyword())+1.0);
			   }
		   }
		   
		}
		
		ArrayList<Entry<String, Double>> entryset = new ArrayList<Entry<String, Double>>(ent.entrySet());
		Collections.sort(entryset, LanguageModelEntity.valueComparator);
		return entryset;
	}
	
	public static ArrayList<Entry<String,Double>> findPrerequisitesAndRank1(Set<String> f, Trie trie, HashMap<String, LanguageModelEntity> s, String aspect, HashSet<String> tagged_entities, HashMap<String,Double> refdScores)
	{
		LanguageModelEntity lm = s.get(aspect.replace(" ", "_"));
		HashMap<String, Double> ent = new HashMap<String, Double>();
		for(String fac:f)
		{
		   Collection<Emit> named_entity_occurences = trie.parseText(fac.toLowerCase());
		   for(Emit e:named_entity_occurences)
		   {
			   String ee = e.getKeyword().replace(" ", "_");
			   if(tagged_entities.contains(ee)) continue;
			   if(e.getKeyword().length()<=4) continue;
			   if(ent.get(e.getKeyword())==null)
			   {
				   ent.put(e.getKeyword(), 1.0);
			   }
			   else
			   {
				   ent.put(e.getKeyword(), ent.get(e.getKeyword())+1.0);
			   }
		   }
		   
		}
		int count_f=0;
		HashMap<String,Double> sim = new HashMap<String,Double>();
	   for(String e:ent.keySet())
	   {
		   
		   if(lm==null) sim.put(e, ent.get(e));
		   else 
		   {
			    e = e.replace(" ", "_");
			   LanguageModelEntity lm1 = s.get(e);
			   if(lm1!=null)
			   {
				   double cos = lm.cosineSimilarity(lm1);
				   //sim.put(e, cos*ent.get(e));
				   sim.put(e, cos);

			   }
		   }
	   }
		ArrayList<Entry<String, Double>> entryset = new ArrayList<Entry<String, Double>>(sim.entrySet());
		Collections.sort(entryset, LanguageModelEntity.valueComparator);
		ArrayList<Entry<String,Double>> new_list = new ArrayList<Entry<String,Double>>();
		SimpleWebApp simple = new SimpleWebApp();
		for(int i=0;i<entryset.size();i++)
    	{
			count_f++;
			MyEntry<String, Double> mm = simple.getEntry(entryset.get(i).getKey(),entryset.get(i).getValue());
			new_list.add(mm);
			if(count_f==10) break;
    	}
		
		return new_list;
		/*ArrayList<Entry<String,Double>> new_list = new ArrayList<Entry<String,Double>>();
    	int count_f=0;
    	SimpleWebApp simple = new SimpleWebApp();
    	for(int i=0;i<entryset.size();i++)
    	{
    		if(refdScores.get(entryset.get(i).getKey())!=null)
    		{
    			if (refdScores.get(entryset.get(i).getKey())>0.0)
    			{
    				count_f++;
    				MyEntry<String, Double> mm = simple.getEntry(entryset.get(i).getKey(),refdScores.get(entryset.get(i).getKey()));
    				new_list.add(mm);
    				if(count_f==10) break;
    			}
    		}
			//MyEntry<String, Double> mm = simple.getEntry(ss.get(i).getKey(),refdScores.get(ss.get(i).getKey()));
    		//sss.MyEntry<String, Double> mm= new sss.MyEntry<String,Double>(ss.get(i).getKey(),refdScores.get(ss.get(i).getKey()));
    		
    		
    	}
		
		return new_list;*/
	}
	
	final class MyEntry<K, V> implements Map.Entry<K, V> {
	    private final K key;
	    private V value;

	    public MyEntry(K key, V value) {
	        this.key = key;
	        this.value = value;
	    }

	    @Override
	    public K getKey() {
	        return key;
	    }

	    @Override
	    public V getValue() {
	        return value;
	    }

	    @Override
	    public V setValue(V value) {
	        V old = this.value;
	        this.value = value;
	        return old;
	    }
	}
	
	public static double jaccardSim(Set<String> s1, Set<String> s2, HashMap<String, HashSet<String>> features)
	{
		HashSet<String> f1 = new HashSet<String>();
		HashSet<String> f2 = new HashSet<String>();

		for(String ss:s1)
		{
			f1.add(ss);
		}
		for(String ss:s2)
		{
			f2.add(ss);
		}
		int a = f1.size();
		int b=f2.size();
		f2.retainAll(f1);
		double sim=1.0*((f2.size()*1.0)/((a+b-f2.size())*1.0));
		return sim;
	}
	
	public static void testJaccardSim(String p, String pp) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/features_for_jaccard"));
		String line;
		HashMap<String, HashSet<String>> features = new HashMap<String, HashSet<String>>();
		while((line=br.readLine())!=null)
		{
			
			String arr[] = line.split("\t");
			if(arr.length<2) continue;
			String list[] = arr[1].replace("[", "").replace("]", "").split(", ");
			HashSet<String> f = new HashSet<String>(Arrays.asList(list));
			features.put(arr[0], f);
		}
		
		System.out.println("read features");
		HashSet<String> p_neigh = features.get(p);
		p_neigh.add(p);
		int a = p_neigh.size();
		
		HashSet<String> pp_neigh=features.get(pp);
		int b=pp_neigh.size();
		pp_neigh.retainAll(p_neigh);
		double sim=1.0*((pp_neigh.size()*1.0)/((a+b-pp_neigh.size())*1.0));
		
		System.out.println("jaccard sim is: "+sim);
	}
	
	public static Set<String> checkExtractNeighbor(String p, HashMap<String, HashSet<String>> features, double mindistance) throws Exception
	{
		//BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/jaccard_sim/"+p.replace(" ","_")));
		HashMap<String, Double> jaccard_sim = new HashMap<String, Double>();
		HashSet<String> p_neigh = features.get(p);
		
		p_neigh.add(p);
		
		double a = p_neigh.size()*1.0;
		for(String pp:features.keySet())
		{
			HashSet<String> pp_neigh=new HashSet<String>(features.get(pp));
			double b=pp_neigh.size()*1.0;
			pp_neigh.retainAll(p_neigh);
			double numerator=pp_neigh.size()*1.0;
			double denominator=(a+b-numerator);
			double sim=numerator/denominator;
			//double sim = 1.0/2.0;
			//double sim=1.0*((pp_neigh.size()*1.0)/((a+b-pp_neigh.size())*1.0));
			jaccard_sim.put(pp, sim);
			//bw.write(pp+"\t"+sim+"\n");
		}
		
		ArrayList<Entry<String, Double>> jaccard_sim_sorted = new ArrayList<Entry<String, Double>>(jaccard_sim.entrySet());
		Collections.sort(jaccard_sim_sorted, LanguageModelEntity.valueComparator);
		Set<String> facet = new HashSet<String>();
		int count=0;
		for(Entry e:jaccard_sim_sorted)
		{
			//System.out.println("most similar phrase to "+p+" is: "+e.getKey()+", similarity is: "+e.getValue());
			
			if((double) e.getValue()>= mindistance)
			{
				facet.add((String) e.getKey());
			}
			else break;
			count++;
			//if(count==10) break;
		}
		//bw.close();
		facet.add(p);
		//System.out.println("chosen element is "+p);
		//System.out.println("chosen set of neighbors are "+facet.toString());
		// TODO Auto-generated method stub
		return facet;
	}
	
	public static void test(String query) throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/demo/"+query+"/kl_divergences"));
		String line;
		HashMap<String,Double> kl_divergences = new HashMap<String, Double>();
		while((line=br.readLine())!=null)
		{
			String[] arr = line.split("\t");
			kl_divergences.put(arr[0], Double.parseDouble(arr[1]));
		}
		ArrayList<Entry<String, Double>> sorted_kl_divergences = new ArrayList<Entry<String, Double>>(kl_divergences.entrySet());
		Collections.sort(sorted_kl_divergences,LanguageModelEntity.valueComparatorIncreasing);
		Entry ee = Collections.min(sorted_kl_divergences,LanguageModelEntity.valueComparatorIncreasing);
		
		System.out.println("min obtained by sorting: "+sorted_kl_divergences.get(0).getKey()+"\t"+sorted_kl_divergences.get(0).getValue());
		System.out.println("min obtaied by using min: "+ee.getKey()+"\t"+ee.getValue());

	}
	
	public static void tagEntitiesInClusters(String queries, String teknowbase_mapped, String nodemap, String relmap) throws Exception
	{
		Aspect a = new Aspect();
		AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(int entity_num : a1.undirectedGraph.nodes())
        {
			String entity = a1.nodeMap.get(entity_num);
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        
        HashMap<String,ArrayList<String>> clustersWithTags = new HashMap<String, ArrayList<String>>();
		
        BufferedReader br1 = new BufferedReader(new FileReader(queries));
        String line1;
        BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/clusters_with_tags/clusters_with_tags"));
        while((line1=br1.readLine())!=null)
        {
        	String query=line1;
			String file="/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/demo/"+query+"/kl_divergences";
			BufferedReader brr = new BufferedReader(new FileReader(file));
			String line;
	
			while((line=brr.readLine())!=null)
			{
				HashMap<String, Double> ent = new HashMap<String, Double>();
				
				String[] arr = line.split("\t");
				Collection<Emit> named_entity_occurences = trie.parseText(arr[0].toLowerCase());
				   for(Emit e:named_entity_occurences)
				   {
					  
					   if(e.getKeyword().length()<=4) continue;
					   if(ent.get(e.getKeyword())==null)
					   {
						   ent.put(e.getKeyword(), 1.0);
					   }
					   else
					   {
						   ent.put(e.getKeyword(), ent.get(e.getKeyword())+1.0);
					   }
				   }
				ArrayList<Entry<String, Double>> entryset = new ArrayList<Entry<String, Double>>(ent.entrySet());
				Collections.sort(entryset, LanguageModelEntity.valueComparator);
				int count=0;
				for(Entry<String,Double> ee:entryset)
				{
					String ent1 = ee.getKey().replace(" ", "_");
					if(clustersWithTags.get(ent1)==null)
					{
						ArrayList<String> aa = new ArrayList<String>();
						aa.add(arr[0]);
						clustersWithTags.put(ent1,aa);
					}
					else
					{
						ArrayList<String> aa = clustersWithTags.get(ent1);
						aa.add(arr[0]);
						clustersWithTags.put(ent1,aa);
					}
					count++;
					if(count==5) break;
					
				}
				
			}
			
	  }
        for(String c:clustersWithTags.keySet())
        {
        	for(String clusters:clustersWithTags.get(c))
        	{
        		bw.write(c+"\t"+clusters+"\n");
        	}
        }
        bw.close();
			
	}
	
	public static void testRanking4(String query, String teknowbase_mapped, String nodemap, String relmap, double mindistance, int k, double lambda, String entityMap, String embedding, String wikipedia) throws Exception
	{
		
			BufferedReader brr = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopics/"+query+"/mixture/refd_mixture"));
			String line1;
			HashMap<String, Integer> ent_index = new HashMap<String, Integer>();
	 	   HashMap<String, Double> ent_val = new HashMap<String, Double>();
	 	   ArrayList<Double> prob_dist_query_list = new ArrayList<Double>();
	 	   int count=0;
	 	   while((line1=brr.readLine())!=null)
	 	   {
	 		   String[] arr = line1.split("\t");
	 		   ent_index.put(arr[0], count);
	 		   ent_val.put(arr[0], Double.parseDouble(arr[1]));
	 		   count++;
	 	   }
	 	   double prob_dist_query[] = new double[ent_index.size()];
	 	   for(String e:ent_index.keySet())
	 	   {
	 		   prob_dist_query[ent_index.get(e)]=ent_val.get(e);
	 	   }
	 	  BufferedReader br = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/demo/"+query+"/kl_divergences"));
			String line;
		HashMap<String,Double> kl_divergences = new HashMap<String, Double>();
		while((line=br.readLine())!=null)
		{
			String[] arr = line.split("\t");
			kl_divergences.put(arr[0], Double.parseDouble(arr[1]));
		}
		System.out.println("read kl divergences");
		
		 br = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/demo/"+query+"/sizes"));
		
		HashMap<String,Double> sizes = new HashMap<String, Double>();
		while((line=br.readLine())!=null)
		{
			String[] arr = line.split("\t");
			sizes.put(arr[0], Double.parseDouble(arr[1]));
		}
		System.out.println("read kl divergences");
		ArrayList<Entry<String, Double>> sorted_kl_divergences = new ArrayList<Entry<String, Double>>(kl_divergences.entrySet());
		HashSet<String> remaining_phrases = new HashSet<String>();
		remaining_phrases.addAll(kl_divergences.keySet());
		HashMap<String, double[]> lm_rep = new HashMap<String, double[]>();

		for(String p:remaining_phrases)
		{
			double[] lm = computeLM(p,ent_index);
			lm_rep.put(p, lm);
		}
		System.out.println("computed lms for clusters");
		Collections.sort(sorted_kl_divergences, LanguageModelEntity.valueComparatorIncreasing);;
		
		Maths m = new Maths();
		int requested=10;
		HashMap<Set<String>,Double> finalFacets = new HashMap<Set<String>,Double>();
		for(int i=0;i<requested;i++)
		   {
			System.out.println(i);
			   if(i==0)
			   {
				   Entry<String, Double> ee = sorted_kl_divergences.get(0);
				   String p = (String) ee.getKey();
				   System.out.println(p+"\t"+ee.getValue());
				   HashSet<String> neighs = new HashSet<String>();
				   neighs.add(p);
				   finalFacets.put(neighs, 10.0);
				   remaining_phrases.remove(p);
				   System.out.println(neighs.toString());
			   }
			   else
			   {
				   double[] prob_dist_S = computeLM(finalFacets, ent_index);
				   //double kl_div = m.klDivergence(prob_dist_query,prob_dist_S);
				   HashMap<String, Double> remaining_phrases_div = new HashMap<String, Double>();
				   for(String p:remaining_phrases)
				   {
					   double[] lm = lm_rep.get(p);
					   double kl_div = m.klDivergence(prob_dist_S,lm);
					   remaining_phrases_div.put(p,kl_divergences.get(p)/(sizes.get(p)*kl_div));
				   }
				   ArrayList<Entry<String, Double>> remaining_phrases_sorted = new ArrayList<Entry<String, Double>>(remaining_phrases_div.entrySet());
				   Entry ee1 = Collections.min(remaining_phrases_sorted, LanguageModelEntity.valueComparatorIncreasing);
				   String p = (String) ee1.getKey();
				  System.out.println(p+"\t"+ee1.getValue());
				  remaining_phrases.remove(p);
				  
			   }
		   }
	}
	
	public static void prefaceTest(String args[], String wikipedia, String wikipedia_first_para, String entityMap, String embedding, String teknowbase_mapped, String nodemap, String relmap,double theta, double sim, String query, double mindistance, int k,String galago_port,String remote_port_number)  throws Exception
	{
		System.out.println("updated working");
		System.out.println(args);
    	
		TagEntities te = new TagEntities();
	    Aspect a = new Aspect();
	    AdjList dummy = new AdjList();
	    ReadSubgraph rr = new ReadSubgraph();
		AdjList aa = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
		AdjList aa_firstpara = rr.readFromFile(new BufferedReader(new FileReader(wikipedia_first_para)));

		UtilityFunctions u = new UtilityFunctions();
		HashMap<String, String> stringToId = u.createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> s = u.readEmbeddingsFromFile(embedding,stringToId,dummy);
		

  		AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
  		BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/bad_entities"));
	     String line;
	     HashSet<String> bad_entities = new HashSet<String>();
	     while((line=br1.readLine())!=null)
	     {
	    	 bad_entities.add(line);
	     }
		for(int entity_num : a1.undirectedGraph.nodes())
        {
			String entity = a1.nodeMap.get(entity_num);
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        HashMap<String, String> descriptions_old = new HashMap<String, String>();
        HashMap<String, ArrayList<String>> address = te.getAddress("/home/prajna/workspace/TutorialBank/data/topics_to_resources.csv");
  		HashMap<String, String> descriptions = te.readFile1("/mnt/dell/prajna/allwikipedia/all_text_1_tkb");
  		
  		if(aa.getAdjList().get(query)==null)
        {
     	   System.out.println(query+" does not exist!!");
        }
        else
        {
        	int requested=10;
     	   HashMap<String,Double> refdScores = new HashMap<String,Double>();

        	 HashMap<String, Double> scores1 = new HashMap<String, Double>();
         	   HashMap<String, HashSet<String>> support_sentences = new HashMap<String, HashSet<String>>();
      			if(descriptions.get(query)!=null)
      			{
      				scores1 = te.computeScore(a1.nodeMap1, descriptions.get(query), query, support_sentences, trie);
      			}
         	 
     	   ArrayList<Entry<Set<String>,Double>> entryset = extractFacets(query, trie, mindistance, k,  0.0, address, descriptions, a1,aa, aa_firstpara, theta, sim, s,galago_port,bad_entities,support_sentences, refdScores, requested);
     	  testSftp(Integer.parseInt(remote_port_number),"10.17.50.132","root","welcome@123","/var/www/preface/json_files/"+query+"_"+sim+"_"+theta+".json","/var/www/app/prerequisites/json_files/");
   	    //	writer.println("</body></html>");
			System.out.println("<html>");
			System.out.println("<head>\n" + 
       			"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" + 
       			"<link rel=\"stylesheet\" type=\"text/css\" href=\"tree-boxes.css\">\n" + 
       			"\n" + 
       			"    <script src=\"https://code.jquery.com/jquery-latest.min.js\" type=\"text/javascript\"></script>\n" + 
       			"    <script src=\"https://d3js.org/d3.v3.min.js\" type=\"text/javascript\"></script>\n" + 
       			"    <script src=\"tree-boxes.js\" type=\"text/javascript\"></script>\n"+
       			"<style>\n" + 
       			"body {font-family: Arial;}\n" + 
       			"\n" + 
       			"/* Style the tab */\n" + 
       			".tab {\n" + 
       			"  overflow: hidden;\n" + 
       			"  border: 1px solid #ccc;\n" + 
       			"  background-color: #f1f1f1;\n" + 
       			"}\n" + 
       			"\n" + 
       			"/* Style the buttons inside the tab */\n" + 
       			".tab button {\n" + 
       			"  background-color: inherit;\n" + 
       			"  float: left;\n" + 
       			"  border: none;\n" + 
       			"  outline: none;\n" + 
       			"  cursor: pointer;\n" + 
       			"  padding: 14px 16px;\n" + 
       			"  transition: 0.3s;\n" + 
       			"  font-size: 17px;\n" + 
       			"}\n" + 
       			"\n" + 
       			"/* Change background color of buttons on hover */\n" + 
       			".tab button:hover {\n" + 
       			"  background-color: #ddd;\n" + 
       			"}\n" + 
       			"\n" + 
       			"/* Create an active/current tablink class */\n" + 
       			".tab button.active {\n" + 
       			"  background-color: #ccc;\n" + 
       			"}\n" + 
       			"\n" + 
       			"/* Style the tab content */\n" + 
       			".tabcontent {\n" + 
       			"  display: none;\n" + 
       			"  padding: 6px 12px;\n" + 
       			"  border: 1px solid #ccc;\n" + 
       			"  border-top: none;\n" + 
       			"}\n" + 
       			"</style>\n" + 
       			"</head>\n"); 
       		System.out.println("<title>Welcome</title><body>");
        
       			System.out.println("<table>\n" + 
       				"		<tr>\n" + 
       				"			<td style=\"height:100px\">\n" + 
       				"    <div class=\"container\">\n" + 
       				"        <ct-visualization id=\"tree-container\"></ct-visualization>\n" + 
       				"        <script>\n" + 
       				"            d3.json(\"json_files/"+query+"_"+sim+"_"+theta+".json\", function(error, json) {\n" + 
       				"                treeBoxes('', json.tree);\n" + 
       				"            });\n" + 
       				"        </script>\n" + 
       				"    </div>\n" + 
       				"    </td>\n" + 
       				"    </tr>\n" + 
       				"    ");
       		 
       		
       		System.out.println(" <tr>\n" + 
              		"		<td>\n" + 
              		"			<h2>Aspects</h2>\n" + 
              		"			<p>Click on the buttons inside the tabbed menu:</p>\n" + 
              		"		</td>\n" + 
              		"	</tr>\n" + 
              		"	<tr>\n" + 
              		"	<td>\n" + 
              		"<div class=\"tab\">"); 
              ArrayList<ArrayList<Entry<String,Double>>> facetsOrdered = new ArrayList<ArrayList<Entry<String,Double>>>();
              HashMap<String,ArrayList<Entry<String,Double>>> facets_for_aspects = new HashMap<String,ArrayList<Entry<String,Double>>>();
            for(Entry e:entryset)
       	   	{
            	  Set<String> s1 = (Set<String>) e.getKey();
         			ArrayList<Entry<String, Double>> ss = findPrerequisitesAndRank(s1, trie, bad_entities);
         			facetsOrdered.add(ss);
         			String aspect_name=ss.get(0).getKey();
         			ArrayList<Entry<String, Double>> ss_new = findPrerequisitesAndRank1(s1, trie,s,aspect_name,bad_entities, refdScores);
         			facets_for_aspects.put(aspect_name, ss_new);
         			System.out.println("<button class=\"tablinks\" onclick=\"openCity(event,'"+aspect_name+"')\">"+aspect_name+"</button>\n");

       	   	}
   	 System.out.println("</div>\n" + 
   	    		"</td>\n" + 
   	    		"</tr>\n" + 
   	    		"</table>");
   	    int count=0;
   	    
   	    for(String aspect_name:facets_for_aspects.keySet())
        {
   	    	ArrayList<Entry<String,Double>> ss = facets_for_aspects.get(aspect_name);
           	//if(!succset1.contains(e.getKey())) continue;
           	//if(scores1.get(e.getKey())==null && co_occ.get(e.getKey())==null && !succset1.contains(e.getKey())) continue;
           	count++;
           	//String aspect_name=ss.get(0).getKey();
			
           	System.out.println("<div id=\""+aspect_name+"\" class=\"tabcontent\">");
           	System.out.println("<h3>"+aspect_name+": "+ss.get(0).getValue()+", "+scores1.get(aspect_name));
           	
           	System.out.println("</h3>");
           	
           	//writer.println("<h3>"+e.getKey()+", "+e.getValue()+"</h3>");
           	for(int i=1;i<ss.size();i++)
              	{
           	//for(String ss:subtypes.get(e.getKey()))
           	//{
           		int c1=1;
           		String aspect_value=ss.get(i).getKey();
           		System.out.println("<p>"+aspect_value+", "+scores1.get(aspect_value)+"</p><br>");
           		if(support_sentences.get(aspect_value)!=null)
               	{
           			for(String ss1:support_sentences.get(aspect_value))
	                	{
           				System.out.println("<p>"+c1+") "+ss1+"</p>");
	                		c1++;
	                	}
               	}
           	}
           	System.out.println("</div>");
           }
           
   	    	System.out.println("<script>");
           System.out.println("function openCity(evt, cityName) {");
           //writer.println("function openCity(evt, cityName) {");
           System.out.println("tabcontent = document.getElementsByClassName(\"tabcontent\");");
           System.out.println(" for (i = 0; i < tabcontent.length; i++) {\n" + 
           		"    tabcontent[i].style.display = \"none\";\n" + 
           		"  }\n" + 
           		"  tablinks = document.getElementsByClassName(\"tablinks\");\n" + 
           		"  for (i = 0; i < tablinks.length; i++) {\n" + 
           		"    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");\n" + 
           		"  }\n" + 
           		"  document.getElementById(cityName).style.display = \"block\";\n" + 
           		"  evt.currentTarget.className += \" active\";\n" + 
           		"}\n" + 
           		"</script>");
           // writer.println("<iframe src=\"https://en.wikipedia.org/wiki/PageRank#Variations\">\n" + "</iframe>");
           
           System.out.println("</body></html>");
      }
	 
		/*String[] query_arr= new String[args.length+1];
		for(int i=0;i<args.length;i++)
		{
			query_arr[i]=args[i];
		}
		query_arr[args.length]="--query="+query.replace("_", " ");
		  List<Parameters> queries;
		  Parameters parameters1 = Arguments.parse(query_arr);
		  System.out.println(parameters1); 
		  String queryFormat = parameters1.get("queryFormat", "json").toLowerCase();
	        switch (queryFormat) {
	            case "json":
	                queries = JSONQueryFormat.collectQueries(parameters1);
	                break;
	            case "tsv":
	                queries = JSONQueryFormat.collectTSVQueries(parameters1);
	                break;
	            default:
	                throw new IllegalArgumentException("Unknown queryFormat: " + queryFormat + " try one of JSON, TSV");
	        }            		 
		 
		 
	        
	        System.out.println("here");
	        System.out.println(queries.get(0));

	        // record results requested
	        int requested = (int) parameters1.get("requested", 1000);

	        // for each query, run it, get the results, print in TREC format
	        HashSet<Integer> h = new HashSet<Integer>();

	        //BufferedWriter bw = new BufferedWriter(new FileWriter(doc_in_file));
	        for(Parameters query_single:queries)
	        {
	       
	            String queryText = query_single.getString("text");
	            System.out.println("text of the query is: " + queryText);
	            String queryNumber = query_single.getString("number");

	            query_single.setBackoff(parameters1);
	            query_single.set("requested", requested);

	            // option to fold query cases -- note that some parameters may require upper case
	            if (query_single.get("casefold", false)) {
	                queryText = queryText.toLowerCase();
	            }

	           
	            // parse and transform query into runnable form
	            Node root = StructuredQuery.parse(queryText);

	            // --operatorWrap=sdm will now #sdm(...text... here)
	            if (parameters1.isString("operatorWrap")) {
	                if (root.getOperator().equals("root")) {
	                    root.setOperator(parameters1.getString("operatorWrap"));
	                } else {
	                    Node oldRoot = root;
	                    root = new Node(parameters1.getString("operatorWrap"));
	                    root.add(oldRoot);
	                }
	            }
	            //String file_name=parameters.getString("entitylist");
	            Node transformed = retrieval.transformQuery(root, query_single);
	            String query_text = transformed.getText().replaceAll("\\s{2,}", " ").trim();

	            List<ScoredDocument> results;

	            // run query
	            results = retrieval.executeQuery(transformed, parameters1).scoredDocuments;
	            System.out.println("\n" + results.size() + "\n");
	             org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
	            org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(parameters1);
	            writer.println("<body>");
	            for (ScoredDocument n : results)
	            {
	                String docname = retrieval.getDocumentName(n.document);
	                org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
	                String dtext = d.contentForXMLTag("title")+"\t"+d.contentForXMLTag("paperAbstract");
	               // out.print(n.document+"\n");
	                System.out.println(docname);
	                
	                writer.println("<p>"+dtext+"</p>");
	               
	            }

	            
	        }
	        writer.println("</body></html>");*/
	        
		

        }

	
	
	/*public static void computeKLDivergenceParallel2(ArrayList<Entry<String,Double>> subtopic_sorted, HashMap<String, Integer> ent_index, double mindistance, HashMap<String, Collection<Emit>> entities_in_phrase, HashMap<String,Double> refdScores, ConcurrentHashMap<Set<String>, Double> kl_div_query, HashMap<Set<String>,double[]> lm_rep, double[] prob_dist_query, HashMap<String,HashSet<String>> features) throws Exception
	{
		
	}*/
	
	public static void computeKLDivergenceParallel2(ArrayList<Entry<String,Double>> subtopic_sorted, HashMap<String, Integer> ent_index, double mindistance, HashMap<String, Collection<Emit>> entities_in_phrase, HashMap<String,Double> refdScores, ConcurrentHashMap<Set<String>, Double> kl_div_query, HashMap<Set<String>,double[]> lm_rep, double[] prob_dist_query, HashMap<String,HashSet<String>> features,ConcurrentHashMap<String, Double> kl_div_query_string) throws Exception
	{
		int count=subtopic_sorted.size();
		 int num_threads = (Runtime.getRuntime().availableProcessors()+1)/2;

	        ThreadPoolExecutor executor = new ThreadPoolExecutor(num_threads, count, Long.MAX_VALUE, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(count));

	        //ConcurrentHashMap<Set<String>, Double> scores = new ConcurrentHashMap<Set<String>, Double>();
		for(Entry e:subtopic_sorted)
		  {
			 TaskParallelizeKLDivergence2 t1 = new TaskParallelizeKLDivergence2(e,mindistance,features,entities_in_phrase,refdScores,kl_div_query,ent_index,lm_rep,prob_dist_query,kl_div_query_string);
	            executor.execute(t1);
			  //System.out.println("computing kl divergence for: "+e.getKey());
			 // Set<String> neighs = chooseNeighborsStored(((String)e.getKey()).toLowerCase().replace("_"," "), clustersWithTags);
			 
		  }
		 executor.shutdown();
		    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
		     System.out.println(kl_div_query.size());
		 // kl_div_query=scores;
	}
	
	public static void computeKLDivergenceParallel(ArrayList<Entry<String,Double>> subtopic_sorted, HashMap<String, HashSet<String>> features, double mindistance, HashMap<String, Collection<Emit>> entities_in_phrase, HashMap<String,Double> refdScores, ConcurrentHashMap<Set<String>, Double> kl_div_query,ConcurrentHashMap<String, Double> kl_div_query_string) throws Exception
	{
		int count=subtopic_sorted.size();
		 int num_threads = (Runtime.getRuntime().availableProcessors()+1)/2;

	        ThreadPoolExecutor executor = new ThreadPoolExecutor(num_threads, count, Long.MAX_VALUE, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(count));

	        //ConcurrentHashMap<Set<String>, Double> scores = new ConcurrentHashMap<Set<String>, Double>();
		for(Entry e:subtopic_sorted)
		  {
			 TaskParallelizeKLDivergence t1 = new TaskParallelizeKLDivergence(e,mindistance,features,entities_in_phrase,refdScores,kl_div_query,kl_div_query_string);
	            executor.execute(t1);
			  //System.out.println("computing kl divergence for: "+e.getKey());
			 // Set<String> neighs = chooseNeighborsStored(((String)e.getKey()).toLowerCase().replace("_"," "), clustersWithTags);
			 
		  }
		 executor.shutdown();
		    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
		     System.out.println(kl_div_query.size());
		  //kl_div_query=scores;
	}
	
	 
	public static void computeKLDivergence(ArrayList<Entry<String,Double>> subtopic_sorted, HashMap<String, HashSet<String>> features, double mindistance, HashMap<String, Collection<Emit>> entities_in_phrase, HashMap<String,Double> refdScores, HashMap<Set<String>, Double> kl_div_query) throws Exception
	{
		for(Entry e:subtopic_sorted)
		  {
			  Set<String> neighs = checkExtractNeighbor(((String)e.getKey()).toLowerCase().replace("_"," "), features,mindistance);
			  if(neighs==null) 
			  {
				  System.out.println(" null for "+e.getKey());
				  continue;
			  }
			  double score=0.0;
			  for(String p:neighs)
			  {
				 
				  Collection<Emit> cc = entities_in_phrase.get(p);
				  HashSet<String> uniqueEnt = new HashSet<String>();
				  for(Emit e1:cc)
				  {
					 String ent = e1.getKeyword().replace(" ", "_");
					 uniqueEnt.add(ent);
				  }
				  for(String ent:uniqueEnt)
				  {
					 //String ent = e1.getKeyword().replace(" ", "_");
					 if(refdScores.get(ent)!=null)
					 {
						 score = score + refdScores.get(ent);
					 }
				  }
				  
				 
			  }
			  kl_div_query.put(neighs, 1.0/score);
			  //System.out.println("computing kl divergence for: "+e.getKey());
			 // Set<String> neighs = chooseNeighborsStored(((String)e.getKey()).toLowerCase().replace("_"," "), clustersWithTags);
			 
		  }
	}
	
	public static String convertToString(Set<String> s)
	{
		ArrayList<String> al = new ArrayList<String>(s);
		Collections.sort(al);
		String ss="";
		for(String s1:al)
		{
			ss = ss + " "+s1;
		}
		return ss;
	}
	
	/**
	 * function that returns the set of facets for a query
	 * @param query
	 * @param mindistance
	 * @param k
	 * @param lambda
	 * @param address
	 * @param descriptions
	 * @param a1
	 * @param aa
	 * @return
	 * @throws Exception
	 */
	
	public static ArrayList<Entry<Set<String>,Double>> extractFacets(String query,Trie trie,double mindistance, int k, double lambda, HashMap<String, ArrayList<String>> address, HashMap<String, String> descriptions, AdjListCompact a1, AdjList aa, AdjList aa_firstpara, double theta, double sim, HashMap<String, LanguageModelEntity> s, String galago_port, HashSet<String> bad_entities, HashMap<String, HashSet<String>> support_sentences, HashMap<String,Double> refdScores, int requested) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        HashSet<String> tagged_entities = new HashSet<String>();
		
        if(address.get(query)!=null)
	 	  {
	  			String tot_line="";
	  			for(String ad:address.get(query))
	  			{
	      			BufferedReader br = new BufferedReader(new FileReader(ad));
	      			String line;
	      			while((line=br.readLine())!=null)
	      			{
	      				tot_line = tot_line + " . "+line;
	      			}
	      			if(descriptions.get(query)!=null)
	      			{
	      				descriptions.put(query, descriptions.get(query)+" . "+tot_line);
	      			}
	      			br.close();
	  			}
	 	  }
        Date firstDate1 = new Date();
		System.out.println("read trie "+dateFormat.format(firstDate1));
		HashMap<String, Double> pairwise_scores = new HashMap<String, Double>();
		File phraseFile = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+query);
	    HashMap<String, Collection<Emit>> entities_in_phrase = new HashMap<String, Collection<Emit>>();

		if(!phraseFile.exists())
		{
	 	 // String tot_abstract = StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(extractAbstract(query,k),"<lb>",""),"\"",""),"'",""),"’",""),"“","");
	 	 String tot_abstract = extractAbstract(query,k,galago_port);
 
	 	 Date firstDate2 = new Date();
	 	 System.out.println("extracted abstract "+dateFormat.format(firstDate2));
		
	 	  tot_abstract = tot_abstract + " "+descriptions.get(query);
	 	   
	 	   Result r = extractPhrases(tot_abstract) ;
	 	  
	 	   	Date firstDate3 = new Date();
			System.out.println("extracted phrases "+dateFormat.format(firstDate3));
		
	     BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/phrases/"+query));
	       System.out.println("extracted phrases");
	       //HashMap<String, ArrayList<String>> phrases = new HashMap<String, ArrayList<String>>();
	 	   for(int i=0;i<r.getFullKeywords().length;i++)
	 	   {
	 		   //System.out.println(r.getFullKeywords()[i]);
	 		   if(r.getScores()[i]<5) continue;
	 		   if(r.getFullKeywords()[i].split(" ").length>5) continue;
	 		  // System.out.println(r.getFullKeywords()[i]);
	 		  bw.write("('"+r.getFullKeywords()[i]+"', "+r.getScores()[i]+")\n");
	 		   Collection<Emit> named_entity_occurences = trie.parseText(r.getFullKeywords()[i].toLowerCase());
	 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
	 		   for(Emit e:named_entity_occurences)
	           {
	 			   	String ent = e.getKeyword().replace(" ", "_");
	           	 	if(e.getKeyword().length()<=4) continue;
	           	 	if(bad_entities.contains(ent)) continue;
	           	 	if(pairwise_scores.get(ent)==null)
	    			 pairwise_scores.put(ent, 1.0);
	    			 else
	    			 {
	    				 pairwise_scores.put(ent, pairwise_scores.get(ent)+1.0);
	    			 }
	           	 	tagged_entities.add(e.getKeyword());
	           }
	 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
	 	   }
	 	   bw.close();
		}
		else
		{
			BufferedReader brr = new BufferedReader(new FileReader(phraseFile.getAbsolutePath()));
			String line;
			while((line=brr.readLine())!=null)
			{
				String[] arr = line.split("', ");
				String phrase = arr[0].substring(2);
				 Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
		 		   entities_in_phrase.put(phrase, named_entity_occurences);
		 		   for(Emit e:named_entity_occurences)
		           {
		 			   	String ent = e.getKeyword().replace(" ", "_");
		           	 	if(e.getKeyword().length()<=4) continue;
		           	 	if(bad_entities.contains(ent)) continue;
		           	 	if(pairwise_scores.get(ent)==null)
		    			 pairwise_scores.put(ent, 1.0);
		    			 else
		    			 {
		    				 pairwise_scores.put(ent, pairwise_scores.get(ent)+1.0);
		    			 }
		           	 	tagged_entities.add(e.getKeyword());
		           }
		 		   entities_in_phrase.put(phrase, named_entity_occurences);

			}
		}
		
		File phraseFile1 = new File("/mnt/dell/prajna/SOPhrases/"+query);
		if(phraseFile1.exists())
		{
			BufferedReader brr = new BufferedReader(new FileReader(phraseFile1.getAbsolutePath()));
			String line;
			while((line=brr.readLine())!=null)
			{
				String[] arr = line.split("', ");
				String phrase = arr[0].substring(2);
				 Collection<Emit> named_entity_occurences = trie.parseText(phrase.toLowerCase());
		 		   entities_in_phrase.put(phrase, named_entity_occurences);
		 		   for(Emit e:named_entity_occurences)
		           {
		 			   	String ent = e.getKeyword().replace(" ", "_");
		           	 	if(e.getKeyword().length()<=4) continue;
		           	 	if(bad_entities.contains(ent)) continue;
		           	 	if(pairwise_scores.get(ent)==null)
		    			 pairwise_scores.put(ent, 1.0);
		    			 else
		    			 {
		    				 pairwise_scores.put(ent, pairwise_scores.get(ent)+1.0);
		    			 }
		           	 	tagged_entities.add(e.getKeyword());
		           }
		 		   entities_in_phrase.put(phrase, named_entity_occurences);

			}
		}
	 	   
	 	 /*  for(Edge ee:aa.getAdjList().get(query))
	 	   {
	 		   if(pairwise_scores.get(ee.getName())!=null)
	 		   {
  				 pairwise_scores.put(ee.getName(), pairwise_scores.get(ee.getName())+1.0);
	 		   }
	 		   else
	 		   {
	 			  pairwise_scores.put(ee.getName(), 1.0); 
	 		   }
	 	   }*/
	 	   
	 	  Date firstDate4 = new Date();
			System.out.println("tagged entities in phrases "+dateFormat.format(firstDate4));
		
	 	   System.out.println("size of phrases "+entities_in_phrase.size());
			System.out.println("size of tagged entities: "+pairwise_scores.size());
	
			
			HashMap<String, Double> subtopicscores = new HashMap<String, Double>();

			  for(String t:pairwise_scores.keySet()) 
			  { 
				  double ref = aa.referenceDistance(query, t);
				  if(ref>0) 
				  {
					  subtopicscores.put(t,ref*pairwise_scores.get(t));
				  
					  refdScores.put(t, ref);
				  }
			  }
			  subtopicscores.put("application", 0.0);
			  //subtopicscores.put("algorithm", 0.0);
			  subtopicscores.put("implementation", 0.0);
			  subtopicscores.put("type", 0.0);
			  subtopicscores.put("technique", 0.0);
			  for(String t:subtopicscores.keySet())
			  {
				  Collection<Emit> named_entity_occurences = trie.parseText(t.toLowerCase().replace("_", " "));
		 		  
				  entities_in_phrase.put(t.toLowerCase().replace("_", " "), named_entity_occurences);
		 		  
			  }
			  Date firstDate5 = new Date();
			  System.out.println("computed refd scores for tagged entities in phrases "+dateFormat.format(firstDate5));
			
			 
			  HashMap<String, HashSet<String>> features = computeFeatureString(entities_in_phrase, a1);
			  System.out.println("computed features");
			
			  Date firstDate6 = new Date();
			  System.out.println("computed refd scores for tagged entities in phrases "+dateFormat.format(firstDate6));
			
			  ArrayList<Entry<String, Double>> subtopic_sorted = new ArrayList<Entry<String, Double>>(subtopicscores.entrySet());
			  Collections.sort(subtopic_sorted, LanguageModelEntity.valueComparator);;
			  ConcurrentHashMap<Set<String>, Double> kl_div_query = new ConcurrentHashMap<Set<String>, Double>();
			  ConcurrentHashMap<String, Double> kl_div_query_string = new ConcurrentHashMap<String, Double>();
			  HashMap<Set<String>, double[]> lm_rep = new HashMap<Set<String>, double[]>();

			  Maths m = new Maths();
			  HashMap<Set<String>,Double> finalFacets = new HashMap<Set<String>,Double>();
			  HashMap<String,Double> finalFacetsString = new HashMap<String,Double>();
			  //BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/demo/"+query+"/results"));

			 File f = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopics/"+query+"/mixture/refd_mixture");
			 
			 if(!f.exists())
			 {
				 computeKLDivergenceParallel(subtopic_sorted, features, mindistance, entities_in_phrase, refdScores, kl_div_query,kl_div_query_string);
				 Date firstDate7 = new Date();
				 System.out.println("computed kl divergence between ideal dist and the facets "+dateFormat.format(firstDate7));
				
				 HashSet<Set<String>> remaining_phrases = new HashSet<Set<String>>(kl_div_query.keySet());
				 HashSet<String> remaining_phrases_string = new HashSet<String>();
				 ArrayList<Entry<Set<String>,Double>> sorted_kl_divergences = new ArrayList<Entry<Set<String>,Double>>(kl_div_query.entrySet());
				 Entry<Set<String>,Double> ee = Collections.min(sorted_kl_divergences,LanguageModelEntity.valueComparatorSetIncreasing);		  
				 HashSet<String> retrieved = new HashSet<String>();
				
				  for(int i=0;i<requested;)
				  {
						System.out.println(i);
						   if(i==0)
						   {
							   Set<String> p = ee.getKey();
							   finalFacets.put(p, ee.getValue());
							   retrieved.addAll(p);
							   remaining_phrases.remove(p);
							   remaining_phrases_string.add(convertToString(p));
							   Date firstDate8 = new Date();
							   System.out.println("computed the next best facet "+dateFormat.format(firstDate8));
								i++;
						   }
						   else
						   {
							   HashMap<Set<String>, Double> remaining_phrases_div = new HashMap<Set<String>, Double>();
							  
							   for(Set<String> p:remaining_phrases)
							   {
								  double jaccard_sim = jaccardSim(p,retrieved, features);
								  remaining_phrases_div.put(p,kl_div_query.get(p)/(p.size()*(1-jaccard_sim)));
							   }
							   ArrayList<Entry<Set<String>, Double>> remaining_phrases_sorted = new ArrayList<Entry<Set<String>, Double>>(remaining_phrases_div.entrySet());
							   Entry ee1 = Collections.min(remaining_phrases_sorted, LanguageModelEntity.valueComparatorSetIncreasing);
							   Set<String> p = (Set<String>) ee1.getKey();
							   String p_string = convertToString(p);
							   if(!remaining_phrases_string.contains(p_string))
							   {

							   		Date firstDate8 = new Date();
							   		System.out.println("computed the next best facet "+dateFormat.format(firstDate8));
								
							   		finalFacets.put(p, (double)ee1.getValue());
							   		remaining_phrases.remove(p);
							   		remaining_phrases_string.add(p_string);
							   		i++;
							   }
							   else
							   {
								   remaining_phrases.remove(p);
								  continue;
							   }
							   //ArrayList<Entry<String,Double>> entities = findPrerequisitesAndRank(p,trie);
							   //bw.write(entities+"\t"+ee1.getValue()+"\n\n\n");
							   //System.out.println(p+"\t"+ee1.getValue());
							  
						   }
					   }
					 
			}
			else
			{
				BufferedReader brr = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopics/"+query+"/mixture/refd_mixture"));
		 	   	String line1;
				HashMap<String, Integer> ent_index = new HashMap<String, Integer>();
		 	   HashMap<String, Double> ent_val = new HashMap<String, Double>();
		 	   ArrayList<Double> prob_dist_query_list = new ArrayList<Double>();
		 	   int count=0;
		 	   while((line1=brr.readLine())!=null)
		 	   {
		 		   String[] arr = line1.split("\t");
		 		   ent_index.put(arr[0], count);
		 		   ent_val.put(arr[0], Double.parseDouble(arr[1]));
		 		   count++;
		 	   }
		 	   double prob_dist_query[] = new double[ent_index.size()];
		 	   for(String e:ent_index.keySet())
		 	   {
		 		   prob_dist_query[ent_index.get(e)]=ent_val.get(e);
		 	   }
		
		
		 	   computeKLDivergenceParallel2(subtopic_sorted, ent_index, sim, entities_in_phrase, ent_val, kl_div_query, lm_rep, prob_dist_query, features,kl_div_query_string);
				 //computeKLDivergenceParallel2(subtopic_sorted, features, mindistance, entities_in_phrase, refdScores, kl_div_query,ent_index,lm_rep,prob_dist_query);

		  
		  Date firstDate7 = new Date();
		  System.out.println("computed kl divergence between the ideal prob dist and facets "+dateFormat.format(firstDate7));
		
		  System.out.println("size of kl div ds is :"+kl_div_query.size());
		  HashSet<Set<String>> remaining_phrases = new HashSet<Set<String>>(lm_rep.keySet());
		  HashSet<String> remaining_phrases_string = new HashSet<String>();

		  ArrayList<Entry<Set<String>,Double>> sorted_kl_divergences = new ArrayList<Entry<Set<String>,Double>>(kl_div_query.entrySet());
		  Entry<Set<String>,Double> ee = Collections.min(sorted_kl_divergences,LanguageModelEntity.valueComparatorSetIncreasing);		  
		 
		  for(int i=0;i<requested;)
		  {
				System.out.println(i);
				   if(i==0)
				   {
					   Set<String> p = ee.getKey();
					   finalFacets.put(p, ee.getValue());
					   remaining_phrases.remove(p);
					   remaining_phrases_string.add(convertToString(p));
					   i++;
					   Date firstDate8 = new Date();
					   System.out.println("computed 1st facet"+dateFormat.format(firstDate8));
						
				   }
				   else
				   {
					   double[] prob_dist_S = computeLM(finalFacets, ent_index);
					   //double kl_div = m.klDivergence(prob_dist_query,prob_dist_S);
					   HashMap<Set<String>, Double> remaining_phrases_div = new HashMap<Set<String>, Double>();
					   for(Set<String> p:remaining_phrases)
					   {
						   double[] lm = lm_rep.get(p);
						   double kl_div = m.klDivergence(prob_dist_S,lm);
						   remaining_phrases_div.put(p,kl_div_query.get(p)/(p.size()*kl_div));
					   }
					   ArrayList<Entry<Set<String>, Double>> remaining_phrases_sorted = new ArrayList<Entry<Set<String>, Double>>(remaining_phrases_div.entrySet());
					   Entry ee1 = Collections.min(remaining_phrases_sorted, LanguageModelEntity.valueComparatorSetIncreasing);
					   Set<String> p = (Set<String>) ee1.getKey();
					   String p_string = convertToString(p);
					   if(!remaining_phrases_string.contains(p_string))
					   {
						   finalFacets.put(p, (double)ee1.getValue());
						   remaining_phrases_string.add(p_string);
						   i++;
						   remaining_phrases.remove(p);
						  Date firstDate8 = new Date();
						  System.out.println("computed next best facet"+dateFormat.format(firstDate8));
						
					   }
					   else
					   {
						   remaining_phrases.remove(p);
					   }
					   //ArrayList<Entry<String,Double>> entities = findPrerequisitesAndRank(p,trie);
					   //bw.write(entities+"\t"+ee1.getValue()+"\n\n\n");
					   //System.out.println(p+"\t"+ee1.getValue());
					 
					  
				   }
			   }
			 //bw.close();
			}
		//	bw.write("\n");
		  ArrayList<Entry<Set<String>,Double>> finalfacetsList = new ArrayList<Entry<Set<String>,Double>>(finalFacets.entrySet());
		 // Collections.sort(ee,LanguageModelEntity.valueComparatorSetIncreasing);
		  File ff = new File("/var/www/preface/json_files/"+query+"_"+sim+"_"+theta+".json");
		  Date secondDate = new Date();
		  System.out.println("computed facets: "+dateFormat.format(secondDate));
		
		  if(!ff.exists())
   		   computeRefDAndGenerateJSON(query,aa_firstpara,theta,s,sim,refdScores,descriptions,support_sentences);
   	 
		  
		  	Date thirdDate = new Date();
			System.out.println("over: "+dateFormat.format(thirdDate));
		    long diffInMillies = Math.abs(secondDate.getTime() - firstDate1.getTime());
		    System.out.println(diffInMillies);
		    return finalfacetsList;
	}
	
	public static void testRanking3(String query, String teknowbase_mapped, String nodemap, String relmap, double mindistance, int k, double lambda, String entityMap, String embedding, String wikipedia,String galago_port) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate = new Date();
		System.out.println(dateFormat.format(firstDate));
		
		TagEntities te = new TagEntities();
		AdjList dummy = new AdjList();
		UtilityFunctions u = new UtilityFunctions();
		HashMap<String, String> stringToId = u.createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> s = u.readEmbeddingsFromFile(embedding,stringToId,dummy);
		
		ReadSubgraph rr = new ReadSubgraph();
		AdjList aa = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
		System.out.println("read wikipedia");
		HashMap<String, ArrayList<String>> address = te.getAddress("/home/prajna/workspace/TutorialBank/data/topics_to_resources.csv");
  		HashMap<String, String> descriptions = te.readFile1("/mnt/dell/prajna/allwikipedia/all_text_1_tkb");
  		
		Aspect a = new Aspect();
		AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        HashSet<String> tagged_entities = new HashSet<String>();
		for(int entity_num : a1.undirectedGraph.nodes())
        {
			String entity = a1.nodeMap.get(entity_num);
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        Date firstDate1 = new Date();
		System.out.println(dateFormat.format(firstDate1));
		HashMap<String, Double> pairwise_scores = new HashMap<String, Double>();
		
	 	   String tot_abstract = StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(extractAbstract(query,k, galago_port),"<lb>",""),"\"",""),"'",""),"’",""),"“","");
	       
	 	  if(address.get(query)!=null)
	  		{
	  			String tot_line="";
	  			for(String ad:address.get(query))
	  			{
	      			BufferedReader br = new BufferedReader(new FileReader(ad));
	      			String line;
	      			while((line=br.readLine())!=null)
	      			{
	      				tot_line = tot_line + " . "+line;
	      			}
	      			if(descriptions.get(query)!=null)
	      			{
	      				descriptions.put(query, descriptions.get(query)+" . "+tot_line);
	      			}
	      			br.close();
	  			}
	  		}
	 	 Date firstDate2 = new Date();
			System.out.println("extracted abstract "+dateFormat.format(firstDate2));
			
	 	  tot_abstract = tot_abstract + " "+descriptions.get(query);
	 	   
	 	   
	 	   Result r = extractPhrases(tot_abstract) ;
	       System.out.println("extracted phrases");
	       Date firstDate3 = new Date();
			System.out.println("extracted phrases "+dateFormat.format(firstDate3));
		
	       //HashMap<String, ArrayList<String>> phrases = new HashMap<String, ArrayList<String>>();
	       HashMap<String, Collection<Emit>> entities_in_phrase = new HashMap<String, Collection<Emit>>();
	 	   for(int i=0;i<r.getFullKeywords().length;i++)
	 	   {
	 		   //System.out.println(r.getFullKeywords()[i]);
	 		   if(r.getScores()[i]<5) continue;
	 		   if(r.getFullKeywords()[i].split(" ").length>5) continue;
	 		  // System.out.println(r.getFullKeywords()[i]);
	 		  
	 		   Collection<Emit> named_entity_occurences = trie.parseText(r.getFullKeywords()[i].toLowerCase());
	 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
	 		   for(Emit e:named_entity_occurences)
	            {
	 			   String ent = e.getKeyword().replace(" ", "_");
	           	 if(e.getKeyword().length()<=4) continue;
	           	 if(pairwise_scores.get(ent)==null)
	    			 pairwise_scores.put(ent, 1.0);
	    			 else
	    			 {
	    				 pairwise_scores.put(ent, pairwise_scores.get(ent)+1.0);
	    			 }
	           	 tagged_entities.add(e.getKeyword());
	            }
	 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
	 	   }
	 	  Date firstDate4 = new Date();
			System.out.println("tagged entities in phrases "+dateFormat.format(firstDate4));
		
	 	   System.out.println("size of phrases "+entities_in_phrase.size());
			System.out.println("size of tagged entities: "+pairwise_scores.size());

			BufferedReader brr = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopics/"+query+"/mixture/refd_mixture"));
		 	   String line1;
				HashMap<String, Integer> ent_index = new HashMap<String, Integer>();
		 	   HashMap<String, Double> ent_val = new HashMap<String, Double>();
		 	   ArrayList<Double> prob_dist_query_list = new ArrayList<Double>();
		 	   int count=0;
		 	   while((line1=brr.readLine())!=null)
		 	   {
		 		   String[] arr = line1.split("\t");
		 		   ent_index.put(arr[0], count);
		 		   ent_val.put(arr[0], Double.parseDouble(arr[1]));
		 		   count++;
		 	   }
		 	   double prob_dist_query[] = new double[ent_index.size()];
		 	   for(String e:ent_index.keySet())
		 	   {
		 		   prob_dist_query[ent_index.get(e)]=ent_val.get(e);
		 	   }
		
		/*HashMap<String, ArrayList<String>> clustersWithTags = new HashMap<String, ArrayList<String>>();
		brr = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/clusters_with_tags/clusters_with_tags"));
		while((line1=brr.readLine())!=null)
		{
			String arr[] = line1.split("\t");
			if(clustersWithTags.get(arr[0])==null)
			{
				ArrayList<String> clusters = new ArrayList<String>();
				clusters.add(arr[1]);
				clustersWithTags.put(arr[0], clusters);
			}
			else
			{
				ArrayList<String> clusters = clustersWithTags.get(arr[0]);
				clusters.add(arr[1]);
				clustersWithTags.put(arr[0], clusters);
			}
		}*/
		
		HashMap<String, Double> subtopicscores = new HashMap<String, Double>();

		  for(String t:pairwise_scores.keySet()) 
		  { 
			  double ref = aa.referenceDistance(query, t);
			  if(ref>0) subtopicscores.put(t,ref*pairwise_scores.get(t));
		  }
		  for(String t:subtopicscores.keySet())
		  {
			  Collection<Emit> named_entity_occurences = trie.parseText(t.toLowerCase().replace("_", " "));
	 		  
			  entities_in_phrase.put(t.toLowerCase().replace("_", " "), named_entity_occurences);
	 		  
		  }
		  Date firstDate5 = new Date();
		  System.out.println("computed refd scores for tagged entities in phrases "+dateFormat.format(firstDate5));
		
		 
		  HashMap<String, HashSet<String>> features = computeFeatureString(entities_in_phrase, a1);
		  
		  Date firstDate6 = new Date();
		  System.out.println("computed features "+dateFormat.format(firstDate6));
			
		
		  BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/hierarchical_cluster/demo/"+query+"/results"));
		  
		  Maths m = new Maths();
		  ArrayList<Entry<String, Double>> subtopic_sorted = new ArrayList<Entry<String, Double>>(subtopicscores.entrySet());
		  Collections.sort(subtopic_sorted, LanguageModelEntity.valueComparator);;
		  HashMap<Set<String>, Double> kl_div_query = new HashMap<Set<String>, Double>();
		  
		  HashMap<Set<String>, double[]> lm_rep = new HashMap<Set<String>, double[]>();

		  for(Entry e:subtopic_sorted)
		  {
			  Set<String> neighs = checkExtractNeighbor(((String)e.getKey()).toLowerCase().replace("_"," "), features,mindistance);

			  //System.out.println("computing kl divergence for: "+e.getKey());
			 // Set<String> neighs = chooseNeighborsStored(((String)e.getKey()).toLowerCase().replace("_"," "), clustersWithTags);
			  if(neighs==null) 
			  {
				  System.out.println(" null for "+e.getKey());
				  continue;
			  }
			  //System.out.println("not null for "+e.getKey());
			  double[] lm = computeLMSet(neighs,ent_index);
			  lm_rep.put(neighs, lm);
			  double kl_div = m.klDivergence(prob_dist_query,lm);
			  kl_div_query.put(neighs, kl_div);
		  }
		  HashSet<Set<String>> remaining_phrases = new HashSet<Set<String>>(lm_rep.keySet());

		  ArrayList<Entry<Set<String>,Double>> sorted_kl_divergences = new ArrayList<Entry<Set<String>,Double>>(kl_div_query.entrySet());
		  Entry<Set<String>,Double> ee = Collections.min(sorted_kl_divergences,LanguageModelEntity.valueComparatorSetIncreasing);		  
		  int requested=10;
		  HashMap<Set<String>,Double> finalFacets = new HashMap<Set<String>,Double>();
			for(int i=0;i<requested;i++)
			   {
				System.out.println(i);
				   if(i==0)
				   {
					   //Entry<String, Double> ee = sorted_kl_divergences.get(0);
					   //String p = (String) ee.getKey();
					   Set<String> p = ee.getKey();
					  // ArrayList<Entry<String,Double>> entities = findPrerequisitesAndRank(p,trie);

					  // bw.write(entities+"\t"+ee.getValue()+"\n\n\n");
					   //System.out.println(p+"\t"+ee.getValue());
					   //HashSet<String> neighs = new HashSet<String>(p);
					   finalFacets.put(p, ee.getValue());
					   remaining_phrases.remove(p);
					   
					   //System.out.println(p.toString());
				   }
				   else
				   {
					   double[] prob_dist_S = computeLM(finalFacets, ent_index);
					   //double kl_div = m.klDivergence(prob_dist_query,prob_dist_S);
					   HashMap<Set<String>, Double> remaining_phrases_div = new HashMap<Set<String>, Double>();
					   for(Set<String> p:remaining_phrases)
					   {
						   double[] lm = lm_rep.get(p);
						   double kl_div = m.klDivergence(prob_dist_S,lm);
						   remaining_phrases_div.put(p,kl_div_query.get(p)/(p.size()*kl_div));
					   }
					   ArrayList<Entry<Set<String>, Double>> remaining_phrases_sorted = new ArrayList<Entry<Set<String>, Double>>(remaining_phrases_div.entrySet());
					   Entry ee1 = Collections.min(remaining_phrases_sorted, LanguageModelEntity.valueComparatorSetIncreasing);
					   Set<String> p = (Set<String>) ee1.getKey();
					   finalFacets.put(p, (double)ee1.getValue());
					   //ArrayList<Entry<String,Double>> entities = findPrerequisitesAndRank(p,trie);
					   //bw.write(entities+"\t"+ee1.getValue()+"\n\n\n");
					   //System.out.println(p+"\t"+ee1.getValue());
					  remaining_phrases.remove(p);
					  
				   }
			   }
		//	bw.write("\n");
		 // ArrayList<Entry<Set<String>,Double>> ee = new ArrayList<Entry<Set<String>,Double>>(kl_div_query.entrySet());
		 // Collections.sort(ee,LanguageModelEntity.valueComparatorSetIncreasing);
		  bw.close();
		  Date secondDate = new Date();
			System.out.println("over: "+dateFormat.format(secondDate));
		    long diffInMillies = Math.abs(secondDate.getTime() - firstDate1.getTime());
		    System.out.println(diffInMillies);
	}
	
	public  static Set<String> chooseNeighborsStored(String topic, HashMap<String, ArrayList<String>> clustersWithTags) 
	{
		if(clustersWithTags.get(topic)!=null)
		// TODO Auto-generated method stub
			return new HashSet<String>(clustersWithTags.get(topic));
		return null;
	}

	//testing if star clustering entities tagged from abstracts work
	
	public static void testRanking2(String query, String teknowbase_mapped, String nodemap, String relmap, double mindistance, int k, double lambda, String entityMap, String embedding, String wikipedia) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate = new Date();
		System.out.println(dateFormat.format(firstDate));
		AdjList dummy = new AdjList();
		UtilityFunctions u = new UtilityFunctions();
		HashMap<String, String> stringToId = u.createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> s = u.readEmbeddingsFromFile(embedding,stringToId,dummy);
		
		ReadSubgraph rr = new ReadSubgraph();
		AdjList aa = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
		System.out.println("read wikipedia");
		Aspect a = new Aspect();
		AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(int entity_num : a1.labeledGraph.nodes())
        {
			String entity = a1.nodeMap.get(entity_num);
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        Date firstDate1 = new Date();
		System.out.println(dateFormat.format(firstDate1));
		HashMap<String, Double> pairwise_scores = extractAbstractAndTagEntities(query, k, trie);
		
		
 	   System.out.println("size of tagged entities: "+pairwise_scores.size());
 	  	HashMap<String, Double> refdScores = new HashMap<String, Double>();

		  for(String t:pairwise_scores.keySet()) 
		  { 
			  double ref = aa.referenceDistance(query, t);
			  if(ref>0) refdScores.put(t,ref);
		  }
		  //System.out.println(aa.referenceDistance(query, t));
		  //System.out.println("keyword is: "+t); }
		  MutableValueGraph<String, Integer> weightedGraph = ValueGraphBuilder.directed().allowsSelfLoops(true).build();
	 	   
		 
		for(String n:refdScores.keySet())
 	    {
 	    	if(a1.nodeMap1.get(n)==null) continue;
 	    	int e_code = a1.nodeMap1.get(n);
 	    	if(!a1.labeledGraph.nodes().contains(e_code)) continue;
 	    	for(int neigh:a1.labeledGraph.adjacentNodes(e_code))
 	    	{
 	    		String neigh_string = a1.nodeMap.get(neigh);
 	    		Optional c1 = a1.labeledGraph.edgeValue(e_code, neigh);
 	    		if(refdScores.get(neigh_string)!=null)
 	    		{
 	    			if(c1.isPresent())
 	    			{
 	    				ArrayList<Integer> c = (ArrayList<Integer>) c1.get();
 	    				for(int cc:(ArrayList<Integer>) c)
 	    				{
 	    					if(a1.relmap1.get(cc)!=null)
 	    					{
 	    						String rel = a1.relmap1.get(cc);
 	    						if(rel.contains("_inverse"))
 	    						{
 	    							continue;
 	    						}
 	    						else
 	    						{
 	    							weightedGraph.putEdgeValue(n, neigh_string, 1);
 	    						}
 	    					}
 	    				}
 	    			}
 	    		}
 	    		
 	    	}
 	    	
 	    }
 	    HashMap<String, Double> h1 = new HashMap<String, Double>();
 	    for(String n:weightedGraph.nodes())
 	    {
 	    	h1.put(n, (double) weightedGraph.adjacentNodes(n).size());
 	    }
 	   ArrayList<Entry<String, Double>> en = new ArrayList<Entry<String, Double>>(h1.entrySet());
	   Collections.sort(en, LanguageModelEntity.valueComparator);
	  
	   ArrayList<ArrayList<String>> clusters = new ArrayList<ArrayList<String>>();
		HashSet<String> marked = new HashSet<String>();
		for(Entry<String, Double> e:en)
		{
			String a11 = e.getKey();
			int a1_code = a1.nodeMap1.get(a11);
			if(marked.contains(a11)) continue;
			marked.add(a11);
			ArrayList<String> list1 = new ArrayList<String>();
			list1.add(a11);
			if(!a1.labeledGraph.nodes().contains(a1_code)) continue;
			if(!weightedGraph.nodes().contains(a11)) continue;
				for(String adj:weightedGraph.adjacentNodes(a11))
				{
					
					if(marked.contains(adj)) continue;
					list1.add(adj);
					marked.add(adj);
				}
				clusters.add(list1);
		}
		System.out.println(clusters.toString());
		
		HashMap<ArrayList<String>, Double> clusters_sorted = new HashMap<ArrayList<String>, Double>();
		
		for(ArrayList<String> c:clusters)
		{
			double sum=0.0;
			/*for(String s1:c)
			{
				sum = sum + pairwise_scores.get(s1)*refdScores.get(s1);
			}*/
			clusters_sorted.put(c, sum);
			clusters_sorted.put(c, 1.0*c.size());
		}
		
		
		ArrayList<Entry<ArrayList<String>, Double>> ec_both = new ArrayList<Entry<ArrayList<String>, Double>>(clusters_sorted.entrySet());
		Collections.sort(ec_both, LanguageModelEntity.valueComparatorList);
		
		System.out.println(ec_both.toString());
		SimpleWebApp simple = new SimpleWebApp();
		BuildTree b = new BuildTree();
		for(Entry<ArrayList<String>,Double> ee:ec_both)
		{
			ArrayList<String> list1 = new ArrayList<String>(ee.getKey());
			ArrayList<String> list2 = new ArrayList<String>(ee.getKey());
			ArrayList<Entry<String, Double>> list1_sorted = new ArrayList<Entry<String,Double>>();
			for(String s1:list1)
			{
				MyEntry<String, Double> mm = simple.getEntry(s1,pairwise_scores.get(s1)*refdScores.get(s1));
				list1_sorted.add(mm);
			}
			
			Collections.sort(list1_sorted, LanguageModelEntity.valueComparator);
			Entry<String, Double> rep = list1_sorted.get(0);
			System.out.print("\n");
			for(Entry<String, Double> el:list1_sorted)
			{
				System.out.print(" ("+el.getKey()+", "+el.getValue()+" )");
			}
			System.out.print("\n");
			LanguageModelEntity lm = s.get(rep.getKey());
			String rep_s = rep.getKey();
			if(s.get(rep_s)==null) 
			{
				if(s.get(b.removeDisambiguation(rep_s))==null)
				{
					if(s.get(b.stem(b.removeDisambiguation(rep_s)))==null)
					{
						continue;
					}
					else
					{
						rep_s=s.get(b.stem(b.removeDisambiguation(rep_s))).getName();
						//s1.add(s.get(b.stem(b.removeDisambiguation(entity))));
					}
				}
				else
				{
					//s1.add(s.get(b.removeDisambiguation(entity)));
					rep_s=s.get(b.removeDisambiguation(rep_s)).getName();
				}
				
			}
			
			lm = s.get(rep_s);
			//System.out.println("rep is: "+rep_s+" and lm is "+lm);
			ArrayList<Entry<String, Double>> list2_sorted = new ArrayList<Entry<String,Double>>();
			for(String s1:list2)
			{
				String entity = s1;
				if(s.get(entity)==null) 
				{
					if(s.get(b.removeDisambiguation(entity))==null)
					{
						if(s.get(b.stem(b.removeDisambiguation(entity)))==null)
						{
							continue;
						}
						else
						{
							entity=s.get(b.stem(b.removeDisambiguation(entity))).getName();
							//s1.add(s.get(b.stem(b.removeDisambiguation(entity))));
						}
					}
					else
					{
						//s1.add(s.get(b.removeDisambiguation(entity)));
						entity=s.get(b.removeDisambiguation(entity)).getName();
					}
					
				}
				//s1=entity;
				
				if(entity.equals(rep_s)) continue;
				LanguageModelEntity lm1 = s.get(entity);
				//System.out.println("s1 is: "+entity+" and lm is "+lm1);

				if(lm==null || lm1==null) continue;
				double cosine_sim = lm.cosineSimilarity(lm1);
				//System.out.println(cosine_sim);
				
				MyEntry<String, Double> mm = simple.getEntry(s1,cosine_sim);
				list2_sorted.add(mm);
			}
			Collections.sort(list2_sorted, LanguageModelEntity.valueComparator);
			System.out.println("rep is: "+rep.getKey()+", refd score:"+rep.getValue());
			for(Entry<String, Double> ee1:list2_sorted)
			{
				System.out.print("("+ee1.getKey()+" "+ee1.getValue()+")\t");
			}
			//System.out.println(list2_sorted.toString());
		}
		
	   
	  
	   //System.out.println(entryset.size());
	   Date secondDate = new Date();
	   System.out.println("over: "+dateFormat.format(secondDate));
	    long diffInMillies = Math.abs(secondDate.getTime() - firstDate1.getTime());
	    System.out.println(diffInMillies);
	}
	
	public static void testRanking(String query, String teknowbase_mapped, String nodemap, String relmap, double mindistance, int k, double lambda, String galago_port) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate = new Date();
		System.out.println(dateFormat.format(firstDate));
		
		Aspect a = new Aspect();
		AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(int entity_num : a1.undirectedGraph.nodes())
        {
			String entity = a1.nodeMap.get(entity_num);
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        Date firstDate1 = new Date();
        BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/bad_entities"));
	     String line;
	     HashSet<String> bad_entities = new HashSet<String>();
	     while((line=br1.readLine())!=null)
	     {
	    	 bad_entities.add(line);
	     }
		System.out.println(dateFormat.format(firstDate1));
		
        HashSet<String> tagged_entities = new HashSet<String>();
 	   String tot_abstract = StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(extractAbstract(query,k,galago_port),"<lb>",""),"\"",""),"'",""),"’",""),"“","");
       Result r = extractPhrases(tot_abstract) ;
       System.out.println("extracted phrases");
       //HashMap<String, ArrayList<String>> phrases = new HashMap<String, ArrayList<String>>();
       HashMap<String, Collection<Emit>> entities_in_phrase = new HashMap<String, Collection<Emit>>();
 	   for(int i=0;i<r.getFullKeywords().length;i++)
 	   {
 		   //System.out.println(r.getFullKeywords()[i]);
 		   if(r.getScores()[i]<5) continue;
 		   if(r.getFullKeywords()[i].split(" ").length>5) continue;
 		  // System.out.println(r.getFullKeywords()[i]);
 		  
 		   Collection<Emit> named_entity_occurences = trie.parseText(r.getFullKeywords()[i].toLowerCase());
 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
 		   for(Emit e:named_entity_occurences)
            {
           	 if(e.getKeyword().length()<=4) continue;
           	 tagged_entities.add(e.getKeyword());
            }
 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
 	   }
 	   System.out.println("size of tagged entities: "+tagged_entities.size());
 	   System.out.println("size of phrases "+entities_in_phrase.size());
		HashMap<String, double[]> features = computeFeature(entities_in_phrase, a1);
		System.out.println("computed features");
		//HashMap<String, HashMap<String, Double>> pairwise_sim = computePairwiseSim(features);
		System.out.println("computed pairwise sim");
		
		BufferedReader brr = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopics/"+query+"/mixture/refd_mixture"));
 	   String line1;
 	   HashMap<String, Integer> ent_index = new HashMap<String, Integer>();
 	   HashMap<String, Double> ent_val = new HashMap<String, Double>();
 	   ArrayList<Double> prob_dist_query_list = new ArrayList<Double>();
 	   int count=0;
 	   while((line1=brr.readLine())!=null)
 	   {
 		   String[] arr = line1.split("\t");
 		   ent_index.put(arr[0], count);
 		   ent_val.put(arr[0], Double.parseDouble(arr[1]));
 		   count++;
 	   }
 	   double prob_dist_query[] = new double[ent_index.size()];
 	   for(String e:ent_index.keySet())
 	   {
 		   prob_dist_query[ent_index.get(e)]=ent_val.get(e);
 	   }
		HashMap<String, double[]> lm_rep = new HashMap<String, double[]>();

		for(String p:entities_in_phrase.keySet())
		{
			double[] lm = computeLM(p,ent_index);
			lm_rep.put(p, lm);
		}
 	   
 		Maths m = new Maths();
	 	HashMap<String, Double> hh = new HashMap<String,Double>();
	   	//System.out.println("candidates size is: "+candidates.size());
	 	for(String s1:entities_in_phrase.keySet())
	   	{
		   double[] lm = computeLM(s1, ent_index);
		   double kl_div = m.klDivergence(prob_dist_query,lm);
		   hh.put(s1, kl_div);
		   //System.out.println("computed KL divergence for set, value is: "+kl_div);
		   
	   	}
	   ArrayList<Entry<String, Double>> entryset1 = new ArrayList<Entry<String, Double>>(hh.entrySet());
	   Entry ee = Collections.min(entryset1, LanguageModelEntity.valueComparatorIncreasing);
	   int requested=10;
	   HashMap<Set<String>, Double> finalFacets = new HashMap<Set<String>, Double>();
	   HashSet<String> remaining_phrases = new HashSet<String>(entities_in_phrase.keySet());
	   for(int i=0;i<requested;i++)
	   {
		   if(i==0)
		   {
			   String p = (String) ee.getKey();
			   System.out.println(p+"\t"+ee.getValue());
			   Set<String> neighs = chooseNeighbors(p, features, mindistance);
			   //neighs.add(p);
			   finalFacets.put(neighs, 10.0);
			   remaining_phrases.removeAll(neighs);
			  System.out.println(neighs.toString());
		   }
		   else
		   {
			   double[] prob_dist_S = computeLM(finalFacets, ent_index);
			   //double kl_div = m.klDivergence(prob_dist_query,prob_dist_S);
			   HashMap<String, Double> remaining_phrases_div = new HashMap<String, Double>();
			   for(String p:remaining_phrases)
			   {
				   double[] lm = lm_rep.get(p);
				   double kl_div = m.klDivergence(prob_dist_S,lm);
				   remaining_phrases_div.put(p, lambda*hh.get(p)-(1-lambda)*kl_div);
			   }
			   ArrayList<Entry<String, Double>> remaining_phrases_sorted = new ArrayList<Entry<String, Double>>(remaining_phrases_div.entrySet());
			   Entry ee1 = Collections.min(remaining_phrases_sorted, LanguageModelEntity.valueComparatorIncreasing);
			   String p = (String) ee1.getKey();
			  System.out.println(p+"\t"+ee1.getValue());
			   Set<String> neighs = chooseNeighbors(p, features, mindistance);
			   finalFacets.put(neighs, 10.0);
			   remaining_phrases.removeAll(neighs);
			  System.out.println(neighs.toString());
		   }
	   }
	   ArrayList<Entry<Set<String>, Double>> entryset = new ArrayList<Entry<Set<String>,Double>>(finalFacets.entrySet());
	   
	   for(Entry e:entryset)
	   {
		   //System.out.println(e.getKey()+"\t"+e.getValue());
		   ArrayList<Entry<String, Double>> es1 = findPrerequisitesAndRank((Set<String>) e.getKey(),trie,bad_entities);
		   System.out.println(es1.toString()+"\n");
	   }
	   //System.out.println(entryset.size());
	   Date secondDate = new Date();
	   System.out.println("over: "+dateFormat.format(secondDate));
	    long diffInMillies = Math.abs(secondDate.getTime() - firstDate1.getTime());
	    System.out.println(diffInMillies);
	}
	
	
	public static void testRanking1(String query, String teknowbase_mapped, String nodemap, String relmap, double mindistance, int k, double lambda,String galago_port) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate = new Date();
		System.out.println(dateFormat.format(firstDate));
		
		Aspect a = new Aspect();
		AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(int entity_num : a1.undirectedGraph.nodes())
        {
			String entity = a1.nodeMap.get(entity_num);
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        Date firstDate1 = new Date();
		System.out.println(dateFormat.format(firstDate1));
		
        HashSet<String> tagged_entities = new HashSet<String>();
 	   String tot_abstract = StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(extractAbstract(query,k,galago_port),"<lb>",""),"\"",""),"'",""),"’",""),"“","");
       Result r = extractPhrases(tot_abstract) ;
       System.out.println("extracted phrases");
       //HashMap<String, ArrayList<String>> phrases = new HashMap<String, ArrayList<String>>();
       HashMap<String, Collection<Emit>> entities_in_phrase = new HashMap<String, Collection<Emit>>();
 	   for(int i=0;i<r.getFullKeywords().length;i++)
 	   {
 		   //System.out.println(r.getFullKeywords()[i]);
 		   if(r.getScores()[i]<5) continue;
 		   if(r.getFullKeywords()[i].split(" ").length>5) continue;
 		  // System.out.println(r.getFullKeywords()[i]);
 		  
 		   Collection<Emit> named_entity_occurences = trie.parseText(r.getFullKeywords()[i].toLowerCase());
 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
 		   for(Emit e:named_entity_occurences)
            {
           	 if(e.getKeyword().length()<=4) continue;
           	 tagged_entities.add(e.getKeyword());
            }
 		   entities_in_phrase.put(r.getFullKeywords()[i], named_entity_occurences);
 	   }
 	   System.out.println("size of tagged entities: "+tagged_entities.size());
 	   System.out.println("size of phrases "+entities_in_phrase.size());
		HashMap<String, double[]> features = computeFeature(entities_in_phrase, a1);
		System.out.println("computed features");
		//HashMap<String, HashMap<String, Double>> pairwise_sim = computePairwiseSim(features);
		System.out.println("computed pairwise sim");
		
		BufferedReader brr = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/subtopics/"+query+"/mixture/refd_mixture"));
 	   String line1;
 	   HashMap<String, Integer> ent_index = new HashMap<String, Integer>();
 	   HashMap<String, Double> ent_val = new HashMap<String, Double>();
 	   ArrayList<Double> prob_dist_query_list = new ArrayList<Double>();
 	   int count=0;
 	   while((line1=brr.readLine())!=null)
 	   {
 		   String[] arr = line1.split("\t");
 		   ent_index.put(arr[0], count);
 		   ent_val.put(arr[0], Double.parseDouble(arr[1]));
 		   count++;
 	   }
 	   double prob_dist_query[] = new double[ent_index.size()];
 	   for(String e:ent_index.keySet())
 	   {
 		   prob_dist_query[ent_index.get(e)]=ent_val.get(e);
 	   }
 	   
 	   HashSet<Feature> hf = new HashSet<Feature>();
 	   SimpleWebApp simple = new SimpleWebApp();
 	   for(String p:features.keySet())
 	   {
 		   Feature f1 = simple.getFeature(p,features.get(p));
 		   hf.add(f1);
 	   }
 	  Date firstDate_1 = new Date();
		System.out.println(dateFormat.format(firstDate_1));
 	  Set<Set<Feature>> candidates = clusterHierarchical(hf, new BuildTree(),mindistance);
 	 dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
 	Date firstDate_2 = new Date();
	System.out.println(dateFormat.format(firstDate_2));
 	  System.out.println("clustered hierarchically");
 	  HashMap<String, double[]> lm_rep = new HashMap<String, double[]>();

		for(String p:entities_in_phrase.keySet())
		{
			double[] lm = computeLM(p,ent_index);
			lm_rep.put(p, lm);
		}
 	   
 		Maths m = new Maths();
	 	HashMap<String, Double> hh = new HashMap<String,Double>();
	   	//System.out.println("candidates size is: "+candidates.size());
	 	for(String s1:entities_in_phrase.keySet())
	   	{
		   double[] lm = computeLM(s1, ent_index);
		   double kl_div = m.klDivergence(prob_dist_query,lm);
		   hh.put(s1, kl_div);
		   //System.out.println("computed KL divergence for set, value is: "+kl_div);
		   
	   	}
	   ArrayList<Entry<String, Double>> entryset1 = new ArrayList<Entry<String, Double>>(hh.entrySet());
	   Entry ee = Collections.min(entryset1, LanguageModelEntity.valueComparatorIncreasing);
	   int requested=10;
	   HashMap<Set<String>, Double> finalFacets = new HashMap<Set<String>, Double>();
	   HashSet<String> remaining_phrases = new HashSet<String>(entities_in_phrase.keySet());
	   for(int i=0;i<requested;i++)
	   {
		   if(i==0)
		   {
			   String p = (String) ee.getKey();
			   System.out.println(p+"\t"+ee.getValue());
			   Set<String> neighs = chooseNeighbors(p, features, mindistance);
			   //neighs.add(p);
			   finalFacets.put(neighs, 10.0);
			   remaining_phrases.removeAll(neighs);
			 // System.out.println(neighs.toString());
		   }
		   else
		   {
			   double[] prob_dist_S = computeLM(finalFacets, ent_index);
			   //double kl_div = m.klDivergence(prob_dist_query,prob_dist_S);
			   HashMap<String, Double> remaining_phrases_div = new HashMap<String, Double>();
			   for(String p:remaining_phrases)
			   {
				   double[] lm = lm_rep.get(p);
				   double kl_div = m.klDivergence(prob_dist_S,lm);
				   remaining_phrases_div.put(p, lambda*hh.get(p)-(1-lambda)*kl_div);
			   }
			   ArrayList<Entry<String, Double>> remaining_phrases_sorted = new ArrayList<Entry<String, Double>>(remaining_phrases_div.entrySet());
			   Entry ee1 = Collections.min(remaining_phrases_sorted, LanguageModelEntity.valueComparatorIncreasing);
			   String p = (String) ee1.getKey();
			  System.out.println(p+"\t"+ee1.getValue());
			   Set<String> neighs = chooseNeighbors(p, features, mindistance);
			   finalFacets.put(neighs, 10.0);
			   remaining_phrases.removeAll(neighs);
			  //System.out.println(neighs.toString());
		   }
	   }
	   ArrayList<Entry<Set<String>, Double>> entryset = new ArrayList<Entry<Set<String>,Double>>(finalFacets.entrySet());
	   
	   for(Entry e:entryset)
	   {
		   System.out.println(e.getKey()+"\t"+e.getValue());
	   }
	   //System.out.println(entryset.size());
	   Date secondDate = new Date();
		System.out.println("over: "+dateFormat.format(secondDate));
	    long diffInMillies = Math.abs(secondDate.getTime() - firstDate1.getTime());
	    System.out.println(diffInMillies);
	}
	
	public static void generateJSON(String q) throws Exception
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(q));
		
	}
	
	public static void convertToJSON(String root, String q, HashMap<String, ArrayList<String>> children, BufferedWriter bw, boolean last, HashMap<String,String> descriptions, HashMap<String,HashSet<String>> support_sentences) throws Exception
	{
		String desc = "";
		if(root.equals(q))
		{
			
			desc="";
			
		}
		else if(support_sentences.get(q)!=null)
		{
			for(String d:support_sentences.get(q))
			{
				desc=desc + " "+d;
				//break;
			}
			desc = desc.replace('"', ' ');
		}
		
		bw.write(" {\n" + 
				"		\"nodeName\" : \""+q.replace("_", " ")+"\",\n" + 
				"		\"name\" : \""+q.replace("_", " ")+"\",\n" + 
				"		\"type\" : \"type3\",\n" + 
				"       \"label\" : \""+desc+"\",\n "+
				"		\n" + 
				"		\n" + 
				"		\"link\" : {\n" + 
				"				\"direction\" : \"ASYN\"\n" + 
				"			},\n" + 
				"		\"children\" : [");
		if(children.get(q)!=null)
		{
			int count=0;
			for(String q1:children.get(q))
			{
				if(count==children.get(q).size()-1)
				{
					convertToJSON(root,q1,children,bw,true,descriptions,support_sentences);
				}
				else
				{
					convertToJSON(root,q1,children,bw,false,descriptions,support_sentences);
				}
				
				count++;
			}
		}
		if(last)
		bw.write("]}");
		else
		bw.write("]},");
	}
	
	public static void convertToJSONMaster(String q,  HashMap<String, String> pred, String file_location, HashMap<String,String> descriptions, HashMap<String, HashSet<String>> support_sentences) throws Exception
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(file_location));
		HashMap<String, ArrayList<String>> children = new HashMap<String, ArrayList<String>>();
		for(String n:pred.keySet())
		{
			if(pred.get(n)!=null)
			{
				String parent = pred.get(n);
				if(children.get(parent)==null)
				{
					ArrayList<String> list = new ArrayList<String>();
					list.add(n);
					children.put(parent, list);
				}
				else
				{
					ArrayList<String> list = children.get(parent);
					list.add(n);
					children.put(parent, list);
				}
			}
		}
		bw.write("{\n \"tree\":  ");
		convertToJSON(q,q,children,bw,true,descriptions,support_sentences);
		bw.write("\n}");
		bw.close();
		
	}
	
	
	
	
	public static void computeRefDAndGenerateJSON(String q, AdjList a, double thresh, HashMap<String, LanguageModelEntity> s,double sim, HashMap<String,Double> refdScores, HashMap<String,String> descriptions, HashMap<String, HashSet<String>> support_sentences) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate = new Date();
		System.out.println(dateFormat.format(firstDate));
		
		LanguageModelEntity lm = s.get(q);
		HashMap<String,String> pred = new HashMap<String,String>();
		
		HashMap<String, ArrayList<Edge>> children = new HashMap<String, ArrayList<Edge>>();
		ArrayList<Edge> ee1 = a.getAdjList().get(q);
		ArrayList<Edge> newlist1 = new ArrayList<Edge>();
		for(Edge ee:a.getAdjList().get(q))
		{
			if(ee.getName().equals(q)) continue;
			LanguageModelEntity lm1 = s.get(ee.getName());
			
			if(lm!=null)
			{
				if(lm1==null) 
				{
					if(refdScores.get(ee.getName())==null) continue;
					double ref = refdScores.get(ee.getName());
					if(ref>=thresh)
					{
						newlist1.add(ee);
						pred.put(ee.getName(), q);
					}
				}
				if(lm.cosineSimilarity(lm1)<sim) continue;
			}
			if(refdScores.get(ee.getName())==null) continue;
			double ref = refdScores.get(ee.getName());
			if(ref>=thresh)
			{
				newlist1.add(ee);
				pred.put(ee.getName(), q);
			}
			
			
		}
		//children.put(q, newlist1);
		for(Edge e:newlist1)
		{
			if(a.getAdjList().get(e.getName())==null) continue;
			else
			{
				if(e.getName().equals(q)) continue;
				
				for(Edge ee:a.getAdjList().get(e.getName()))
				{
					LanguageModelEntity lm1 = s.get(ee.getName());
					
					if(lm!=null)
					{
						if(lm1==null) continue;
						if(lm.cosineSimilarity(lm1)<sim) continue;
					}
					if(refdScores.get(ee.getName())==null) continue;
					double ref = refdScores.get(ee.getName());
					if(ref>=thresh)
					{
					if(ee.getName().equals(q)) continue;
					if(pred.get(ee.getName())==null)
					{
						pred.put(ee.getName(), e.getName());
					}
					}
				}
				//children.put(e.getName(), newlist);
				
			}
		}
		
		/*for(String c:candidates)
		{
			LanguageModelEntity lm1 = s.get(c);
			
			if(lm!=null)
			{
				if(lm1==null) continue;
				if(lm.cosineSimilarity(lm1)<sim) continue;
			}
			double ref = refdScores.get(c);
			if(ref>=thresh)
			{
				Edge ee =new Edge(0,c);
				onehop.add(ee);
				c1.add(c);
			}
		}*/
		//AdjList aa = new AdjList(children);
		
		//HashMap<String, String> pred = new HashMap<String, String>();
		//LinkedList<String> visited = new LinkedList<String>();
		//visited.add(q);
		//aa.dfs(q, visited, pred);
		
		System.out.println(pred);
		JSONObject j = new JSONObject(pred);
		
		System.out.println("here is the string: "+j.toString());
		
		Date seconddate = new Date();
		System.out.println(dateFormat.format(seconddate));
		
		long diffInMillies = Math.abs(seconddate.getTime() - firstDate.getTime());
	    System.out.println(diffInMillies);
	    
	    convertToJSONMaster(q,pred,"/var/www/preface/json_files/"+q+"_"+sim+"_"+thresh+".json",descriptions,support_sentences);
	}
	
	public static void computeRefDAndGenerateJSONRefD(String q, AdjList a, double thresh, HashMap<String, LanguageModelEntity> s,double sim, HashMap<String,Double> refdScores, HashMap<String,String> descriptions, HashMap<String, HashSet<String>> support_sentences) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date firstDate = new Date();
		System.out.println(dateFormat.format(firstDate));
		
		LanguageModelEntity lm = s.get(q);
		
		/*HashSet<String> cand = new HashSet<String>();
		ArrayList<Edge> ee1 = a.getAdjList().get(q);
		cand.addAll(a.getListInString(ee1));
		for(Edge e:ee1)
		{
			if(a.getAdjList().get(e.getName())==null) continue;
			cand.addAll(a.getListInString(a.getAdjList().get(e.getName())));
		}*/
		ArrayList<String> candidates = new ArrayList<String>(refdScores.keySet());
		
		ArrayList<String> c1 = new ArrayList<String>();
		HashMap<String, ArrayList<Edge>> children = new HashMap<String, ArrayList<Edge>>();
		ArrayList<Edge> onehop = new ArrayList<Edge>();
		for(String c:candidates)
		{
			LanguageModelEntity lm1 = s.get(c);
			
			if(lm!=null)
			{
				if(lm1==null) continue;
				if(lm.cosineSimilarity(lm1)<sim) continue;
			}
			double ref = refdScores.get(c);
			if(ref>=thresh)
			{
				Edge ee =new Edge(0,c);
				onehop.add(ee);
				c1.add(c);
			}
		}
		System.out.println("ref 1 hop is: "+c1);
		children.put(q,onehop);
		for(int i=0;i<c1.size()-1;i++)
		{
			for(int j=i+1;j<c1.size();j++)
			{
				double ref = a.referenceDistance(c1.get(i), c1.get(j));
				System.out.println(c1.get(i)+"\t"+c1.get(j)+"\t"+ref+"\n");
				if(ref>=thresh)
				{
					if(children.get(c1.get(i))==null)
					{
						ArrayList<Edge> childrenlist = new ArrayList<Edge>();
						Edge ee = new Edge(0,c1.get(j));
						childrenlist.add(ee);
						children.put(c1.get(i), childrenlist);
					}
					else
					{
						ArrayList<Edge> childrenlist = children.get(c1.get(i));
						Edge ee = new Edge(0,c1.get(j));
						childrenlist.add(ee);
						children.put(c1.get(i), childrenlist);
					}
				}
			}
		}
		AdjList aa = new AdjList(children);
		HashMap<String, String> pred = new HashMap<String, String>();
		LinkedList<String> visited = new LinkedList<String>();
		//visited.add(q);
		aa.dfs(q, visited, pred);
		
		System.out.println(pred);
		JSONObject j = new JSONObject(pred);
		
		System.out.println("here is the string: "+j.toString());
		
		Date seconddate = new Date();
		System.out.println(dateFormat.format(seconddate));
		
		long diffInMillies = Math.abs(seconddate.getTime() - firstDate.getTime());
	    System.out.println(diffInMillies);
	    
	    convertToJSONMaster(q,pred,"/var/www/preface/json_files/"+q+"_"+sim+"_"+thresh+".json",descriptions,support_sentences);
	}
	
	public static ChannelSftp setupJsch(int remote_port_number) throws Exception
	{
		JSch jsch = new JSch();
  		jsch.setKnownHosts("/home/prajna/.ssh/known_hosts");
  		Session jschSession = jsch.getSession("root", "10.17.50.132",remote_port_number);
  	    jschSession.setPassword("welcome@123");
  	    jschSession.setConfig("StrictHostKeyChecking", "no");
  	    jschSession.connect();
  	    return (ChannelSftp) jschSession.openChannel("sftp");
  		
	}
	
	public static void testSftp(int port, String hostname, String username, String password, String file, String remotedir) throws Exception
	{
		 //String hostname = "10.17.50.132", username = "root", password = "welcome@123", file = "/var/www/preface/json_files/latent_dirichlet_allocation_0.5_0.08.json", remotedir = "/var/www/app/prerequisites/json_files/";
	      //  int port = 32783;
	        JSch jsch = new JSch();
	        Session session = null;
	        session = jsch.getSession(username, hostname, port);
	        session.setPassword(password);
	        session.setConfig("StrictHostKeyChecking", "no");
	            session.connect();
	        ChannelSftp channel = null;
	        channel = (ChannelSftp)session.openChannel("sftp");
	        channel.connect();
	            File localFile = new File(file);
	            //If you want you can change the directory using the following line.
	            channel.cd(remotedir);
	        channel.put(new FileInputStream(localFile),localFile.getName());
	            channel.disconnect();
	        session.disconnect();
		
		/*ChannelSftp channelSftp = setupJsch(remote_port_number);
	    channelSftp.connect();
	    
	    ChannelSftp channel = null;
        channel = (ChannelSftp)session.openChannel("sftp");
        channel.connect();
            File localFile = new File(file);
            //If you want you can change the directory using the following line.
            channel.cd(remotedir);
        channel.put(new FileInputStream(localFile),localFile.getName());
            channel.disconnect();
        session.disconnect();
	 
	    String localFile = "/var/www/preface/json_files/latent_dirichlet_allocation_0.5_0.08.json";
	    String remoteDir = "/var/www/app/prerequisites/json_files/";
	    System.out.println("before copy");
	    channelSftp.put(localFile, remoteDir + "jschFile.txt");
	    System.out.println("after copy");*/

	 
	    //channelSftp.exit();
	}

	
	public static void preface(String args[], String wikipedia, String wikipedia_first_para, String entityMap, String embedding, String teknowbase_mapped, String nodemap, String relmap, String urlPattern, String log, String port_, String remote_port_number,String galago_port) throws Exception
	{
		System.out.println("updated working");
		System.out.println(args);
    	
		TagEntities te = new TagEntities();
	    Aspect a = new Aspect();
	    AdjList dummy = new AdjList();
	    int port = Integer.parseInt(port_);
	   // Parameters parameters = Arguments.parse(args);
	   // System.out.println(parameters);
       // Retrieval retrieval = RetrievalFactory.create(parameters);
        
        ReadSubgraph rr = new ReadSubgraph();
		AdjList aa = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
		AdjList aa_firstpara = rr.readFromFile(new BufferedReader(new FileReader(wikipedia_first_para)));
		UtilityFunctions u = new UtilityFunctions();
		HashMap<String, String> stringToId = u.createStringToIdMapping(entityMap);
		HashMap<String, LanguageModelEntity> s = u.readEmbeddingsFromFile(embedding,stringToId,dummy);
		

  		AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(int entity_num : a1.undirectedGraph.nodes())
        {
			String entity = a1.nodeMap.get(entity_num);
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
		BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/bad_entities"));
	     String line;
	     HashSet<String> bad_entities = new HashSet<String>();
	     while((line=br1.readLine())!=null)
	     {
	    	 bad_entities.add(line);
	     }
        Trie trie = trie_builder.build();
        HashMap<String, String> descriptions_old = new HashMap<String, String>();
        HashMap<String, ArrayList<String>> address = te.getAddress("/home/prajna/workspace/TutorialBank/data/topics_to_resources.csv");
  		HashMap<String, String> descriptions = te.readFile1("/mnt/dell/prajna/allwikipedia/all_text_1_tkb");
  		
  		ChannelSftp channelSftp = setupJsch(Integer.parseInt(remote_port_number));
  		channelSftp.connect();
  		
  		Tomcat tomcat = new Tomcat();
         tomcat.setBaseDir("temp1");
         tomcat.setPort(port);
        
         Logger logger = Logger.getLogger("");
         Handler fileHandler = new FileHandler(log, true);
         fileHandler.setFormatter(new SimpleFormatter());
         fileHandler.setLevel(Level.ALL);
         fileHandler.setEncoding("UTF-8");
         logger.addHandler(fileHandler);
         logger.log(Level.ALL, "hello");
         
         
         String contextPath = "/";
         String docBase = new File(".").getAbsolutePath();
          
         Context context = tomcat.addContext(contextPath, docBase);
          
         HttpServlet servlet = new HttpServlet() 
         {
             @Override
             protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                     throws ServletException, IOException 
             {
            	 PrintWriter writer = resp.getWriter();
              	
                  String querystring[] = req.getQueryString().split("&");
                  System.out.println(querystring.length);
                  System.out.println(querystring[0]);
                  System.out.println(querystring[1]);

                  String query="";
                  double mindistance=0.0;
                  int k=0;
                    query = querystring[0].replace("query=" ,"").trim();
                    mindistance = Double.parseDouble(querystring[1].replace("mindistance=", ""));
                    k = Integer.parseInt(querystring[2].replace("k=", ""));
                   double theta = Double.parseDouble(querystring[3].replace("theta=", ""));
                   double sim = Double.parseDouble(querystring[4].replace("sim=", ""));
                   int requested = Integer.parseInt(querystring[5].replace("requested=", ""));
                   // double lambda = Double.parseDouble(querystring[3].replace("lambda=", ""));
                  double lambda=0.0;
                  try 
 				{
             	   if(aa.getAdjList().get(query)==null)
                   {
                	   	writer.println("<h1>Query is: "+query+", it is not a valid wikipedia page name. Sorry :(</h1>");
    	                
    	                writer.println("</body></html>");
                   }
                   else
                   {
                	   HashMap<String,Double> refdScores = new HashMap<String,Double>();
                	   HashMap<String, HashSet<String>> support_sentences = new HashMap<String, HashSet<String>>();
                	   HashMap<String, Double> scores1 = new HashMap<String, Double>();

                	   if(descriptions.get(query)!=null)
                	   {
							scores1 = te.computeScore(a1.nodeMap1, descriptions.get(query), query, support_sentences, trie);
                	   }
                	   ArrayList<Entry<Set<String>,Double>> entryset = extractFacets(query,trie, mindistance, k,  lambda, address, descriptions, a1,aa,aa_firstpara,theta,sim,s,galago_port,bad_entities,support_sentences,refdScores,requested);
                	   testSftp(Integer.parseInt(remote_port_number),"10.17.50.132","root","welcome@123","/var/www/preface/json_files/"+query+"_"+sim+"_"+theta+".json","/var/www/app/prerequisites/json_files/");
                	  
                	   //	writer.println("</body></html>");
                	   writer.println("<html>");
   	             		writer.println("<head>\n" + 
   	             			"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" + 
   	             			"<link rel=\"stylesheet\" type=\"text/css\" href=\"tree-boxes.css\">\n" + 
   	             			"\n" + 
   	             			"    <script src=\"https://code.jquery.com/jquery-latest.min.js\" type=\"text/javascript\"></script>\n" + 
   	             			"    <script src=\"https://d3js.org/d3.v3.min.js\" type=\"text/javascript\"></script>\n" + 
   	             			"    <script src=\"tree-boxes.js\" type=\"text/javascript\"></script>\n"+
   	             			"<style>\n" + 
   	             			"body {font-family: Arial;}\n" + 
   	             			"\n" + 
   	             			"/* Style the tab */\n" + 
   	             			".tab {\n" + 
   	             			"  overflow: hidden;\n" + 
   	             			"  border: 1px solid #ccc;\n" + 
   	             			"  background-color: #f1f1f1;\n" + 
   	             			"}\n" + 
   	             			"\n" + 
   	             			"/* Style the buttons inside the tab */\n" + 
   	             			".tab button {\n" + 
   	             			"  background-color: inherit;\n" + 
   	             			"  float: left;\n" + 
   	             			"  border: none;\n" + 
   	             			"  outline: none;\n" + 
   	             			"  cursor: pointer;\n" + 
   	             			"  padding: 24px 36px;\n" + 
   	             			"  transition: 0.3s;\n" + 
   	             			"  font-size: 17px;\n" + 
   	             			"}\n" + 
   	             			"\n" + 
   	             			"/* Change background color of buttons on hover */\n" + 
   	             			".tab button:hover {\n" + 
   	             			"  background-color: #ddd;\n" + 
   	             			"}\n" + 
   	             			"\n" + 
   	             			"/* Create an active/current tablink class */\n" + 
   	             			".tab button.active {\n" + 
   	             			"  background-color: #ccc;\n" + 
   	             			"}\n" + 
   	             			"\n" + 
   	             			"/* Style the tab content */\n" + 
   	             			".tabcontent {\n" + 
   	             			"  display: none;\n" + 
   	             			"  padding: 36px 12px;\n" + 
   	             			"  border: 1px solid #ccc;\n" + 
   	             			"  border-top: none;\n" + 
   	             			"}\n" + 
   	             			"</style>\n" + 
   	             			"<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">\n" + 
   	             			"\n" + 
   	             			"\n" + 
   	             			"\n" + 
   	             			"	<style type=\"text/css\">\n" + 
   	             			"\n" + 
   	             			"		.gst20{\n" + 
   	             			"\n" + 
   	             			"			margin-top:20px;\n" + 
   	             			"\n" + 
   	             			"		}\n" + 
   	             			"\n" + 
   	             			"		#hdTuto_search{\n" + 
   	             			"\n" + 
   	             			"			display: none;\n" + 
   	             			"\n" + 
   	             			"		}\n" + 
   	             			"\n" + 
   	             			"		.list-gpfrm-list a{\n" + 
   	             			"\n" + 
   	             			"			text-decoration: none !important;\n" + 
   	             			"\n" + 
   	             			"		}\n" + 
   	             			"\n" + 
   	             			"		.list-gpfrm li{\n" + 
   	             			"\n" + 
   	             			"			cursor: pointer;\n" + 
   	             			"\n" + 
   	             			"			padding: 4px 0px;\n" + 
   	             			"\n" + 
   	             			"		}\n" + 
   	             			"\n" + 
   	             			"		.list-gpfrm{\n" + 
   	             			"\n" + 
   	             			"			list-style-type: none;\n" + 
   	             			"\n" + 
   	             			"    		background: #d4e8d7;\n" + 
   	             			"\n" + 
   	             			"		}\n" + 
   	             			"\n" + 
   	             			"		.list-gpfrm li:hover{\n" + 
   	             			"\n" + 
   	             			"			color: white;\n" + 
   	             			"\n" + 
   	             			"			background-color: #3d3d3d;\n" + 
   	             			"\n" + 
   	             			"		}\n" + 
   	             			"\n" + 
   	             			"	</style>\n" + 
   	             			"    <link href=\"https://pro.fontawesome.com/releases/v5.13.0/css/all.css\" rel=\"stylesheet\">\n" + 
   	             			"    <link href=\"http://eval_teknowbase_for_ir.apps.iitd.ac.in/prerequisites/css/bootstrap_4.3.1.min.css\" rel=\"stylesheet\">\n" + 
   	             			"    <link href=\"http://eval_teknowbase_for_ir.apps.iitd.ac.in/prerequisites/css/local.css\" rel=\"stylesheet\">\n" + 
   	             			"    <link href=\"http://eval_teknowbase_for_ir.apps.iitd.ac.in/prerequisites/css/bootstrap-multiselect.css\" rel=\"stylesheet\">"+
   	             			"</head>\n"); 
   	             			writer.println("<title>Welcome</title><body>");
   	             			writer.println("<div class=\"container\">\n" + 
   	              		"\n" + 
   	              		"	<!--<h1 class=\"text-center gst20\">Aspect-based pre-requisites generation</h1>-->\n" + 
   	              		"\n" + 
   	              		"	<div class=\"row justify-content-center gst20\">\n" + 
   	              		"\n" + 
   	              		"		<div class=\"col-sm-6\">\n" + 
   	              		"\n" + 
   	              		"			<form id=\"hdTutoForm\" method=\"POST\" action=\"action_page1.php\">\n" + 
   	              		"\n" + 
   	              		"				<div class=\"input-gpfrm input-gpfrm-lg\">\n" + 
   	              		"\n" + 
   	              		"				  <br><a href=\"http://eval_teknowbase_for_ir.apps.iitd.ac.in/prerequisites/index.php\"> <img src=\"preface_text_logo.png\" width=\"200\" height=\"50\" alt=\"PreFace Logo\" style=\"vertical-align:middle;margin:0px 150px\"></a>\n" + 
   	              		"				  <input type=\"text\" id=\"querystr\" name=\"query\" class=\"form-control\" placeholder=\"Enter the concept name\" aria-describedby=\"basic-addon2\"> <input type=\"submit\"/> \n" + 
   	              		"				  \n" + 
   	              		"\n" + 
   	              		"				</div>\n" + 
   	              		" <a href=\"#\" onclick=\"document.getElementById('querystr').value='latent_dirichlet_allocation';\">latent_dirichlet_allocation</a><br>\n" + 
   	              		"    <a href=\"#\" onclick=\"document.getElementById('querystr').value='convolutional_neural_network';\">convolutional_neural_network</a><br>\n" + 
   	              		"    <a href=\"#\" onclick=\"document.getElementById('querystr').value='conditional_random_field';\">conditional_random_field</a><br>\n" + 
   	              		"    <a href=\"#\" onclick=\"document.getElementById('querystr').value='artificial_neural_network';\">artificial_neural_network</a><br>\n" + 
   	              		"    <a href=\"#\" onclick=\"document.getElementById('querystr').value='hidden_markov_model';\">hidden_markov_model</a><br>\n" + 
   	              		"<a href=\"#\" onclick=\"document.getElementById('querystr').value='hash_array_mapped_trie';\">hash_array_mapped_trie</a><br>"+
   	              		""+
   	              		"\n" + 
   	              		"			</form>\n" + 
   	              		"\n" + 
   	              		"			<ul class=\"list-gpfrm\" id=\"hdTuto_search\"></ul>\n" + 
   	              		"\n" + 
   	              		"		</div>\n" + 
   	              		"\n" + 
   	              		"	</div>\n" + 
   	              		"\n" + 
   	              		"</div>\n" + 
   	              		"\n" + 
   	              		"\n" + 
   	              		"\n" + 
   	              		"<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js\"></script>\n" + 
   	              		"\n" + 
   	              		"<script type=\"text/javascript\">\n" + 
   	              		"\n" + 
   	              		"	$(document).ready(function(){\n" + 
   	              		"\n" + 
   	              		"	//Autocomplete search using PHP, MySQLi, Ajax and jQuery\n" + 
   	              		"\n" + 
   	              		"		//generate suggestion on keyup\n" + 
   	              		"\n" + 
   	              		"		$('#querystr').keyup(function(e){\n" + 
   	              		"\n" + 
   	              		"			e.preventDefault();\n" + 
   	              		"\n" + 
   	              		"			var form = $('#hdTutoForm').serialize();\n" + 
   	              		"\n" + 
   	              		"			$.ajax({\n" + 
   	              		"\n" + 
   	              		"				type: 'POST',\n" + 
   	              		"\n" + 
   	              		"				url: 'do_search.php',\n" + 
   	              		"\n" + 
   	              		"				data: form,\n" + 
   	              		"\n" + 
   	              		"				dataType: 'json',\n" + 
   	              		"\n" + 
   	              		"				success: function(response){\n" + 
   	              		"\n" + 
   	              		"					if(response.error){\n" + 
   	              		"\n" + 
   	              		"						$('#hdTuto_search').hide();\n" + 
   	              		"\n" + 
   	              		"					}\n" + 
   	              		"\n" + 
   	              		"					else{\n" + 
   	              		"\n" + 
   	              		"						$('#hdTuto_search').show().html(response.data);\n" + 
   	              		"\n" + 
   	              		"					}\n" + 
   	              		"\n" + 
   	              		"				}\n" + 
   	              		"\n" + 
   	              		"			});\n" + 
   	              		"\n" + 
   	              		"		});\n" + 
   	              		"\n" + 
   	              		"\n" + 
   	              		"\n" + 
   	              		"		//fill the input\n" + 
   	              		"\n" + 
   	              		"		$(document).on('click', '.list-gpfrm-list', function(e){\n" + 
   	              		"\n" + 
   	              		"			e.preventDefault();\n" + 
   	              		"\n" + 
   	              		"			$('#hdTuto_search').hide();\n" + 
   	              		"\n" + 
   	              		"			var fullname = $(this).data('fullname');\n" + 
   	              		"\n" + 
   	              		"			$('#querystr').val(fullname);\n" + 
   	              		"\n" + 
   	              		"		});\n" + 
   	              		"\n" + 
   	              		"	});\n" + 
   	              		"\n" + 
   	              		"</script>");
   	             		writer.println("<table>\n" + 
   	             				"		<tr>\n" + 
   	             				"			<td style=\"height:100px\">\n" + 
   	             				"    <div class=\"container\">\n" + 
   	             				"        <ct-visualization id=\"tree-container\"></ct-visualization>\n" + 
   	             				"        <script>\n" + 
   	             				"            d3.json(\"json_files/"+query+"_"+sim+"_"+theta+".json\", function(error, json) {\n" + 
   	             				"                treeBoxes('', json.tree);\n" + 
   	             				"            });\n" + 
   	             				"        </script>\n" + 
   	             				"    </div>\n" + 
   	             				"    </td>\n" + 
   	             				"    </tr>\n" + 
   	             				"    ");
   	             		 
   	             		
   		                writer.println(" <tr>\n" + 
   		                		"		<td>\n" + 
   		                		"			<h2>Facets (Click on the tabs below)</h2>\n" + 
   		                		"		</td>\n" + 
   		                		"	</tr>\n" + 
   		                		"	<tr>\n" + 
   		                		"	<td>\n" + 
   		                		"<div class=\"tab\">"); 
   		                
   		               ArrayList<ArrayList<Entry<String,Double>>> facetsOrdered = new ArrayList<ArrayList<Entry<String,Double>>>();
                	   HashMap<String,ArrayList<Entry<String, Double>>> facets_for_aspects = new HashMap<String,ArrayList<Entry<String, Double>>>();
   		               int f_count=0;
   		               HashSet<String> already_ret = new HashSet<String>();
                	   System.out.println("entryset size: "+entryset.size());
   		               BuildTree b = new BuildTree();
                	   for(Entry e:entryset)
                	   {
   		                	Set<String> s1 = (Set<String>) e.getKey();
 	             			ArrayList<Entry<String, Double>> ss = findPrerequisitesAndRank(s1, trie,bad_entities);
 	             			System.out.println("ss size is: "+ss.size());
 	             			facetsOrdered.add(ss);
 	             			
 	             			String aspect_name=b.stem(ss.get(0).getKey());
 	             			
 	             			
 	             			if(!already_ret.contains(aspect_name))
 	             			{
 	             				already_ret.add(aspect_name);
	 	             			ArrayList<Entry<String, Double>> ss_new = findPrerequisitesAndRank1(s1, trie,s,aspect_name,bad_entities, refdScores);
	 	            			if(ss_new.size()<=2) continue;
	 	             			facets_for_aspects.put(aspect_name, ss_new);
	 	            			if(f_count==0)
	 	            				writer.println("<button class=\"tablinks\" onclick=\"openCity(event,'"+aspect_name+"')\" id=\"defaultOpen\">"+aspect_name+"</button>\n");
	 	            			else
	 	            				writer.println("<button class=\"tablinks\" onclick=\"openCity(event,'"+aspect_name+"')\">"+aspect_name+"</button>\n");
	 	            			f_count++;
 	             			}
 	             			else
 	             			{
 	             				if(!ss.get(0).getKey().equals(aspect_name))
 	             				{
 	 	             				ArrayList<Entry<String,Double>> ss_new = facets_for_aspects.get(aspect_name);
 	 	             				ss_new.addAll(findPrerequisitesAndRank1(s1, trie,s,ss.get(0).getKey(),bad_entities, refdScores));
 	             					//ArrayList<Entry<String, Double>> ss_new = findPrerequisitesAndRank1(s1, trie,s,aspect_name,bad_entities, refdScores);
 		 	            			facets_for_aspects.put(aspect_name, ss_new);
 	             				}
 	             			}
	                	}
                	    writer.println("</div>\n" + 
                	    		"</td>\n" + 
                	    		"</tr>\n" + 
                	    		"</table>");
                	    int count=0;
                	    
                	    String query_with_spaces = query.replace("_", " ");
                	    for(String aspect_name:facets_for_aspects.keySet())
    	                {
                	    	ArrayList<Entry<String,Double>> new_list = facets_for_aspects.get(aspect_name);
                	    	String facet_query_string=query+"_"+aspect_name;
                	    	for(int i=0;i<new_list.size();i++)
   		                	{
                	    		facet_query_string = facet_query_string+"_"+new_list.get(i).getKey();
   		                	}
                	    	facet_query_string=facet_query_string.replace("_", "+");
                	    	//if(!succset1.contains(e.getKey())) continue;
    	                	//if(scores1.get(e.getKey())==null && co_occ.get(e.getKey())==null && !succset1.contains(e.getKey())) continue;
    	                	count++;
    	                	//String aspect_name=ss.get(0).getKey();
						
    	                	writer.println("<div id=\""+aspect_name+"\" class=\"tabcontent\">");
    	                	writer.println("<h3>"+aspect_name);
    	                	
    	                	writer.println("</h3>");
    	                	
    	                	//writer.println("<h3>"+e.getKey()+", "+e.getValue()+"</h3>");
    	                	//for(int i=0;i<ss.size();i++)
    	                	for(int i=0;i<new_list.size();i++)
   		                	{
    	                		int c1=1;
    	                		String aspect_value=new_list.get(i).getKey();
    	                		if(aspect_value.equals(query)) continue;
    	                		String aspect_value_q = query.replace("_","+")+"+"+aspect_value.replace("_", "+");
    	                		writer.println("<p><b>"+aspect_value+"</b></p>"+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"http://eval_teknowbase_for_ir.apps.iitd.ac.in/olivaw6004/search?q="+aspect_value_q+"\" target=\"_blank\">"+"[Explore on Open Research Corpus]<img src=\"external_link.png\" width=\"20\" height=\"20\"></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=\"https://stackoverflow.com/search?q="+aspect_value_q+"\" target=\"_blank\">[Explore on Stack Overflow]<img src=\"so.png\" width=\"20\" height=\"20\"></a>"+"<br>");
    	                		if(query.contains(aspect_value)) continue;
    	                			
    	                		if(support_sentences.get(aspect_value)!=null)
    		                	{
    	                			for(String ss1:support_sentences.get(aspect_value))
    			                	{
    	                				String aspect_name_space = aspect_value.replace("_", " ");
    	                				ss1 = ss1.replace(aspect_name_space, "<mark><b>"+aspect_name_space+"</b></mark>").replace(query_with_spaces, "<mark><b>"+query_with_spaces+"</b></mark>");
    			                		writer.println("<p>      "+c1+") "+ss1+"</p><br>");
    			                		c1++;
    			                	}
    		                	}
    	                	}
    	                	writer.println("</div>");
    	                }
    	                
    	                writer.println("<script>");
    	                writer.println("function openCity(evt, cityName) {");
    	                //writer.println("function openCity(evt, cityName) {");
    	                writer.println("tabcontent = document.getElementsByClassName(\"tabcontent\");");
    	                writer.println(" for (i = 0; i < tabcontent.length; i++) {\n" + 
    	                		"    tabcontent[i].style.display = \"none\";\n" + 
    	                		"  }\n" + 
    	                		"  tablinks = document.getElementsByClassName(\"tablinks\");\n" + 
    	                		"  for (i = 0; i < tablinks.length; i++) {\n" + 
    	                		"    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");\n" + 
    	                		"  }\n" + 
    	                		"  document.getElementById(cityName).style.display = \"block\";\n" + 
    	                		"  evt.currentTarget.className += \" active\";\n" + 
    	                		"}\n" + 
    	                		"document.getElementById(\"defaultOpen\").click();\n"+
    	                		"</script>");
    	                // writer.println("<iframe src=\"https://en.wikipedia.org/wiki/PageRank#Variations\">\n" + "</iframe>");
    	                
    	                writer.println("</body></html>");
    	                logger.log(Level.ALL, "wrote to file");
                   }
             	 
            		/*String[] query_arr= new String[args.length+1];
            		for(int i=0;i<args.length;i++)
            		{
            			query_arr[i]=args[i];
            		}
            		query_arr[args.length]="--query="+query.replace("_", " ");
            		  List<Parameters> queries;
            		  Parameters parameters1 = Arguments.parse(query_arr);
            		  System.out.println(parameters1); 
            		  String queryFormat = parameters1.get("queryFormat", "json").toLowerCase();
            	        switch (queryFormat) {
            	            case "json":
            	                queries = JSONQueryFormat.collectQueries(parameters1);
            	                break;
            	            case "tsv":
            	                queries = JSONQueryFormat.collectTSVQueries(parameters1);
            	                break;
            	            default:
            	                throw new IllegalArgumentException("Unknown queryFormat: " + queryFormat + " try one of JSON, TSV");
            	        }            		 
            		 
            		 
            	        
            	        System.out.println("here");
            	        System.out.println(queries.get(0));

            	        // record results requested
            	        int requested = (int) parameters1.get("requested", 1000);

            	        // for each query, run it, get the results, print in TREC format
            	        HashSet<Integer> h = new HashSet<Integer>();

            	        //BufferedWriter bw = new BufferedWriter(new FileWriter(doc_in_file));
            	        for(Parameters query_single:queries)
            	        {
            	       
            	            String queryText = query_single.getString("text");
            	            System.out.println("text of the query is: " + queryText);
            	            String queryNumber = query_single.getString("number");

            	            query_single.setBackoff(parameters1);
            	            query_single.set("requested", requested);

            	            // option to fold query cases -- note that some parameters may require upper case
            	            if (query_single.get("casefold", false)) {
            	                queryText = queryText.toLowerCase();
            	            }

            	           
            	            // parse and transform query into runnable form
            	            Node root = StructuredQuery.parse(queryText);

            	            // --operatorWrap=sdm will now #sdm(...text... here)
            	            if (parameters1.isString("operatorWrap")) {
            	                if (root.getOperator().equals("root")) {
            	                    root.setOperator(parameters1.getString("operatorWrap"));
            	                } else {
            	                    Node oldRoot = root;
            	                    root = new Node(parameters1.getString("operatorWrap"));
            	                    root.add(oldRoot);
            	                }
            	            }
            	            //String file_name=parameters.getString("entitylist");
            	            Node transformed = retrieval.transformQuery(root, query_single);
            	            String query_text = transformed.getText().replaceAll("\\s{2,}", " ").trim();

            	            List<ScoredDocument> results;

            	            // run query
            	            results = retrieval.executeQuery(transformed, parameters1).scoredDocuments;
            	            System.out.println("\n" + results.size() + "\n");
            	             org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
            	            org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(parameters1);
            	            writer.println("<body>");
            	            for (ScoredDocument n : results)
            	            {
            	                String docname = retrieval.getDocumentName(n.document);
            	                org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
            	                String dtext = d.contentForXMLTag("title")+"\t"+d.contentForXMLTag("paperAbstract");
            	               // out.print(n.document+"\n");
            	                System.out.println(docname);
            	                
            	                writer.println("<p>"+dtext+"</p>");
            	               
            	            }

            	            
            	        }
            	        writer.println("</body></html>");*/
            	        
             		
 				}
             	catch(Exception e)
             	{
             		
             	}
             }

			
         };
         
        String servletName = "Servlet2";
        //String urlPattern = hm.get("urlpattern");
         
        
        
        tomcat.addServlet(contextPath, servletName, servlet);      
        context.addServletMappingDecoded(urlPattern, servletName);
         
        tomcat.start();
        tomcat.getServer().await();
      
	}
	
	public static void readGraph(String teknowbase_mapped, String nodemap, String relmap, String query) throws Exception
	{
		//String query="declarative_memory";
		Aspect a = new Aspect();
  		AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);

		Set<Integer> neigh = a1.undirectedGraph.adjacentNodes(a1.nodeMap1.get(query));
		

	}
	
	public static Set<String> chooseNeighbors(String p, HashMap<String, double[]> features,
			double mindistance) 
	{
		NormalizedDotProductMetric norm = new NormalizedDotProductMetric();
		
		SparseVector sv1 = new SparseVector(features.get(p));
		//if(sv1.absNorm()!=0) System.out.println("norm of "+p+" is: "+sv1.absNorm());
		HashMap<String, Double> cosine_sim = new HashMap<String, Double>();
		
		for(String pp:features.keySet())
		{
			SparseVector sv2 = new SparseVector(features.get(pp));
			//if(sv2.absNorm()!=0) System.out.println("norm of "+pp+" is: "+sv2.absNorm());
			cosine_sim.put(pp, 1-norm.distance(sv1,sv2));
		}
		
		ArrayList<Entry<String, Double>> cosine_sim_sorted = new ArrayList<Entry<String, Double>>(cosine_sim.entrySet());
		Collections.sort(cosine_sim_sorted, LanguageModelEntity.valueComparator);
		Set<String> facet = new HashSet<String>();
		int count=0;
		for(Entry e:cosine_sim_sorted)
		{
			//System.out.println("most similar phrase to "+p+" is: "+e.getKey()+", similarity is: "+e.getValue());
			
			if((double) e.getValue() >= mindistance)
			{
				facet.add((String) e.getKey());
			}
			else break;
			count++;
			//if(count==10) break;
		}
		facet.add(p);
		//System.out.println("chosen element is "+p);
		//System.out.println("chosen set of neighbors are "+facet.toString());
		// TODO Auto-generated method stub
		return facet;
	}
	
	public static void main(String args[]) throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		System.out.println("here");
		ReadSubgraph rr = new ReadSubgraph();
		//generateScript(hm.get("queries"));
		//parseXMLStackOverflow("/mnt/dell/prajna/stackoverflow");
		preface(args,  hm.get("wikipedia"), hm.get("wikipedia-first-para"),  hm.get("mappings"), hm.get("emb"), hm.get("teknowbase-mapped"), hm.get("nodemap"), hm.get("relmap"), hm.get("urlPattern"), hm.get("log"), hm.get("port"), hm.get("remote_port_number"),hm.get("galago_port")); 
		//extractAbstractFromDisk(hm.get("query"),Integer.parseInt(hm.get("topk")),hm.get("galago_port"));
		//prefaceTest(args, hm.get("wikipedia"), hm.get("wikipedia-first-para"),hm.get("mappings"), hm.get("emb"), hm.get("teknowbase-mapped"), hm.get("nodemap"), hm.get("relmap"), Double.parseDouble(hm.get("theta")), Double.parseDouble(hm.get("sim")), hm.get("query"), Double.parseDouble(hm.get("mindistance")), Integer.parseInt(hm.get("topk")),hm.get("galago_port"),hm.get("remote_port_number"));
		//testSftp(Integer.parseInt(hm.get("remote_port_number")));
		//generateJSON(hm.get("path"));
		/*AdjList dummy = new AdjList();
		UtilityFunctions u = new UtilityFunctions();
		HashMap<String, String> stringToId = u.createStringToIdMapping(hm.get("mappings"));
		HashMap<String, LanguageModelEntity> s = u.readEmbeddingsFromFile(hm.get("emb"),stringToId,dummy);
		
		AdjList aa = rr.readFromFile(new BufferedReader(new FileReader(hm.get("wikipedia"))));
		//computeRefDAndGenerateJSON(hm.get("query"),aa, Double.parseDouble(hm.get("theta")),s,Double.parseDouble(hm.get("sim")));
		//testJaccardSim(hm.get("phrase1"),hm.get("phrase2"));
		//tagEntitiesInClusters(hm.get("queries"),hm.get("teknowbase-mapped"), hm.get("nodemap"), hm.get("relmap"));
		//testRanking3(hm.get("query"),hm.get("teknowbase-mapped"), hm.get("nodemap"), hm.get("relmap"),Double.parseDouble(hm.get("mindistance")), Integer.parseInt(hm.get("topk")), Double.parseDouble(hm.get("lambda")),hm.get("mappings"),hm.get("emb"),hm.get("wikipedia"));
		//test(hm.get("query"));

		//readGraph(hm.get("teknowbase-mapped"), hm.get("nodemap"), hm.get("relmap"), hm.get("query"));
		//String text = "this is a text that has to generate keywords such as hidden markov model ";
		//extractPhrases(text);
		
		
		/*System.setProperty("https.proxyHost", "10.10.78.61");
		System.setProperty("https.proxyPort", "3128");
		String urlString="https://en.wikipedia.org/w/api.php?action=query&titles=machine_learning&prop=links&pllimit=max&format=json";
		URL url = new URL(urlString);
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		String line;
		String inline="";
		while((line=reader.readLine())!=null)
		{
			inline = inline + "\n"+line;
		}
		//System.out.println(inline);
		JSONParser parse = new JSONParser();
		JSONObject jobj = (JSONObject)parse.parse(inline);
		JSONObject jobj1 = (JSONObject) ((JSONObject) jobj.get("query")).get("pages");
		System.out.println(jobj1.toJSONString());
		/*JSONArray jsonarr_1 = new JSONArray();
		jsonarr_1.add(jobj);
		System.out.println(jsonarr_1.size());
		for(int i=0;i<jsonarr_1.size();i++)
		{
		//Store the JSON objects in an array
		//Get the index of the JSON object and print the values as per the index
		JSONObject jsonobj_1 = (JSONObject)jsonarr_1.get(i);
		System.out.println("Elements under results array");
		
		}*/
		//JSONArray jsonarr_1 = (JSONArray) jobj.get("query");
		
		
		//preface(args);
		 
		// (new RetrieveDocuments()).run(Arguments.parse(args), System.out);
	}
	
	
	
	public static void generateScript(String queries) throws Exception
	{
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    Aspect a = new Aspect();
	    AdjListCompact a1 = a.readGraphEfficientAlternateUnd(teknowbase_mapped, relmap, nodemap);
	  	BufferedReader br = new BufferedReader(new FileReader(queries));
	  	String line;
	  	HashSet<String> moreQueries = new HashSet<String>();
	  	while((line=br.readLine())!=null)
	  	{
	  		if(a1.nodeMap1.get(line)!=null)
	  		{
		  		Set<Integer> neigh = a1.undirectedGraph.adjacentNodes(a1.nodeMap1.get(line));
				
				for(int n:neigh)
				{
					String en = a1.nodeMap.get(n);
			  		
			  			moreQueries.add(en);
			  			Set<Integer> neigh1 = a1.undirectedGraph.adjacentNodes(n);
						for(int nn:neigh1)
						{
							moreQueries.add(a1.nodeMap.get(nn));
						}
						
				}
	  		}
	  		
	  		line = line.replace("_", " ");
	  		//String command='./galago_aspect1/galago-3.13/core/target/appassembler/bin/galago retrieve-documents --requested=1000 --index=research_papers_indexed --query="'+line+'"  --outputFile="/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/top_1000_results/'+line.replace(" ","_")+'"\n';
	  	}
	  	HashSet<String> moreQ1 = new HashSet<String>();
	  	BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/retrieve_entities.sh"));
	  	
	  	for(String qq:moreQueries)
	  	{
	  		qq = qq.replace("'", "").replace("\"", "");
	  		File f = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/top_1000_results/"+qq);
	  		qq = qq.replace("_", " ");
	  		if(!f.exists())
	  		{
	  			String command="./galago_aspect1/galago-3.13/core/target/appassembler/bin/galago retrieve-documents --requested=1000 --index=research_papers_indexed --query='"+qq+"' --outputFile='/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/top_1000_results/"+qq.replace(" ","_")+"'\n";
	  			bw.write(command+"\n");
	  		}
	  		
	  	}
	  	bw.close();
	  	
	    
	}
	
    public static void mainOld(String[] args) throws Exception 
    {
    	System.out.println("updated working");
    	GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		TagEntities te = new TagEntities();
		String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	    String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	    String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	    String wikipedia = "/mnt/dell/prajna/neo4j/input/allLinks.tsv";
	    Aspect a = new Aspect();
	    int port = Integer.parseInt(hm.get("port"));
	      
  		AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
  		Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        
		for(String entity : a1.nodeMap1.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity.replace("_", " "));
        }
        Trie trie = trie_builder.build();
        HashMap<String, String> descriptions_old = new HashMap<String, String>();
        HashMap<String, ArrayList<String>> address = te.getAddress("/home/prajna/workspace/TutorialBank/data/topics_to_resources.csv");
  		HashMap<String, String> descriptions = te.readFile1("/mnt/dell/prajna/allwikipedia/all_text_1_tkb");
  		
  		String app_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/application.tsv";
  		String alg_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/algorithm.tsv";
  		String imp_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/implementation.tsv";
  		
  		HashMap<String, Double> app_paths = readPathScores(app_meta_path, a1);
  		HashMap<String, Double> alg_paths = readPathScores(alg_meta_path, a1);
  		HashMap<String, Double> imp_paths = readPathScores(imp_meta_path, a1);
  		
  		
  		BufferedReader br1 = new BufferedReader(new FileReader(hm.get("types")));
		String line;
		HashMap<String, HashSet<String>> supertypes = new HashMap<String, HashSet<String>>();
		HashMap<String, ArrayList<Edge>> supertypes_edge = new HashMap<String, ArrayList<Edge>>();
		while((line=br1.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			String aa = tok.nextToken();
			String bb = tok.nextToken();
			if(aa.length()<=3 || bb.length()<=3) continue;
			if(supertypes.get(aa)==null)
			{
				HashSet<String> superl = new HashSet<String>();
				///ArrayList<Edge> superl_edge = new ArrayList<Edge>();
				superl.add(bb);
				//superl_edge.add(new Edge(0,bb));
				supertypes.put(aa, superl);
				//supertypes_edge.put(aa, superl_edge);
			}
			else
			{
				HashSet<String> superl = supertypes.get(aa);
				//ArrayList<Edge> superl_edge = supertypes_edge.get(aa);
				superl.add(bb);
				//superl_edge.add(new Edge(0,bb));
				supertypes.put(aa, superl);
				//supertypes_edge.put(aa, superl_edge);
			}
		}
		System.out.println("read types");
		
		//AdjList aa = new AdjList(supertypes_edge);
	//	HashMap<String, ArrayList<String>> supertypes_inc = aa.getIncidentList();
		/*
		 * br1 = new BufferedReader(new FileReader(hm.get("entity-mappings")));
		 * HashMap<String, String> h1 = new HashMap<String, String>();
		 * while((line=br1.readLine())!=null) { StringTokenizer tok = new
		 * StringTokenizer(line,"\t"); String a11 = tok.nextToken(); String b11 =
		 * tok.nextToken(); h1.put(a11, b11); } LanguageModelEntity l = new
		 * LanguageModelEntity(); HashMap<String, LanguageModelEntity> h_la =
		 * l.readEmbeddingsFromFile(hm.get("entity-embeddings"), h1);
		 */
		 
		 ReadSubgraph rr = new ReadSubgraph();
  		//AdjList wiki = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
  		MutableValueGraph<String, Integer> wiki1 = a.readGraphEfficientWiki(wikipedia);
  		MutableValueGraph<String, Integer> wiki = a.addGraph(wiki1, hm.get("new-graph"));
  		ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki1);
  		//System.out.println("read graph and it worked");
  		LanguageModelEntity lm = new LanguageModelEntity();
        
  		System.out.println("read wikipedia and tutorial bank files");
		
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir("temp1");
        tomcat.setPort(port);
        Logger logger = Logger.getLogger("");
        Handler fileHandler = new FileHandler(hm.get("log"), true);
        fileHandler.setFormatter(new SimpleFormatter());
        fileHandler.setLevel(Level.ALL);
        fileHandler.setEncoding("UTF-8");
        logger.addHandler(fileHandler);
        logger.log(Level.ALL, "hello");
        
        
         
        String contextPath = "/";
        String docBase = new File(".").getAbsolutePath();
         
        Context context = tomcat.addContext(contextPath, docBase);
         
        HttpServlet servlet = new HttpServlet() 
        {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                    throws ServletException, IOException 
            {
            	 PrintWriter writer = resp.getWriter();
             	
                 String query = req.getQueryString().replace("query=", "");
                 
            	try 
				{
            		if(address.get(query)!=null)
            		{
            			String tot_line="";
            			for(String ad:address.get(query))
            			{
	            			BufferedReader br = new BufferedReader(new FileReader(ad));
	            			String line;
	            			while((line=br.readLine())!=null)
	            			{
	            				tot_line = tot_line + " . "+line;
	            			}
	            			if(descriptions.get(query)!=null)
	            			{
	            				descriptions.put(query, descriptions.get(query)+" . "+tot_line);
	            			}
	            			br.close();
            			}
            		}
            		
                HashMap<String, Double> app_hm = new HashMap<String, Double>();
                HashMap<String, Double> alg_hm = new HashMap<String, Double>();
               HashMap<String, Double> imp_hm = new HashMap<String, Double>();
                
               if(wiki_i.successors(query)==null)
               {
            	   	writer.println("<h1>Query is: "+query+", it is not a valid wikipedia page name. Sorry :(</h1>");
	                
	                writer.println("</body></html>");
               }
               else
               {
            	   HashMap<String, Double> scores1 = new HashMap<String, Double>();
            	   HashMap<String, HashSet<String>> support_sentences = new HashMap<String, HashSet<String>>();
					if(descriptions.get(query)!=null)
					{
						scores1 = te.computeScore(a1.nodeMap1, descriptions.get(query), query, support_sentences, trie);
					}
					
					File ff = new File("/mnt/dell/Hemanth/iitd/research_papers_indexed/faceted_prerequisites/pairwise_scores/"+query);
					HashMap<String, Double> co_occ = new HashMap<String, Double>(); //stores co-occurence statistics from semantic scholar corpus
					if(ff.exists())
					{
						System.out.println(query+" exists.");
						co_occ = te.computeScore1(ff);
					}
					
					System.out.println("parse_tree: "+ co_occ.get("parse_tree"));
					
            	   	ArrayList<String> successors = new ArrayList<String>(wiki_i.successors(query));
            	   	HashSet<String> succset = new HashSet<String>(successors);
					
            	   	//successors.addAll(co_occ.keySet());
            	   	
            	   	HashMap<String, Double> scores = new HashMap<String, Double>();
            	   	HashMap<String, HashSet<String>> subtypes = new HashMap<String, HashSet<String>>();
					HashMap<String, HashSet<String>> subtypes2 = new HashMap<String, HashSet<String>>();

            	   	for(String name:successors)
					{
            	   		//System.out.println("successor: "+name+"\n");
						if(a1.nodeMap1.get(query)!=null && a1.nodeMap1.get(name)!=null)
						{
							//System.out.println(name+" is mapped");
							Optional c1 =  a1.labeledGraph.edgeValue(a1.nodeMap1.get(query), a1.nodeMap1.get(name));
							Optional c11 =  a1.labeledGraph.edgeValue(a1.nodeMap1.get(name), a1.nodeMap1.get(query));
							if(c1.isPresent() || c11.isPresent())
							{
								ArrayList<Integer> c2 = (ArrayList<Integer>) c1.get();
								for(int cc:c2)
								{
									//System.out.println("Relations are: "+a1.relmap1.get(cc));
									if(subtypes2.get(a1.relmap1.get(cc))==null)
									{
										HashSet<String> types = new HashSet<String>();
										types.add(name);
										subtypes2.put(a1.relmap1.get(cc), types);
									}
									else
									{
										HashSet<String> types = subtypes2.get(a1.relmap1.get(cc));
										types.add(name);
										subtypes2.put(a1.relmap1.get(cc), types);
									}
								}
							}
							
							if(c11.isPresent())
							{
								//System.out.println("relation exists");
								ArrayList<Integer> c2 = (ArrayList<Integer>) c11.get();
								for(int cc:c2)
								{
									if(subtypes2.get(a1.relmap1.get(cc))==null)
									{
										HashSet<String> types = new HashSet<String>();
										types.add(name);
										subtypes2.put(a1.relmap1.get(cc), types);
									}
									else
									{
										HashSet<String> types = subtypes2.get(a1.relmap1.get(cc));
										types.add(name);
										subtypes2.put(a1.relmap1.get(cc), types);
									}
									if(scores.get(a1.relmap1.get(cc))!=null)
		            	   			{
		            	   				scores.put(a1.relmap1.get(cc), scores.get(a1.relmap1.get(cc))+1);
		            	   			}
		            	   			else
		            	   			{
		            	   				scores.put(a1.relmap1.get(cc), 1.0);
		            	   			}
								}
							}
						}
						
            	   		if(supertypes.get(name)!=null)
            	   		{
            	   			HashSet<String> superl = supertypes.get(name);
            	   			HashSet<String> subtypes_names;
            	   			for(String ss:superl)
            	   			{
            	   				//System.out.println("supertype of "+name+" is "+ss);
	            	   			if(subtypes.get(ss)!=null)
	            	   			{
	            	   				subtypes_names = subtypes.get(ss);
	            	   				subtypes_names.add(name);
	            	   				subtypes.put(ss,subtypes_names);
	            	   			}
	            	   			else
	            	   			{
	            	   				subtypes_names = new HashSet<String>();
	            	   				subtypes_names.add(name);
	            	   				subtypes.put(ss,subtypes_names);
	            	   			}
	            	   			
	            	   			
            	   			}
            	   		}
					
					}
					
					for(String ss:subtypes.keySet())
					{
						boolean flag = false;
						//String ss_stemmed = PlingStemmer.stem(ss);
						for(String ss1:subtypes.keySet())
						{
							if(ss.equals(ss1)) continue;
							//////String ss1_stemmed = PlingStemmer.stem(ss1);
							if(supertypes.get(ss)!=null )
							{
								
								if(supertypes.get(ss).contains(ss1) )
								{
									flag=true;
									HashSet<String> hs = subtypes.get(ss1);
									hs.add(ss);
									hs.addAll(subtypes.get(ss));
									if(subtypes2.get(ss1)==null)
									{
										subtypes2.put(ss1, hs);
									}
									else
									{
										HashSet<String> hs1 = subtypes2.get(ss1);
										hs1.addAll(hs);
										subtypes2.put(ss1, hs1);
									}
									
								}
								
							}
							
							
						}
						if(!flag)
						{
							subtypes2.put(ss, subtypes.get(ss));
						}
					}
            	   	
					for(String ss:subtypes2.keySet())
					{
						System.out.println("subtypes2 parent is: "+ss);
						for(String ss1:subtypes2.get(ss))
						{
							System.out.println("subtypes are: "+ss1);
						}
						if(scores.get(ss)==null)
						{
							scores.put(ss, (double) subtypes2.get(ss).size());
						}
					}
					 
					subtypes = subtypes2;
					
					ArrayList<Entry<String, Double>> app_ordered = new ArrayList(app_hm.entrySet());
					ArrayList<Entry<String, Double>> alg_ordered = new ArrayList(alg_hm.entrySet());
					ArrayList<Entry<String, Double>> imp_ordered = new ArrayList(imp_hm.entrySet());
					
					ArrayList<Entry<String, Double>> scores_ordered = new ArrayList(scores.entrySet());
					ArrayList<Entry<String, Double>> scores1_ordered = new ArrayList<Entry<String, Double>>();
					HashMap<String, Double> scores2 = new HashMap<String, Double>(); 
					
					for(Entry<String, Double> e:scores_ordered)
	                {
					 	String e1 = e.getKey();
					 	double cooccurence_score=0;
					 	if(scores1.get(e1)!=null)
					 	{
					 		cooccurence_score = cooccurence_score + scores1.get(e1);
					 	}
					 	if(co_occ.get(e1)!=null)
					 	{
					 		cooccurence_score = cooccurence_score + co_occ.get(e1);
					 	}
					 	for(String ss:subtypes.get(e1))
					 	{
					 		if(scores1.get(ss)!=null)
						 	{
						 		cooccurence_score = cooccurence_score + scores1.get(ss);
						 	}
					 		if(co_occ.get(ss)!=null)
						 	{
						 		cooccurence_score = cooccurence_score + co_occ.get(ss);
						 	}
					 	}
					 	//if(cooccurence_score!=0)
					 	//{
					 		//cooccurence_score = cooccurence_score + scores.get(e1);
					 	//}
					 	Entry<String, Double> e2 = new AbstractMap.SimpleEntry<String, Double>(e1,cooccurence_score);
					 	scores1_ordered.add(e2);
					 	scores2.put(e1, cooccurence_score);
	                }
					
					Collections.sort(app_ordered, LanguageModelEntity.valueComparator);
					Collections.sort(alg_ordered, LanguageModelEntity.valueComparator);
					Collections.sort(imp_ordered, LanguageModelEntity.valueComparator);
					
					Collections.sort(scores1_ordered, LanguageModelEntity.valueComparator);
					
					HashMap<String, Double> word_wise = new HashMap<String, Double>();
					
					/*for(Entry<String, Double> ss:scores_ordered)
					{
						StringTokenizer tok = new StringTokenizer(ss.getKey(),"_");
						while(tok.hasMoreTokens())
						{
							String a = tok.nextToken();
							if(word_wise.get(a)==null)
								word_wise.put(a,ss.getValue());
							else
								word_wise.put(a, word_wise.get(a)+ss.getValue());
						}
						
					}*/
					HashMap<String, HashSet<String>> word_types = new HashMap<String, HashSet<String>>();
					HashMap<String, HashSet<String>> word_types_up = new HashMap<String, HashSet<String>>();
					/*for(Entry<String, Double> ss:scores1_ordered)
					{
						String ent = ss.getKey();
						if(a1.nodeMap1.get(ent)!=null && a1.getRelMap().get("typeof")!=null)
						{
							Integer ent_id = a1.nodeMap1.get(ent);
							Integer rel_id=a1.relmap.get("typeof");
							long l = (((long)ent_id.intValue()) << 32) | (rel_id.intValue() & 0xffffffffL);
							if(a1.pairAdjList.get(l)!=null)
							{
								Set<Integer> types = a1.pairAdjList.get(l);
								for(int t:types)
								{
									String parent = a1.nodeMap.get(t);
									
									if(word_types.get(parent)==null)
									{
										HashSet<String> sstypes = new HashSet<String>();
										sstypes.addAll(subtypes.get(ent));
										word_types.put(parent, sstypes);
										HashSet<String> wtypes_up = new HashSet<String>();
										wtypes_up.add(ent);
										word_types_up.put(parent, wtypes_up);
									}
									else
									{
										HashSet<String> sstypes = word_types.get(parent);
										sstypes.addAll(subtypes.get(ent));
										word_types.put(parent, sstypes);
										
										HashSet<String> wtypes_up = word_types_up.get(parent);
										wtypes_up.add(ent);
										word_types_up.put(parent, wtypes_up);
									}
									if(word_wise.get(parent)==null)
										word_wise.put(parent, scores2.get(ent));
									else
										word_wise.put(parent, scores2.get(ent)+(double) word_types.get(parent).size());
								}
							}
						}
						else
						{
							//if(a1.nodeMap1.get(ent)==null)
								//System.out.println("entry of "+ent+" is null");
							//if(a1.relmap.get("typeof")==null)
								//System.out.println("entry of typeof is null");
						}
					}*/
					
					
					
					ArrayList<Entry<String, Double>> word_wise_ordered = new ArrayList<>(word_wise.entrySet());
					Collections.sort(word_wise_ordered,LanguageModelEntity.valueComparator);
					
					
	               // writer.println("<html><title>Welcome</title><body>");
	                //writer.println("<h1>Query is: "+query+"</h1>");
					
					writer.println("<html>");
	             	writer.println("<head>\n" + 
	             			"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" + 
	             			"<style>\n" + 
	             			"body {font-family: Arial;}\n" + 
	             			"\n" + 
	             			"/* Style the tab */\n" + 
	             			".tab {\n" + 
	             			"  overflow: hidden;\n" + 
	             			"  border: 1px solid #ccc;\n" + 
	             			"  background-color: #f1f1f1;\n" + 
	             			"}\n" + 
	             			"\n" + 
	             			"/* Style the buttons inside the tab */\n" + 
	             			".tab button {\n" + 
	             			"  background-color: inherit;\n" + 
	             			"  float: left;\n" + 
	             			"  border: none;\n" + 
	             			"  outline: none;\n" + 
	             			"  cursor: pointer;\n" + 
	             			"  padding: 14px 16px;\n" + 
	             			"  transition: 0.3s;\n" + 
	             			"  font-size: 17px;\n" + 
	             			"}\n" + 
	             			"\n" + 
	             			"/* Change background color of buttons on hover */\n" + 
	             			".tab button:hover {\n" + 
	             			"  background-color: #ddd;\n" + 
	             			"}\n" + 
	             			"\n" + 
	             			"/* Create an active/current tablink class */\n" + 
	             			".tab button.active {\n" + 
	             			"  background-color: #ccc;\n" + 
	             			"}\n" + 
	             			"\n" + 
	             			"/* Style the tab content */\n" + 
	             			".tabcontent {\n" + 
	             			"  display: none;\n" + 
	             			"  padding: 6px 12px;\n" + 
	             			"  border: 1px solid #ccc;\n" + 
	             			"  border-top: none;\n" + 
	             			"}\n" + 
	             			"</style>\n" + 
	             			"</head>\n"); 
	             			writer.println("<title>Welcome</title><body>");
	              
	                HashMap<String, String> new_parent = new HashMap<String, String>();
	                int cc=0;
	                HashSet<String> dont_display = new HashSet<String>();
	                for(Entry<String, Double> e:word_wise_ordered)
	                {
	                	dont_display.add(e.getKey());
	                	cc++;
	                	writer.println("<h3>"+e.getKey()+", "+e.getValue()+"</h3>");
	                	for(String wt:word_types.get(e.getKey()))
	                	{
	                		dont_display.add(wt);
	                			writer.println("<p>"+wt+"</p>");
	                		
	                	}
	                	if(cc==5) break;
	                }
	                writer.println("<br><br><br>");
	                writer.println("<h2>Aspects</h2>\n" + 
	                		"<p>Click on the buttons inside the tabbed menu:</p>\n" + 
	                		"\n" + 
	                		"<div class=\"tab\">\n"); 
	                
	               HashMap<String, Double> scores1_ordered1 = new HashMap<String, Double>();
	               HashMap<String, HashSet<String>> support_sentences1 = new HashMap<String, HashSet<String>>();
	               HashMap<String, HashSet<String>> subtypes1 = new HashMap<String, HashSet<String>>();
	               HashSet<String> succset1 = new HashSet<String>(succset);
	               for(Entry<String, Double> e:scores1_ordered)
	                {
	                	String stemmed_en = PlingStemmer.stem(e.getKey());
	                	//succset1.add(stemmed_en);
	                	
	                	if(scores1_ordered1.get(stemmed_en)==null) scores1_ordered1.put(stemmed_en, e.getValue());
	                	else scores1_ordered1.put(stemmed_en, e.getValue() + scores1_ordered1.get(stemmed_en));
	                	
	                	if(support_sentences1.get(stemmed_en)!=null)
	                	{
	                		if(support_sentences.get(e.getKey())!=null)
	                		{
	                			HashSet<String> sent1 = support_sentences.get(e.getKey());
	                			sent1.addAll(support_sentences1.get(stemmed_en));
	                			support_sentences1.put(stemmed_en, sent1);
	                		}
	                		
	                	}
	                	else
                		{
	                		if(support_sentences.get(e.getKey())!=null)
	                		{
	                			HashSet<String> sent1 = support_sentences.get(e.getKey());
	                			//sent1.addAll(support_sentences1.get(stemmed_en));
	                			support_sentences1.put(stemmed_en, sent1);
	                		}
                		}
	                	
	                	if(subtypes1.get(stemmed_en)!=null)
	                	{
	                		if(subtypes.get(e.getKey())!=null)
	                		{
	                			HashSet<String> sent1 = subtypes.get(e.getKey());
	                			sent1.addAll(subtypes1.get(stemmed_en));
	                			subtypes1.put(stemmed_en, sent1);
	                		}
	                		
	                	}
	                	else
                		{
	                		if(subtypes.get(e.getKey())!=null)
	                		{
	                			HashSet<String> sent1 = subtypes.get(e.getKey());
	                			//sent1.addAll(support_sentences1.get(stemmed_en));
	                			subtypes1.put(stemmed_en, sent1);
	                		}
                		}
	                	
	                }
	                
	                scores1_ordered = new ArrayList<>(scores1_ordered1.entrySet());
	                Collections.sort(scores1_ordered, LanguageModelEntity.valueComparator);
	                subtypes = subtypes1;
	                support_sentences = support_sentences1;
	                int count=0;
	                for(Entry<String, Double> e:scores1_ordered)
	                {
	                	count++;
	                	//if(count>10) break;
	                	if(co_occ.get(e.getKey())==null) 
	                		{
	                			boolean flag = false;
		                		for(String ss:subtypes.get(e.getKey()))
			                	{
		                			if(co_occ.get(ss)!=null) 
		                			{
		                				flag = true;
		                			}
			                	}
		                		continue;
	                		}
	                	//if(scores1.get(e.getKey())==null && co_occ.get(e.getKey())==null && !succset1.contains(e.getKey())) continue;
	                	//if(!succset1.contains(e.getKey())) continue;
	                	if(dont_display.contains(e.getKey())) continue;
	                		writer.println("<button class=\"tablinks\" onclick=\"openCity(event,'"+e.getKey()+"')\">"+e.getKey()+"</button>\n");
	                }
	                writer.println("</div>");
	                count=0;
	                
	                for(Entry<String, Double> e:scores1_ordered)
	                {
	                	//if(!succset1.contains(e.getKey())) continue;
	                	//if(scores1.get(e.getKey())==null && co_occ.get(e.getKey())==null && !succset1.contains(e.getKey())) continue;
	                	if(dont_display.contains(e.getKey())) continue;
	                	count++;
	                	//if(count>10) break;
	                	writer.println("<div id=\""+e.getKey()+"\" class=\"tabcontent\">");
	                	writer.println("<h3>"+e.getKey()+": "+scores.get(e.getKey())+", "+scores1.get(e.getKey())+", "+co_occ.get(e.getKey()));
	                	int c=1;
	                	if(support_sentences.get(e.getKey())!=null)
	                	{
		                	for(String ss1:support_sentences.get(e.getKey()))
		                	{
		                		writer.println(c+") "+ss1+"<br>");
		                		c++;
		                	}
	                	}
	                	writer.println("</h3>");
	                	
	                	//writer.println("<h3>"+e.getKey()+", "+e.getValue()+"</h3>");
	                	for(String ss:subtypes.get(e.getKey()))
	                	{
	                		int c1=1;
	                		writer.println("<p>"+ss+", "+scores1.get(ss)+", "+co_occ.get(ss)+"</p>");
	                		if(support_sentences.get(ss)!=null)
		                	{
	                			for(String ss1:support_sentences.get(ss))
			                	{
			                		writer.println("<p>"+c1+") "+ss1+"</p>");
			                		c1++;
			                	}
		                	}
	                	}
	                	writer.println("</div>");
	                }
	                
	                writer.println("<script>");
	                writer.println("function openCity(evt, cityName) {");
	                //writer.println("function openCity(evt, cityName) {");
	                writer.println("tabcontent = document.getElementsByClassName(\"tabcontent\");");
	                writer.println(" for (i = 0; i < tabcontent.length; i++) {\n" + 
	                		"    tabcontent[i].style.display = \"none\";\n" + 
	                		"  }\n" + 
	                		"  tablinks = document.getElementsByClassName(\"tablinks\");\n" + 
	                		"  for (i = 0; i < tablinks.length; i++) {\n" + 
	                		"    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");\n" + 
	                		"  }\n" + 
	                		"  document.getElementById(cityName).style.display = \"block\";\n" + 
	                		"  evt.currentTarget.className += \" active\";\n" + 
	                		"}\n" + 
	                		"</script>");
	                // writer.println("<iframe src=\"https://en.wikipedia.org/wiki/PageRank#Variations\">\n" + "</iframe>");
	                
	                writer.println("</body></html>");
	                logger.log(Level.ALL, "wrote to file");
	                //System.out.println(a1.print());
               }
			
				} catch (Exception e1) 
            	{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
            }
        };
         
        String servletName = "Servlet2";
        String urlPattern = hm.get("urlpattern");
         
        tomcat.addServlet(contextPath, servletName, servlet);      
        context.addServletMappingDecoded(urlPattern, servletName);
         
        tomcat.start();
        tomcat.getServer().await();
      
    }
}
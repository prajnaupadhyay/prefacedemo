package tomcat;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cc.mallet.util.Maths;
import main.java.org.ahocorasick.trie.Emit;

public class TaskParallelizeExtractAbstract implements Runnable
{
	Element el;
	ConcurrentHashMap<Element,String> abstractlist;
	String port;
	
	public TaskParallelizeExtractAbstract(Element el, ConcurrentHashMap<Element,String> abstractlist,String port)
	{
		this.el=el;
		this.abstractlist=abstractlist;
		this.port=port;
	}
	
	public void run() 
	{
		try
		{
			String tot_abstract="";
			String elem_query="http://localhost:"+port+"/"+el.attr("href");
	     	  String elem_string = Jsoup.connect(elem_query).get().html();
	     	 Document elem_doc = Jsoup.parse(elem_string);
	     	 Elements eel = elem_doc.getElementsByTag("paperAbstract");
	     	 
	     	 for(Element e1:eel)
	     	 {
	     		 tot_abstract = tot_abstract + " "+e1.text() ;
	     	 }
	     	 
	     	 abstractlist.put(el, tot_abstract);
			  
		}
		catch(Exception e)
		{
			System.out.println("Exception is: "+e);
		}
	}

}

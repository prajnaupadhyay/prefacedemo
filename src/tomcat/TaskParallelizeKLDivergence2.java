package tomcat;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import cc.mallet.util.Maths;
import main.java.org.ahocorasick.trie.Emit;

public class TaskParallelizeKLDivergence2 implements Runnable
{
	Entry<String,Double> e;
	double mindistance;
	HashMap<String, HashSet<String>> features;
	HashMap<String, Collection<Emit>> entities_in_phrase;
	HashMap<String,Double> refdScores;
	ConcurrentHashMap<Set<String>, Double> scores;
	HashMap<String, Integer> ent_index;
	HashMap<Set<String>,double[]> lm_rep;
	double[] prob_dist_query;
	ConcurrentHashMap<String, Double> scores_string;
	
	public TaskParallelizeKLDivergence2(Entry<String,Double> e,double mindistance, HashMap<String, HashSet<String>> features, HashMap<String, Collection<Emit>> entities_in_phrase, HashMap<String,Double> refdScores,ConcurrentHashMap<Set<String>, Double> scores,HashMap<String, Integer> ent_index,HashMap<Set<String>,double[]> lm_rep, double[] prob_dist_query,ConcurrentHashMap<String, Double> scores_string)
	{
		this.e=e;
		this.mindistance=mindistance;
		this.features=features;
		this.entities_in_phrase=entities_in_phrase;
		this.refdScores=refdScores;
		this.scores=scores;
		this.ent_index=ent_index;
		this.lm_rep=lm_rep;
		this.prob_dist_query=prob_dist_query;
		this.scores_string=scores_string;
	}
	
	public void run() 
	{
		try
		{
			Maths m = new Maths();
			 
				  Set<String> neighs = SimpleWebApp.checkExtractNeighbor(((String)e.getKey()).toLowerCase().replace("_"," "), features,mindistance);

				  //System.out.println("computing kl divergence for: "+e.getKey());
				 // Set<String> neighs = chooseNeighborsStored(((String)e.getKey()).toLowerCase().replace("_"," "), clustersWithTags);
				  if(neighs==null) 
				  {
					  System.out.println(" null for "+e.getKey());
				  }
				  //System.out.println("not null for "+e.getKey());
				  double[] lm = SimpleWebApp.computeLMSet(neighs,ent_index);
				  lm_rep.put(neighs, lm);
				  double kl_div = m.klDivergence(prob_dist_query,lm);
				  scores.put(neighs, kl_div);
				 // scores_string.put(SimpleWebApp.convertToString(neighs), kl_div);
			  
		}
		catch(Exception e)
		{
			System.out.println("Exception is: "+e);
		}
	}

}

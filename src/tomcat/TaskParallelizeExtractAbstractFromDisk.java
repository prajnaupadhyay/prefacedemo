package tomcat;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cc.mallet.util.Maths;
import main.java.org.ahocorasick.trie.Emit;

public class TaskParallelizeExtractAbstractFromDisk implements Runnable
{
	Element el;
	ConcurrentHashMap<Element,String> abstractlist;
	String port;
	DocumentBuilder db;
	
	public TaskParallelizeExtractAbstractFromDisk(Element el, ConcurrentHashMap<Element,String> abstractlist,String port,DocumentBuilder db)
	{
		this.el=el;
		this.abstractlist=abstractlist;
		this.port=port;
		this.db=db;
	}
	
	public void run() 
	{
		try
		{
			String tot_abstract="";
			if( el.attr("href").contains("document?identifier="))
		       {
			        String xml_file = el.attr("href").replace("document?identifier=", "");
			        //System.out.println(el.attr("href"));
			        //System.out.println(xml_file); 
			        org.w3c.dom.Document doc = db.parse(xml_file);
			        
			        doc.getDocumentElement().normalize();    
			       // System.out.println("Root element: " + doc.getDocumentElement().getNodeName());   
			        NodeList nodeList = doc.getElementsByTagName("paperAbstract");		
			        for (int itr = 0; itr < nodeList.getLength(); itr++)   
			        {  
			        	Node node = nodeList.item(itr);  
				     	 tot_abstract = tot_abstract + " "+node.getTextContent();
			        	//System.out.println("\nNode value :" + node.getTextContent());  
				     	  
			        }
			        //SSystem.out.println("extracted abstract");
		       }
	     	 
	     	 abstractlist.put(el, tot_abstract);
			  
		}
		catch(Exception e)
		{
			System.out.println("Exception is: "+e);
		}
	}

}

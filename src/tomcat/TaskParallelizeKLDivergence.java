package tomcat;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import main.java.org.ahocorasick.trie.Emit;

public class TaskParallelizeKLDivergence implements Runnable
{
	Entry<String,Double> e;
	double mindistance;
	HashMap<String, HashSet<String>> features;
	HashMap<String, Collection<Emit>> entities_in_phrase;
	HashMap<String,Double> refdScores;
	ConcurrentHashMap<Set<String>, Double> scores;
	ConcurrentHashMap<String, Double> scores_string;
	
	public TaskParallelizeKLDivergence(Entry<String,Double> e,double mindistance, HashMap<String, HashSet<String>> features, HashMap<String, Collection<Emit>> entities_in_phrase, HashMap<String,Double> refdScores,ConcurrentHashMap<Set<String>, Double> scores,ConcurrentHashMap<String, Double> scores_string)
	{
		this.e=e;
		this.mindistance=mindistance;
		this.features=features;
		this.entities_in_phrase=entities_in_phrase;
		this.refdScores=refdScores;
		this.scores=scores;
		this.scores_string=scores_string;
	}
	
	public void run() 
	{
		try
		{
		Set<String> neighs = SimpleWebApp.checkExtractNeighbor(((String)e.getKey()).toLowerCase().replace("_"," "), features,mindistance);
		  if(neighs==null) 
		  {
			  System.out.println(" null for "+e.getKey());
			  
		  }
		  double score=0.0;
		  for(String p:neighs)
		  {
			 
			  Collection<Emit> cc = entities_in_phrase.get(p);
			  HashSet<String> uniqueEnt = new HashSet<String>();
			  for(Emit e1:cc)
			  {
				 String ent = e1.getKeyword().replace(" ", "_");
				 uniqueEnt.add(ent);
			  }
			  for(String ent:uniqueEnt)
			  {
				 //String ent = e1.getKeyword().replace(" ", "_");
				 if(refdScores.get(ent)!=null)
				 {
					 if(refdScores.get(ent)>0)
						 //score = score + refdScores.get(ent);
					 score = score + 1;
				 }
			  }
			  
			 
		  }
		  scores.put(neighs, 1.0/score);
		 // scores_string.put(SimpleWebApp.convertToString(neighs), score);
		}
		catch(Exception e)
		{
			System.out.println("Exception is: "+e);
		}
	}

}

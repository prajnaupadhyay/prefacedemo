package tomcat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class GetPropertyValues 
{
	String result = "";
	InputStream inputStream;
	 
	public HashMap<String, String> getPropValues() throws IOException {
	//Enumeration<?> e1;
	//try {
	Properties prop = new Properties();
	//String propFileName = "/home/cse/phd/csz138110/workspace/prerequsiteStructures/resources/config/c.properties";
	String propFileName = "/home/prajna/workspace/tomcat/resources/config/config.properties";
	//String propFileName = "/home/pearl/workspace/tomcat/resources/config/config.properties";
	//inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 inputStream = new FileInputStream(propFileName);
	if (inputStream != null) 
	{
		prop.load(inputStream);
	} 
	else 
	{
		throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
	}
	 
	Date time = new Date(System.currentTimeMillis());
	 
	// get the property value and print it out
/*	String user = prop.getProperty("user");
	String task = prop.getProperty("task");
	String type = prop.getProperty("type");
	String prune_para = prop.getProperty("prune-para");
	 
	result = "Company List = " + task + ", " + type + ", " + prune_para;
	System.out.println(result + "\nProgram Ran on " + time + " by user=" + user);*/
	
	Enumeration<?> e1 = prop.propertyNames();
	HashMap<String, String> e2 = new HashMap<String, String>();
	while (e1.hasMoreElements()) 
	{
		String key = (String) e1.nextElement();
		String value = prop.getProperty(key);
		//System.out.println("Key : " + key + ", Value : " + value);
		e2.put(key, value);
	}
	
	//} catch (Exception e) {
	//System.out.println("Exception: " + e);
	//} finally {
	//inputStream.close();
	//}
	inputStream.close();
	return e2;
	}
}
